﻿using GitConnectorModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntoMsg_Module.Logging;
using AssemblyAnaylzeModule.Models;

namespace AutomationLibrary.Controller
{

    public class AnalyseAssemblyController : BaseController, IModuleController
    {


        public SyncMembersParam SyncMembersParameters { get; private set; }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_AssemblyAnalyzeModule;

        public List<Type> Functions => new List<Type> { typeof(SyncMembersParam) };

        public override string Syntax
        {
            get
            {
                return $"{nameof(DatabaseManagementModule)} {nameof(SyncMembersParam.IdConfig)}:<Id of configuration>";
            }
        }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(AssemblyAnaylzeModule).ToLower();
        }
        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new SyncMembersRequest(SyncMembersParameters.IdConfig, SyncMembersParameters.CancellationTokenSource.Token, new clsOntologyItem { GUID = SyncMembersParameters.IdUser });

            var syncConnector = new AssemblyAnaylzeModule.AssemblyAnalyzeController(globals);

            try
            {
                var syncTask = await syncConnector.SyncMembers(request);

                result = syncTask.ResultState;
                if (result.GUID == syncConnector.Globals.LState_Error.GUID)
                {
                    SyncMembersParameters.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new SyncMembersRequest(SyncMembersParameters.IdConfig, SyncMembersParameters.CancellationTokenSource.Token, new clsOntologyItem { GUID = SyncMembersParameters.IdUser });

            var syncConnector = new AssemblyAnaylzeModule.AssemblyAnalyzeController(globals);

            try
            {
                var syncTask = syncConnector.SyncMembers(request);
                syncTask.Wait();

                result = syncTask.Result.ResultState;
                if (result.GUID == syncConnector.Globals.LState_Error.GUID)
                {
                    SyncMembersParameters.MessageOutput?.OutputError(result.Additional1);
                    return result;
                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public AnalyseAssemblyController(SyncMembersParam syncParam, Globals globals) : base(globals)
        {
            SyncMembersParameters = syncParam;
            IsValid = true;
            var validationResult = SyncMembersParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public AnalyseAssemblyController(CommandLine commandLine, Globals globals) : base(globals,commandLine)
        {

            IsValid = true;
            SyncMembersParameters = new SyncMembersParam
            {
                MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
            };

            var validateSyncResult = SyncMembersParameters.ValidateParam(commandLine, globals);
            IsValid = (validateSyncResult.GUID == globals.LState_Success.GUID);
            ErrorMessage += validateSyncResult.Additional1;
            if (string.IsNullOrEmpty(ErrorMessage))
            {
                SyncMembersParameters.MessageOutput?.OutputError(ErrorMessage);
            }


        }

        public AnalyseAssemblyController() { }

    }
}
