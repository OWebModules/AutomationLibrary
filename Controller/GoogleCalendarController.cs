﻿using GoogleCalendarConnector;
using GoogleCalendarConnector.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    public class GoogleCalendarController : BaseController, IModuleController
    {
        
        public GoogleCalendarParam Parameters { get; private set; }
        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(GoogleCalendarConnector).ToLower();
        }

        public override string Syntax
        {
            get
            {
                return $"{nameof(GoogleCalendarConnector)} IdConfig:<Id>";
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_GoogleCalendarConnector;

        public List<Type> Functions => new List<Type> { typeof(GoogleCalendarParam) };

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new CalendarSyncRequest(this.CommandLine.IdConfig);
            var syncConnector = new CalendarConnector(globals);

            try
            {
                var syncTask = syncConnector.SyncCalendar(request);
                syncTask.Wait();

                result = syncTask.Result.ResultState;
                if (result.GUID == globals.LState_Success.GUID)
                {
                    result.Additional1 = $"Synced {syncTask.Result.EventSync.OntoAppointmentEvents.Count} Appointments\nSynced {syncTask.Result.EventSync.OntoPersonalDateEvents.Count} Personal Dates";


                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = base.DoWork();
                if (result.GUID == globals.LState_Error.GUID) return result;

                var request = new CalendarSyncRequest(Parameters.IdConfig)
                {
                    MessageOutput = Parameters.MessageOutput
                };

                var syncConnector = new CalendarConnector(globals);

                try
                {
                    var syncTask = await syncConnector.SyncCalendar(request);

                    result = syncTask.ResultState;
                    if (result.GUID == globals.LState_Success.GUID)
                    {
                        result.Additional1 = $"Synced {syncTask.EventSync.OntoAppointmentEvents.Count} Appointments\nSynced {syncTask.EventSync.OntoPersonalDateEvents.Count} Personal Dates";


                    }
                    return result;
                }
                catch (Exception ex)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = ex.Message;
                    return result;

                }

            });

            return taskResult;
        }

        public GoogleCalendarController(GoogleCalendarParam syncParam, Globals globals) : base(globals)
        {
            Parameters = syncParam;
            IsValid = true;
            var validationResult = Parameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public GoogleCalendarController(CommandLine commandLine, Globals globals) :base(globals,commandLine)
        {
            IsValid = true;
            Parameters = new GoogleCalendarParam();
            if (string.IsNullOrEmpty(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid!";
                IsValid = false;
                return;
            }

            if (!globals.is_GUID(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid (no GUID)!";
                IsValid = false;
                return;
            }

            Parameters.IdConfig = commandLine.IdConfig;
        }

        public GoogleCalendarController() { }
    }
}
