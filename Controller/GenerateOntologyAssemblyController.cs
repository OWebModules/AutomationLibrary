﻿using GenerateOntologyAssembly.Models;
using ImportExport_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    public enum GenerateFunction
    {
        OntologyAssembly = 0,
        ClassAssembly = 1
    }

    public class GenerateOntologyAssemblyController : BaseController, IModuleController
    {
        public GenerateFunction Function { get; private set; }
       
        public GenerateOntologyAssemblyParam GenerateOntologyAssemblyParameters { get; private set; }
        public GenerateOntologyAssemblyClassParam GenerateOntologyAssemblyClassParameters { get; private set; }



        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(GenerateOntologyAssembly).ToLower();   
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"Module:{nameof(GenerateOntologyAssembly)} {nameof(Function)}:{nameof(GenerateFunction.OntologyAssembly)} {nameof(GenerateOntologyAssemblyParam.IdOntologyUpdate)}:<Id>");
                sbSyntax.AppendLine($"Module:{nameof(GenerateOntologyAssembly)} {nameof(Function)}:{nameof(GenerateFunction.ClassAssembly)} {nameof(GenerateOntologyAssemblyClassParam.IdOntology)}:<Id> {nameof(GenerateOntologyAssemblyClassParam.Version)}:<Version> {nameof(GenerateOntologyAssemblyClassParam.NameSpace)}:<Namespace> {nameof(GenerateOntologyAssemblyClassParam.FileName)}:<Filename> {nameof(GenerateOntologyAssemblyClassParam.Path)}:<Path> {nameof(GenerateOntologyAssemblyClassParameters.IdClassRoot)}:<Id of the root-class for export>");
                return sbSyntax.ToString();
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_GenerateOntologyAssembly;

        public List<Type> Functions => new List<Type> { typeof(GenerateOntologyAssemblyParam), typeof(GenerateOntologyAssemblyClassParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var connector = new GenerateOntologyAssembly.GenerateOntologyAssemblyController(globals);
            try
            {

                if (Function == GenerateFunction.OntologyAssembly)
                {
                    var request = new GenerateAssemblyRequest(GenerateOntologyAssemblyParameters.IdOntologyUpdate)
                    {
                        MessageOutput = GenerateOntologyAssemblyParameters.MessageOutput
                    };

                    var exportResult = await connector.GenerateAssembly(request);

                    result = exportResult.ResultState;

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }
                else
                {
                    var request = new GenerateClassAssemblyRequest(GenerateOntologyAssemblyClassParameters.IdOntology, GenerateOntologyAssemblyClassParameters.Version, GenerateOntologyAssemblyClassParameters.NameSpace, GenerateOntologyAssemblyClassParameters.FileName, GenerateOntologyAssemblyClassParameters.Path, GenerateOntologyAssemblyClassParameters.IdClassRoot)
                    {
                        MessageOutput = GenerateOntologyAssemblyClassParameters.MessageOutput
                    };

                    var exportResult = await connector.GenerateClassAssembly(request);

                    result = exportResult.ResultState;

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }


                result.Additional1 = $"Successful generated Assembly";
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }


        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var connector = new GenerateOntologyAssembly.GenerateOntologyAssemblyController(globals);
            try
            {

                if (Function == GenerateFunction.OntologyAssembly)
                {
                    var request = new GenerateAssemblyRequest(GenerateOntologyAssemblyParameters.IdOntologyUpdate);
                    var exportResult = connector.GenerateAssembly(request);
                    exportResult.Wait();

                    result = exportResult.Result.ResultState;

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }
                else
                {
                    var request = new GenerateClassAssemblyRequest(GenerateOntologyAssemblyClassParameters.IdOntology, GenerateOntologyAssemblyClassParameters.Version, GenerateOntologyAssemblyClassParameters.NameSpace, GenerateOntologyAssemblyClassParameters.FileName, GenerateOntologyAssemblyClassParameters.Path, GenerateOntologyAssemblyClassParameters.IdClassRoot);
                    var exportResult = connector.GenerateClassAssembly(request);
                    exportResult.Wait();

                    result = exportResult.Result.ResultState;

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }
                

                result.Additional1 = $"Successful generated Assembly";
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }


        }

        public GenerateOntologyAssemblyController(GenerateOntologyAssemblyParam syncParam, Globals globals) : base(globals)
        {
            GenerateOntologyAssemblyParameters = syncParam;
            IsValid = true;
            var validationResult = GenerateOntologyAssemblyParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public GenerateOntologyAssemblyController(GenerateOntologyAssemblyClassParam syncParam, Globals globals) : base(globals)
        {
            GenerateOntologyAssemblyClassParameters = syncParam;
            IsValid = true;
            var validationResult = GenerateOntologyAssemblyClassParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public GenerateOntologyAssemblyController(CommandLine commandLine, Globals globals) : base(globals,commandLine)
        {

            IsValid = true;
            ErrorMessage = "";

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You must provide a function\n";
                return;
            }

            var function = commandLine.AdditionalParameters[key];

            if (function.ToLower() == nameof(GenerateFunction.OntologyAssembly).ToLower())
            {
                Function = GenerateFunction.OntologyAssembly;
            }
            else if (function.ToLower() == nameof(GenerateFunction.ClassAssembly).ToLower())
            {
                Function = GenerateFunction.ClassAssembly;
            }
            else
            {
                IsValid = false;
                ErrorMessage += "No valid Function!";
                return;
            }

            
            switch(Function)
            {
                case GenerateFunction.ClassAssembly:
                    GenerateOntologyAssemblyClassParameters = new GenerateOntologyAssemblyClassParam()
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };

                    var validateClassResult = GenerateOntologyAssemblyClassParameters.ValidateParam(commandLine, globals);
                    IsValid = validateClassResult.GUID == globals.LState_Success.GUID;
                    ErrorMessage = validateClassResult.Additional1;
                    break;
                case GenerateFunction.OntologyAssembly:
                    GenerateOntologyAssemblyParameters = new GenerateOntologyAssemblyParam()
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };

                    var validateResult = GenerateOntologyAssemblyParameters.ValidateParam(commandLine, globals);
                    IsValid = validateResult.GUID == globals.LState_Success.GUID;
                    ErrorMessage = validateResult.Additional1;
                    break;
            }
            
        }


        public GenerateOntologyAssemblyController() { }
    }
}
