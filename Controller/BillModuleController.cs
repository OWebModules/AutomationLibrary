﻿using ImportExport_Module;
using MediaStore_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutomationLibrary.Attributes;
using OntoMsg_Module.Logging;

namespace AutomationLibrary.Controller
{
    public class BillModuleController : BaseController, IModuleController
    {

        public enum ModuleFunction
        {
            SyncBanktransactions = 0,
            SyncFinancialTransactionsFromParentToChildren = 1
        }
        public SyncBanktransactionsParam SyncBanktransactionsParameters { get; private set; } 
        public SyncFinancialTransactionsFromParentToChildrenParam SyncFinancialTransactionsFromParentToChildrenParameters { get; private set; }

        public ModuleFunction Function { get; private set; }
        
        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"{nameof(BillModule)} {nameof(Function)}:{nameof(ModuleFunction.SyncBanktransactions)} {nameof(SyncBanktransactionsParam.IdTextParser)}:<Id of Textparser> {nameof(SyncBanktransactionsParam.Query)}:<Query for documents (Optional)>");
                sbSyntax.AppendLine($"{nameof(BillModule)} {nameof(Function)}:{nameof(ModuleFunction.SyncFinancialTransactionsFromParentToChildren)} {nameof(SyncFinancialTransactionsFromParentToChildrenParam.IdFinanicalTransaction)}:<Id of Financial Transaction>");
                return sbSyntax.ToString();
            }
        }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(BillModule).ToLower();
        }

        public clsOntologyItem Module
        {
            get => Modules.Config.LocalData.Object_Bill_Module;
        }
        public List<Type> Functions
        {
            get => new List<Type> { typeof(SyncBanktransactionsParam), typeof(SyncFinancialTransactionsFromParentToChildrenParam) };
        }

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;
            

            switch (Function)
            {
                case ModuleFunction.SyncBanktransactions:
                    var syncConnector = new BillModule.BanktransactionsController(globals);
                    var request = new BillModule.Models.SyncBanktransactionsRequest(SyncBanktransactionsParameters.IdTextParser)
                    {
                        MessageOutput = SyncBanktransactionsParameters.MessageOutput,
                        Query = SyncBanktransactionsParameters.Query
                    };

                    try
                    {
                        var syncTask = await syncConnector.SyncBanktransactions(request);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Additional1 = $"Count of Synced Items: {syncTask.Result.TransactionItems.Count}";
                        SyncBanktransactionsParameters.MessageOutput?.OutputInfo(result.Additional1);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }
                case ModuleFunction.SyncFinancialTransactionsFromParentToChildren:
                    var syncParentToChildrenConnector = new BillModule.TransactionConnector(globals);
                    var syncRequestParentToChildrenRequest = new BillModule.Models.SyncFinancialTransactionsParentToChildrenRequest(SyncFinancialTransactionsFromParentToChildrenParameters.IdFinanicalTransaction)
                    {
                        MessageOutput = SyncFinancialTransactionsFromParentToChildrenParameters.MessageOutput,
                    };

                    try
                    {
                        var syncTask = await syncParentToChildrenConnector.SyncFinancialTransactionsParentToChildren(syncRequestParentToChildrenRequest);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Additional1 = $"Count of Synced Items: {syncTask.Result.Count}";
                        SyncFinancialTransactionsFromParentToChildrenParameters.MessageOutput?.OutputInfo(result.Additional1);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }

            }

            return result;
            
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new BillModule.BanktransactionsController(globals);

            try
            {

                switch(Function)
                {
                    case ModuleFunction.SyncBanktransactions:
                        var request = new BillModule.Models.SyncBanktransactionsRequest(SyncBanktransactionsParameters.IdTextParser)
                        {
                            MessageOutput = SyncBanktransactionsParameters.MessageOutput,
                            Query = SyncBanktransactionsParameters.Query
                        };

                        try
                        {
                            var syncTask = syncConnector.SyncBanktransactions(request);
                            syncTask.Wait();

                            result = syncTask.Result.ResultState;
                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                return result;
                            }

                            result.Additional1 = $"Count of Synced Items: {syncTask.Result.Result.TransactionItems.Count}";
                            SyncBanktransactionsParameters.MessageOutput?.OutputInfo(result.Additional1);

                            return result;
                        }
                        catch (Exception ex)
                        {
                            result = globals.LState_Error.Clone();
                            result.Additional1 = ex.Message;
                            return result;

                        }
                }
                


                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public BillModuleController(SyncBanktransactionsParam syncParam, Globals globals) : base(globals)
        {
            SyncBanktransactionsParameters = syncParam;
            IsValid = true;
            Function = ModuleFunction.SyncBanktransactions;
            var validationResult = SyncBanktransactionsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public BillModuleController(SyncFinancialTransactionsFromParentToChildrenParam syncParam, Globals globals) : base(globals)
        {
            SyncFinancialTransactionsFromParentToChildrenParameters = syncParam;
            IsValid = true;
            Function = ModuleFunction.SyncFinancialTransactionsFromParentToChildren;
            var validationResult = SyncFinancialTransactionsFromParentToChildrenParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public BillModuleController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            IsValid = true;

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You have to provide an Function\n";
            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == ModuleFunction.SyncBanktransactions.ToString().ToLower())
                {
                    Function = ModuleFunction.SyncBanktransactions;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == ModuleFunction.SyncFinancialTransactionsFromParentToChildren.ToString().ToLower())
                {
                    Function = ModuleFunction.SyncFinancialTransactionsFromParentToChildren;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a valid Function\n";
                }
            }

            switch(Function)
            {
                case ModuleFunction.SyncBanktransactions:
                    SyncBanktransactionsParameters = new SyncBanktransactionsParam();
                    SyncBanktransactionsParameters.MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath);
                    var validateSyncResult = SyncBanktransactionsParameters.ValidateParam(commandLine, globals);
                    IsValid = (validateSyncResult.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateSyncResult.Additional1;
                    if (string.IsNullOrEmpty(ErrorMessage))
                    {
                        SyncBanktransactionsParameters.MessageOutput?.OutputError(ErrorMessage);
                    }
                    
                    break;
                
            }   
        }

        public BillModuleController() { }

    }
}
