﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module.Logging;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    public class ObjectTreeController : BaseController, IModuleController
    {
        
        public enum ControllerFunction
        {
            GetHierarchicalItem = 0
        }

        public ControllerFunction Function { get; private set; }
        public GetHierarchicalItemParam GetHierarchicalItemParameters { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(OntologyItemsModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbResult = new StringBuilder();

                sbResult.AppendLine($"{nameof(CommandLine.Module)}:{nameof(OntologyItemsModule)} {nameof(Function)}:{nameof(ControllerFunction.GetHierarchicalItem)} {nameof(GetHierarchicalItemRequest.FullName)}:<FullName of item to search> {nameof(GetHierarchicalItemRequest.IdClass)}:<Class-Id of hierarchical class> {nameof(GetHierarchicalItemRequest.IdRelationType)}:<RelationType-Id of hierarchical class> {nameof(GetHierarchicalItemRequest.FullNameSplitter)}:<Splitter to split the fullname>");
                return $"{nameof(OModules)}";
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_OntologyItemsModule;

        public List<Type> Functions => new List<Type> { typeof(GetHierarchicalItemParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new OntologyItemsModule.ObjectTreeController(globals);

            try
            {

                switch (Function)
                {
                    case ControllerFunction.GetHierarchicalItem:
                        var requestGet = new GetHierarchicalItemRequest(GetHierarchicalItemParameters.IdClass,
                            GetHierarchicalItemParameters.IdRelationType,
                            GetHierarchicalItemParameters.FullName,
                            GetHierarchicalItemParameters.FullNameSplitter)
                        {
                            MessageOutput = GetHierarchicalItemParameters.MessageOutput,
                            NameSpaceIdsToExclude = GetHierarchicalItemParameters.NameSpaceIdsToExclude.Select(excl => excl.Id).ToList()
                        };
                        var syncTask = await syncConnector.GetHierarchicalItem(requestGet);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            if (syncTask.Result.HierarchicalItem == null)
                            {
                                result.Additional1 = "No Hierarchical Item found!";
                            }
                            else
                            {
                                result.Additional1 = $"Found item {syncTask.Result.HierarchicalItem.GetFullPath(requestGet.FullNameSplitter)}, id: {syncTask.Result.HierarchicalItem.NameItem}";
                            }



                        }
                        return result;
                    default:
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "No valid Function provided!";
                        return result;
                }


            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new OntologyItemsModule.ObjectTreeController(globals);

            try
            {

                switch(Function)
                {
                    case ControllerFunction.GetHierarchicalItem:
                        var requestGet = new GetHierarchicalItemRequest(GetHierarchicalItemParameters.IdClass, 
                            GetHierarchicalItemParameters.IdRelationType, 
                            GetHierarchicalItemParameters.FullName, 
                            GetHierarchicalItemParameters.FullNameSplitter)
                        {
                            MessageOutput = GetHierarchicalItemParameters.MessageOutput,
                            NameSpaceIdsToExclude = GetHierarchicalItemParameters.NameSpaceIdsToExclude.Select(excl => excl.Id).ToList()
                        };
                        var syncTask = syncConnector.GetHierarchicalItem(requestGet);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            if (syncTask.Result.Result.HierarchicalItem == null)
                            {
                                result.Additional1 = "No Hierarchical Item found!";
                            }
                            else
                            {
                                result.Additional1 = $"Found item {syncTask.Result.Result.HierarchicalItem.GetFullPath(requestGet.FullNameSplitter)}, id: {syncTask.Result.Result.HierarchicalItem.NameItem}";
                            }
                            


                        }
                        return result;
                    default:
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "No valid Function provided!";
                        return result;
                }

                
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public ObjectTreeController(GetHierarchicalItemParam syncParam, Globals globals) : base(globals)
        {
            GetHierarchicalItemParameters = syncParam;
            IsValid = true;
            Function = ControllerFunction.GetHierarchicalItem;
            var validationResult = GetHierarchicalItemParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public ObjectTreeController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {

            IsValid = true;

            var sbError = new StringBuilder();
            var key = commandLine.AdditionalParameters.Keys.SingleOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                sbError.AppendLine($"You have to provide a valid function!");
            }
            else
            {
                var function = commandLine.AdditionalParameters[key];
                if (function == nameof(ControllerFunction.GetHierarchicalItem))
                {
                    Function = ControllerFunction.GetHierarchicalItem;
                }
                else
                {
                    IsValid = false;
                    sbError.AppendLine($"The provided function is not valid!");
                }

            }

            if (IsValid && Function == ControllerFunction.GetHierarchicalItem)
            {
                GetHierarchicalItemParameters = new GetHierarchicalItemParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                key = commandLine.AdditionalParameters.Keys.SingleOrDefault(keyItem => keyItem.ToLower() == nameof(GetHierarchicalItemRequest.FullName).ToLower());
                if (key != null)
                {
                    GetHierarchicalItemParameters.FullName = commandLine.AdditionalParameters[key];
                }


                key = commandLine.AdditionalParameters.Keys.SingleOrDefault(keyItem => keyItem.ToLower() == nameof(GetHierarchicalItemRequest.IdClass).ToLower());
                if (key != null)
                {
                    GetHierarchicalItemParameters.IdClass = CommandLine.AdditionalParameters[key];
                }

                key = commandLine.AdditionalParameters.Keys.SingleOrDefault(keyItem => keyItem.ToLower() == nameof(GetHierarchicalItemRequest.FullNameSplitter).ToLower());
                if (key != null)
                {
                    GetHierarchicalItemParameters.FullNameSplitter = CommandLine.AdditionalParameters[key];
                }
                
                key = commandLine.AdditionalParameters.Keys.SingleOrDefault(keyItem => keyItem.ToLower() == nameof(GetHierarchicalItemRequest.IdRelationType).ToLower());
                if (key != null)
                {
                    GetHierarchicalItemParameters.IdRelationType = CommandLine.AdditionalParameters[key];
                }

                key = commandLine.AdditionalParameters.Keys.SingleOrDefault(keyItem => keyItem.ToLower() == nameof(GetHierarchicalItemRequest.NameSpaceIdsToExclude).ToLower());

                if (key != null)
                {
                    var excludes = commandLine.AdditionalParameters[key];
                    GetHierarchicalItemParameters.NameSpaceIdsToExclude = excludes.Split('.').Select(exclude => new HierarchicalExcludeItem
                    {
                        Id = exclude
                    }).ToList();
                }

                var validationResult = GetHierarchicalItemParameters.ValidateParam(globals);
                IsValid = (validationResult.GUID == globals.LState_Success.GUID);
                ErrorMessage = validationResult.Additional1;

            }

            ErrorMessage = sbError.ToString();
        }

        public ObjectTreeController() { }
    }
}