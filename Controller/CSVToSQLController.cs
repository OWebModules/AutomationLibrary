﻿using BCMCSVImporter;
using BCMCSVImporter.Models;
using ImportExport_Module;
using MediaStore_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    public class CSVToSQLController : BaseController, IModuleController
    {

        public CSVToSQLImportParam CSVToSQLImportParameters { get; private set; }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_CSVToSQLScript;

        public List<Type> Functions => new List<Type> { typeof(CSVToSQLImportParam) };

        public override string Syntax
        {
            get
            {
                return $"{nameof(CSVToSQLScript)} {nameof(CSVToSQLImportParam.IdConfig)}:<Id> {nameof(CSVToSQLImportParam.DownloadPath)}:<DownloadPath>";
            }
        }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(CSVToSQLScript).ToLower();
        }

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new RequestInsertInto(CSVToSQLImportParameters.IdConfig)
            {
                ReadCSVFiles = true,
                MessageOutput = CSVToSQLImportParameters.MessageOutput
            };

            var syncConnector = new CSVToSQLScript(globals);
            var mediaStoreController = new MediaStoreConnector(globals);

            try
            {
                var syncTask = await syncConnector.CSVImport(request);
                syncTask.MessageOutput = request.MessageOutput;

                result = syncTask.ResultState;
                if (result.GUID == syncConnector.Globals.LState_Error.GUID)
                {
                    return result;
                }

                var resultZipTask = await syncConnector.ZipScripts(syncTask);

                result = resultZipTask.ResultState;
                if (result.GUID == syncConnector.Globals.LState_Error.GUID)
                {
                    return result;
                }

                result = syncTask.ResultState;


                var fileName = mediaStoreController.GetFileName(syncTask.RootConfig.Name);

                var filePath = Path.Combine(CSVToSQLImportParameters.DownloadPath, $"{fileName}.zip");
                CSVToSQLImportParameters.MessageOutput?.OutputInfo($"Copy Zipfile to {filePath}");
                using (var streamWriter = new System.IO.FileStream(filePath, System.IO.FileMode.CreateNew))
                {
                    resultZipTask.Result.CopyTo(streamWriter);
                }

                if (result.GUID == globals.LState_Success.GUID)
                {
                    result.Additional1 = $"Created";
                    CSVToSQLImportParameters.MessageOutput?.OutputInfo(result.Additional1);
                }
                else
                {
                    CSVToSQLImportParameters.MessageOutput?.OutputError(result.Additional1);
                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new RequestInsertInto(CSVToSQLImportParameters.IdConfig)
            {
                ReadCSVFiles = true,
                MessageOutput = CSVToSQLImportParameters.MessageOutput
            };

            var syncConnector = new CSVToSQLScript(globals);
            var mediaStoreController = new MediaStoreConnector(globals);

            try
            {
                var syncTask = syncConnector.CSVImport(request);
                syncTask.Wait();

                result = syncTask.Result.ResultState;
                if (result.GUID == syncConnector.Globals.LState_Error.GUID)
                {
                    return result;
                }
                syncTask.Result.MessageOutput = request.MessageOutput;
                var resultZipTask = syncConnector.ZipScripts(syncTask.Result);
                resultZipTask.Wait();

                result = resultZipTask.Result.ResultState;
                if (result.GUID == syncConnector.Globals.LState_Error.GUID)
                {
                    CSVToSQLImportParameters.MessageOutput?.OutputInfo(result.Additional1);
                    return result;
                }

                result = syncTask.Result.ResultState;

                
                var fileName = mediaStoreController.GetFileName(syncTask.Result.RootConfig.Name);
                var filePath = Path.Combine(CSVToSQLImportParameters.DownloadPath, $"{fileName}.zip");
                CSVToSQLImportParameters.MessageOutput?.OutputInfo($"Copy Zipfile to {filePath}");
                using (var streamWriter = new System.IO.FileStream(filePath, System.IO.FileMode.CreateNew))
                {
                    resultZipTask.Result.Result.CopyTo(streamWriter);
                }

                if (result.GUID == globals.LState_Success.GUID)
                {
                    result.Additional1 = $"Created";
                }
                else
                {
                    CSVToSQLImportParameters.MessageOutput?.OutputError(result.Additional1);
                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public CSVToSQLController(CSVToSQLImportParam syncParam, Globals globals) : base(globals)
        {
            CSVToSQLImportParameters = syncParam;
            IsValid = true;
            var validationResult = CSVToSQLImportParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public CSVToSQLController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            IsValid = true;
            CSVToSQLImportParameters = new CSVToSQLImportParam
            {
                MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
            };

            var validateSyncResult = CSVToSQLImportParameters.ValidateParam(commandLine, globals);
            IsValid = (validateSyncResult.GUID == globals.LState_Success.GUID);
            ErrorMessage += validateSyncResult.Additional1;
            if (string.IsNullOrEmpty(ErrorMessage))
            {
                CSVToSQLImportParameters.MessageOutput?.OutputError(ErrorMessage);
            }
        }

        public CSVToSQLController() { }
    }
}
