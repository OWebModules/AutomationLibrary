﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OntoMsg_Module.Logging;
using JiraConnectorModule.Models;

namespace AutomationLibrary.Controller
{
    
    public class JiraSyncController : BaseController, IModuleController
    {
        public enum ModuleFunction
        {
            SyncIssues,
            SyncGitComments
        }

        public ModuleFunction Function { get; set; }

        public SyncJiraIssuesParam SyncJiraIssuesParameters { get; private set; }
        public SyncGitCommentsParam SyncGitCommentsParameters { get; private set; }


        public override bool IsResponsible(string module)
        { 
            return module.ToLower() == nameof(JiraConnectorModule).ToLower();  
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();

                sbSyntax.AppendLine($"{nameof(JiraConnectorModule)} Function:{nameof(ModuleFunction.SyncIssues)} IdConfig:<Id> IdUserName:<Id> MasterPassword:<Password>");
                sbSyntax.AppendLine($"{nameof(JiraConnectorModule)} Function:{nameof(ModuleFunction.SyncGitComments)} IdConfig:<Id> IdUserName:<Id> MasterPassword:<Password> IdGitConfig:<Id> RegexIssueId:<Regex>");
                return sbSyntax.ToString();
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_JiraConnectorModule;

        public List<Type> Functions => new List<Type> { typeof(SyncJiraIssuesParam), typeof(SyncGitCommentsParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {

            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new JiraConnectorModule.JiraController(globals);


            try
            {
                switch (Function)
                {
                    case ModuleFunction.SyncIssues:
                        var request = new JiraConnectorModule.Models.SyncIssuesRequest(SyncJiraIssuesParameters.IdConfig, SyncJiraIssuesParameters.IdMasterUser, SyncJiraIssuesParameters.MasterPassword)
                        {
                            MessageOutput = SyncJiraIssuesParameters.MessageOutput
                        };

                        var syncTask = await syncConnector.SyncJiraIssues(request);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Synced Jira-Issues";


                        }
                        break;
                    case ModuleFunction.SyncGitComments:
                        var syncGitCommentsRequest = new SyncGitCommentsRequest(SyncGitCommentsParameters.IdConfig, SyncGitCommentsParameters.IdMasterUser, SyncGitCommentsParameters.MasterPassword)
                        {
                            MessageOutput = SyncGitCommentsParameters.MessageOutput
                        };

                        var syncTaskComments = await syncConnector.SyncGitComments(syncGitCommentsRequest);

                        result = syncTaskComments.ResultState;

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            SyncGitCommentsParameters.MessageOutput?.OutputError(result.Additional1);
                        }

                        break;
                }

                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }


        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            
            var syncConnector = new JiraConnectorModule.JiraController(globals);
            

            try
            {
                switch (Function)
                {
                    case ModuleFunction.SyncIssues:
                        var request = new JiraConnectorModule.Models.SyncIssuesRequest(SyncJiraIssuesParameters.IdConfig, SyncJiraIssuesParameters.IdMasterUser, SyncJiraIssuesParameters.MasterPassword)
                        {
                            MessageOutput = SyncJiraIssuesParameters.MessageOutput
                        };

                        var syncTask = syncConnector.SyncJiraIssues(request);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Synced Jira-Issues";


                        }
                        break;
                    case ModuleFunction.SyncGitComments:
                        var syncGitCommentsRequest = new SyncGitCommentsRequest(SyncGitCommentsParameters.IdConfig, SyncGitCommentsParameters.IdMasterUser, SyncGitCommentsParameters.MasterPassword)
                        {
                            MessageOutput = SyncGitCommentsParameters.MessageOutput
                        };

                        var syncTaskComments = syncConnector.SyncGitComments(syncGitCommentsRequest);
                        syncTaskComments.Wait();

                        result = syncTaskComments.Result.ResultState;

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            SyncGitCommentsParameters.MessageOutput?.OutputError(result.Additional1);
                        }

                        break;
                }
                
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }
        public JiraSyncController(SyncJiraIssuesParam syncParam, Globals globals) : base(globals)
        {
            SyncJiraIssuesParameters = syncParam;
            IsValid = true;
            Function = ModuleFunction.SyncIssues;
            var validationResult = SyncJiraIssuesParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public JiraSyncController(SyncGitCommentsParam syncParam, Globals globals) : base(globals)
        {
            SyncGitCommentsParameters = syncParam;
            IsValid = true;
            Function = ModuleFunction.SyncGitComments;
            var validationResult = SyncGitCommentsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public JiraSyncController(CommandLine commandLine, Globals globals) : base(globals,commandLine)
        {
            
            IsValid = true;

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You must provide a Function";

            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == nameof(ModuleFunction.SyncIssues).ToLower())
                {
                    Function = ModuleFunction.SyncIssues;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == nameof(ModuleFunction.SyncGitComments).ToLower())
                {
                    Function = ModuleFunction.SyncGitComments;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += "You must provide a valid Function";
                }
                    

            }

            if (Function == ModuleFunction.SyncGitComments)
            {
                SyncGitCommentsParameters = new SyncGitCommentsParam
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };

                var gitValidationResult = SyncGitCommentsParameters.ValidateParam(commandLine, globals);
                if (gitValidationResult.GUID == globals.LState_Error.GUID)
                {
                    IsValid = false;
                    ErrorMessage = gitValidationResult.Additional1;
                }
            }
            else if (Function == ModuleFunction.SyncIssues)
            {
                SyncJiraIssuesParameters = new SyncJiraIssuesParam
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };

                var issueValidationResult = SyncJiraIssuesParameters.ValidateParam(commandLine, globals);
                if (issueValidationResult.GUID == globals.LState_Error.GUID)
                {
                    IsValid = false;
                    ErrorMessage = issueValidationResult.Additional1;
                }
            }
        }

        public JiraSyncController() { }

    }
}
