﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TFSConnectorModule;
using WrikeConnectorModule;
using WrikeConnectorModule.Models;

namespace AutomationLibrary.Controller
{
    public enum Function
    {
        SyncIssues = 0,
        SyncTFSComments = 1,
        SyncGitComments = 2
    }

    public class WrikeSyncController : BaseController, IModuleController
    {
        

        public Function Function { get; private set; }

        public WrikeSyncIssuesParam WrikeSyncIssuesParameters { get; private set; }

        public string IdTFSConfig { get; private set; }

        public string IdUser { get; private set; }

        public string IdGroup { get; private set; }

        public string IdGitConfig { get; set; }

        public string RegexIssueId { get; set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(WrikeConnectorModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"{nameof(WrikeConnectorModule)} {nameof(CommandLine.IdConfig)}:<Id> Function:{nameof(Function.SyncIssues)}");
                sbSyntax.AppendLine($"{nameof(WrikeConnectorModule)} {nameof(CommandLine.IdConfig)}:<Id> Function:{nameof(Function.SyncTFSComments)} {nameof(IdTFSConfig)}:<Id> {nameof(IdUser)}:<Id> {nameof(IdGroup)}:<Id>");
                sbSyntax.AppendLine($"{nameof(WrikeConnectorModule)} {nameof(CommandLine.IdConfig)}:<Id> Function:{nameof(Function.SyncGitComments)} {nameof(IdGitConfig)}:<Id> {nameof(IdUser)}:<Id> {nameof(IdGroup)}:<Id>");
                return sbSyntax.ToString();
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_WrikeConnectorModule;

        public List<Type> Functions => new List<Type> { typeof(WrikeSyncIssuesParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            
            var syncConnector = new WrikeConnector(globals);

            try
            {
                switch (Function)
                {
                    case Function.SyncIssues:
                        var request = new SyncIssuesRequest(WrikeSyncIssuesParameters.IdConfig)
                        {
                            MessageOutput = WrikeSyncIssuesParameters.MessageOutput
                        };
                        var syncTask = await syncConnector.SyncTasks(request);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Synced {syncTask.Result.Tasks.Count} Tasks";

                        }
                        return result;
                    case Function.SyncTFSComments:
                        var tfsConnector = new TFSConnector(globals);
                        var syncTfsRequest = new TFSConnectorModule.Models.SyncTFSRequest(IdTFSConfig);

                        var syncResult = await tfsConnector.SyncTFS(syncTfsRequest);

                        result = syncResult.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var commentRequest = new TFSConnectorModule.Models.GetTFSCommentsRequest(IdTFSConfig) { RegexComment = "#[A-Z0-9]{16}" };
                        var getResult = await tfsConnector.GetTFSComments(commentRequest);

                        result = getResult.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        if (getResult.Result.ChangeSets.Any())
                        {
                            var syncRequest = new WrikeConnectorModule.Models.SyncIssueCommentsRequest(CommandLine.IdConfig,
                                getResult.Result.ChangeSets.Select(changeSet => new IssueComment
                                {
                                    Commit = changeSet.ChangeSet,
                                    Comment = changeSet.Description.Val_String,
                                    CreateDate = changeSet.CreateDate.Val_Date.Value,
                                    Url = changeSet.Url.Name
                                }).ToList());

                            var syncCommentsTask = syncConnector.SyncSourceCodeComments(syncRequest);
                            syncCommentsTask.Wait();
                        }

                        return result;
                    case Function.SyncGitComments:
                        var gitConnector = new GitConnectorModule.GitConnector(globals);

                        var syncGitProjectsRequest = new GitConnectorModule.Models.SyncGitProjectsRequest(IdGitConfig);

                        var syncGitProjects = await gitConnector.SyncGitProjects(syncGitProjectsRequest);

                        if (syncGitProjects.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = globals.LState_Error.Clone();
                            result.Additional1 = syncGitProjects.ResultState.Additional1;
                            return result;
                        }

                        var getGitCommitsRequest = new GitConnectorModule.Models.GetGitCommitsRequest(IdGitConfig)
                        {
                            CommitTextFilterRegex = new Regex(@"#[A-Z0-9]{16}")
                        };

                        var getGitCommits = await gitConnector.GetGitCommits(getGitCommitsRequest);

                        if (getGitCommits.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = globals.LState_Error.Clone();
                            result.Additional1 = getGitCommits.ResultState.Additional1;
                            return result;
                        }



                        var issueComments = (from commitToProject in getGitCommits.Result.GitCommitsToProjects
                                             join commitMessage in getGitCommits.Result.CommitMessages on commitToProject.ID_Object equals commitMessage.ID_Object
                                             join commitDateTime in getGitCommits.Result.CommitDateTimes on commitToProject.ID_Object equals commitDateTime.ID_Object
                                             select new IssueComment
                                             {
                                                 Commit = new clsOntologyItem
                                                 {
                                                     GUID = commitToProject.ID_Object,
                                                     Name = commitToProject.Name_Object,
                                                     GUID_Parent = commitToProject.ID_Parent_Object,
                                                     Type = globals.Type_Object
                                                 },
                                                 CreateDate = commitDateTime.Val_Date.Value,
                                                 Comment = commitMessage.Val_String
                                             }).ToList();

                        if (issueComments.Any())
                        {
                            var syncRequestGitComments = new SyncIssueCommentsRequest(CommandLine.IdConfig, issueComments);

                            var syncCommentsTask = await syncConnector.SyncSourceCodeComments(syncRequestGitComments);

                            result = syncCommentsTask.ResultState;
                        }

                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Synced Git-comments to Jira";


                        }

                        return result;
                    default:
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "No valid Function provided!";
                        return result;
                }

            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }



        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            
            var syncConnector = new WrikeConnector(globals);

            try
            {
                switch(Function)
                {
                    case Function.SyncIssues:
                        var request = new SyncIssuesRequest(WrikeSyncIssuesParameters.IdConfig)
                        {
                            MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                        };
                        var syncTask = syncConnector.SyncTasks(request);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Synced {syncTask.Result.Result.Tasks.Count} Tasks";

                        }
                        return result;
                    case Function.SyncTFSComments:
                        var tfsConnector = new TFSConnector(globals);
                        var syncTfsRequest = new TFSConnectorModule.Models.SyncTFSRequest(IdTFSConfig);

                        var syncResult = tfsConnector.SyncTFS(syncTfsRequest);
                        syncResult.Wait();

                        result = syncResult.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var commentRequest = new TFSConnectorModule.Models.GetTFSCommentsRequest(IdTFSConfig) { RegexComment = "#[A-Z0-9]{16}" };
                        var getResult = tfsConnector.GetTFSComments(commentRequest);
                        getResult.Wait();

                        result = getResult.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        if (getResult.Result.Result.ChangeSets.Any())
                        {
                            var syncRequest = new WrikeConnectorModule.Models.SyncIssueCommentsRequest(CommandLine.IdConfig,
                                getResult.Result.Result.ChangeSets.Select(changeSet => new IssueComment
                                {
                                    Commit = changeSet.ChangeSet,
                                    Comment = changeSet.Description.Val_String,
                                    CreateDate = changeSet.CreateDate.Val_Date.Value,
                                    Url = changeSet.Url.Name
                                }).ToList());

                            var syncCommentsTask = syncConnector.SyncSourceCodeComments(syncRequest);
                            syncCommentsTask.Wait();
                        }

                        return result;
                    case Function.SyncGitComments:
                        var gitConnector = new GitConnectorModule.GitConnector(globals);

                        var syncGitProjectsRequest = new GitConnectorModule.Models.SyncGitProjectsRequest(IdGitConfig);

                        var syncGitProjects = gitConnector.SyncGitProjects(syncGitProjectsRequest);
                        syncGitProjects.Wait();

                        if (syncGitProjects.Result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = globals.LState_Error.Clone();
                            result.Additional1 = syncGitProjects.Result.ResultState.Additional1;
                            return result;
                        }

                        var getGitCommitsRequest = new GitConnectorModule.Models.GetGitCommitsRequest(IdGitConfig)
                        {
                            CommitTextFilterRegex = new Regex(@"#[A-Z0-9]{16}")
                        };

                        var getGitCommits = gitConnector.GetGitCommits(getGitCommitsRequest);
                        getGitCommits.Wait();

                        if (getGitCommits.Result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = globals.LState_Error.Clone();
                            result.Additional1 = getGitCommits.Result.ResultState.Additional1;
                            return result;
                        }



                        var issueComments = (from commitToProject in getGitCommits.Result.Result.GitCommitsToProjects
                                             join commitMessage in getGitCommits.Result.Result.CommitMessages on commitToProject.ID_Object equals commitMessage.ID_Object
                                             join commitDateTime in getGitCommits.Result.Result.CommitDateTimes on commitToProject.ID_Object equals commitDateTime.ID_Object
                                             select new IssueComment
                                             {
                                                 Commit = new clsOntologyItem
                                                 {
                                                     GUID = commitToProject.ID_Object,
                                                     Name = commitToProject.Name_Object,
                                                     GUID_Parent = commitToProject.ID_Parent_Object,
                                                     Type = globals.Type_Object
                                                 },
                                                 CreateDate = commitDateTime.Val_Date.Value,
                                                 Comment = commitMessage.Val_String
                                             }).ToList();

                        if (issueComments.Any())
                        {
                            var syncRequestGitComments = new SyncIssueCommentsRequest(CommandLine.IdConfig, issueComments);

                            var syncCommentsTask = syncConnector.SyncSourceCodeComments(syncRequestGitComments);
                            syncCommentsTask.Wait();

                            result = syncCommentsTask.Result.ResultState;
                        }
                        
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Synced Git-comments to Jira";


                        }

                        return result;
                    default:
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "No valid Function provided!";
                        return result;
                }
                
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }



        }

        public WrikeSyncController(WrikeSyncIssuesParam parameters, Globals globals) : base(globals)
        {

            IsValid = true;

            Function = Function.SyncIssues;
            WrikeSyncIssuesParameters = parameters;
            var validationResult = WrikeSyncIssuesParameters.ValidateParam(globals);
            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
            return;

        }

        public WrikeSyncController(CommandLine commandLine, Globals globals) :base(globals,commandLine)
        {
            IsValid = true;
            if (string.IsNullOrEmpty(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid!";
                IsValid = false;
                return;
            }

            if (!globals.is_GUID(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid (no GUID)!";
                IsValid = false;
                return;
            }

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());
            if (key == null)
            {
                IsValid = false;
                ErrorMessage += $"You have to provide a Function: [{nameof(Function.SyncIssues)}|{nameof(Function.SyncTFSComments)}]\n";

            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == nameof(Function.SyncIssues).ToLower())
                {
                    Function = Function.SyncIssues;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == nameof(Function.SyncTFSComments).ToLower())
                {
                    Function = Function.SyncTFSComments;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == nameof(Function.SyncGitComments).ToLower())
                {
                    Function = Function.SyncGitComments;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += $"You have to provide a Function: [{nameof(Function.SyncIssues)}|{nameof(Function.SyncTFSComments)}]\n";
                }

            }

            if (Function == Function.SyncIssues)
            {
                WrikeSyncIssuesParameters = new WrikeSyncIssuesParam
                {
                    IdConfig = CommandLine.IdConfig,
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
            }
            else if (Function == Function.SyncTFSComments)
            {
                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdTFSConfig).ToLower());
                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += $"You have to provide a Id for the config of TFS-Sync\n";

                }
                else
                {
                    IdTFSConfig = commandLine.AdditionalParameters[key];
                    if (!globals.is_GUID(IdTFSConfig))
                    {
                        ErrorMessage = "IdTFSConfig is invalid (no GUID)!";
                        IsValid = false;
                        return;
                    }

                }

                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdUser).ToLower());
                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += $"You have to provide a Id for a user to tag issues with changesets\n";

                }
                else
                {
                    IdUser = commandLine.AdditionalParameters[key];
                    if (!globals.is_GUID(IdUser))
                    {
                        ErrorMessage = "IdUser is invalid (no GUID)!";
                        IsValid = false;
                        return;
                    }

                }

                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdGroup).ToLower());
                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += $"You have to provide a Id for a group to tag issues with changesets\n";

                }
                else
                {
                    IdGroup = commandLine.AdditionalParameters[key];
                    if (!globals.is_GUID(IdGroup))
                    {
                        ErrorMessage = "IdGroup is invalid (no GUID)!";
                        IsValid = false;
                        return;
                    }

                }
            }
            else if (Function == Function.SyncGitComments)
            {
                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdGitConfig).ToLower());
                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += $"You have to provide a Id for the config of Git-Sync\n";

                }
                else
                {
                    IdGitConfig = commandLine.AdditionalParameters[key];
                    if (!globals.is_GUID(IdGitConfig))
                    {
                        ErrorMessage = "IdGitConfig is invalid (no GUID)!";
                        IsValid = false;
                        return;
                    }

                }

                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(RegexIssueId).ToLower());
                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += $"You have to provide a regular expression to recognize the git-comments\n";

                }
                else
                {
                    RegexIssueId = commandLine.AdditionalParameters[key];
                    try
                    {
                        var regex = new Regex(RegexIssueId);
                    }
                    catch (Exception ex)
                    {

                        IsValid = false;
                        ErrorMessage += $"The regular expression is not valid: {ex.Message}";
                    }

                }

            }
            
        }

        public WrikeSyncController() { }
    }
}
