﻿using GitConnectorModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    public class GitController : BaseController, IModuleController
    {
  
        public SyncGitProjectsParam SyncGitProjectsParameters { get; set; }
   
        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(GitConnectorModule).ToLower(); 
        }

        public override string Syntax
        {
            get
            {
                return $"{nameof(GitConnectorModule)} {nameof(SyncGitProjectsParam.IdConfig)}:<Id>";
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_GitConnectorModule;

        public List<Type> Functions => new List<Type> { typeof(SyncGitProjectsParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new SyncGitProjectsRequest(SyncGitProjectsParameters.IdConfig)
            {
                MessageOutput = SyncGitProjectsParameters.MessageOutput
            };
            var syncConnector = new GitConnectorModule.GitConnector(globals);

            try
            {
                var syncTask = await syncConnector.SyncGitProjects(request);

                result = syncTask.ResultState;
                if (result.GUID == globals.LState_Success.GUID)
                {
                    result.Additional1 = $"Synced Git-Projects";


                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new SyncGitProjectsRequest(SyncGitProjectsParameters.IdConfig)
            {
                MessageOutput = SyncGitProjectsParameters.MessageOutput
            };

            var syncConnector = new GitConnectorModule.GitConnector(globals);

            try
            {
                var syncTask = syncConnector.SyncGitProjects(request);
                syncTask.Wait();

                result = syncTask.Result.ResultState;
                if (result.GUID == globals.LState_Success.GUID)
                {
                    result.Additional1 = $"Synced Git-Projects";


                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }
        public GitController(SyncGitProjectsParam syncParam, Globals globals) : base(globals)
        {
            SyncGitProjectsParameters = syncParam;
            IsValid = true;
            var validationResult = SyncGitProjectsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }
        public GitController(CommandLine commandLine, Globals globals) : base(globals,commandLine)
        {
            
            IsValid = true;
            if (string.IsNullOrEmpty(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid!";
                IsValid = false;
                return;
            }

            if (!globals.is_GUID(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid (no GUID)!";
                IsValid = false;
                return;
            }

            SyncGitProjectsParameters = new SyncGitProjectsParam
            {
                IdConfig = commandLine.IdConfig,
                MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
            };

            var validateRelease = SyncGitProjectsParameters.ValidateParam(globals);
            IsValid = (validateRelease.GUID == globals.LState_Error.GUID);
            ErrorMessage = validateRelease.Additional1;
        }

        
        public GitController() { }
    }
}
