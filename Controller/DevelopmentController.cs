﻿using GitConnectorModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{

    public class DevelopmentController : BaseController
    {
    
   
        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(DevelopmentModule).ToLower();   
        }

        public override string Syntax
        {
            get
            {
                return $"{nameof(DevelopmentModule)} IdConfig:<Id>";
            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new SyncGitProjectsRequest(CommandLine.IdConfig);
            var syncConnector = new GitConnectorModule.GitConnector(globals);

            try
            {
                var syncTask = syncConnector.SyncGitProjects(request);
                syncTask.Wait();

                result = syncTask.Result.ResultState;
                if (result.GUID == globals.LState_Success.GUID)
                {
                    result.Additional1 = $"Synced Git-Projects";


                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public DevelopmentController(CommandLine commandLine, Globals globals) : base(globals,commandLine)
        {
            
            IsValid = true;
            if (string.IsNullOrEmpty(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid!";
                IsValid = false;
                return;
            }

            if (!globals.is_GUID(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid (no GUID)!";
                IsValid = false;
                return;
            }
            

        }

        public DevelopmentController() { }

    }
}
