﻿using GitConnectorModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{

    public class FacebookConnector : BaseController
    {
        public enum ModuleFunction
        {
            SyncMyPosts
        }

        public ModuleFunction Function { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(FacebookConnectorModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                return $"Module:{nameof(FacebookConnectorModule)} Function:[{nameof(ModuleFunction.SyncMyPosts)}] IdConfig:<Id>";
            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new FacebookConnectorModule.Models.FacebookPostQueryRequest(CommandLine.IdConfig);
            var syncConnector = new FacebookConnectorModule.FacebookController(globals);

            try
            {
                switch(Function)
                {
                    case ModuleFunction.SyncMyPosts:
                        var syncTask = syncConnector.QueryPosts(request);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Synced Git-Projects";


                        }
                        break;
                }
                
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public FacebookConnector(CommandLine commandLine, Globals globals) : base(globals,commandLine)
        {
            
            IsValid = true;
            if (string.IsNullOrEmpty(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid!";
                IsValid = false;
                return;
            }

            if (!globals.is_GUID(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid (no GUID)!";
                IsValid = false;
                return;
            }

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You must provide a Function";

            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == nameof(ModuleFunction.SyncMyPosts).ToLower())
                {
                    Function = ModuleFunction.SyncMyPosts;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += "You must provide a valid Function";
                }


            }
        }

        
        public FacebookConnector() { }
    }
}
