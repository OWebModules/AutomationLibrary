﻿using GitConnectorModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntoMsg_Module.Logging;
using DatabaseManagementModule.Models;

namespace AutomationLibrary.Controller
{

    public enum DatabaseManagementControllerFunction
    {
        CheckMsConnectionString = 0,
        CheckExistanceDbItems = 1,
        GetDBItemsCode = 2,
        GetColumns = 3
    }
    public class DatabaseManagementController : BaseController, IModuleController
    {


        public CheckMsConnectionStringParam CheckMsConnectionStringParameters { get; private set; }
        public CheckExistanceDBItemsParam CheckExistanceDBItemsParameters { get; private set; }

        public GetDBItemsCodeParam GetDBItemsCodeParameters { get; private set; }

        public GetColumnsParam GetColumnsParameters { get; private set; }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_DatabaseManagementModule;

        public DatabaseManagementControllerFunction Function { get; private set; }

        public List<Type> Functions => new List<Type> 
        { 
            typeof(CheckMsConnectionStringParam),
            typeof(CheckExistanceDBItemsParam),
            typeof(GetDBItemsCodeParam),
            typeof(GetColumnsParam)
        };

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"{nameof(DatabaseManagementModule)} {nameof(Function)}:{nameof(DatabaseManagementControllerFunction.CheckMsConnectionString)} {nameof(CheckMsConnectionStringParam.IdConnectionString)}:<Id>");
                sbSyntax.AppendLine($"{nameof(DatabaseManagementModule)} {nameof(Function)}:{nameof(DatabaseManagementControllerFunction.CheckExistanceDbItems)} {nameof(CheckExistanceDBItemsParam.IdConfig)}:<Id> {nameof(CheckExistanceDBItemsParameters.MasterPassword)}:<Master-Password>");
                sbSyntax.AppendLine($"{nameof(DatabaseManagementModule)} {nameof(Function)}:{nameof(DatabaseManagementControllerFunction.GetDBItemsCode)} {nameof(GetDBItemsCodeParam.IdConfig)}:<Id> {nameof(GetDBItemsCodeParam.MasterPassword)}:<Master-Password>");
                sbSyntax.AppendLine($"{nameof(DatabaseManagementModule)} {nameof(Function)}:{nameof(DatabaseManagementControllerFunction.GetColumns)} {nameof(GetColumnsParam.IdDbItem)}:<Id> {nameof(GetDBItemsCodeParam.MasterPassword)}:<Master-Password>");
                return sbSyntax.ToString();
            }
        }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(DatabaseManagementModule).ToLower();
        }
        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var dbController = new DatabaseManagementModule.DatabaseManagementController(globals);

            try
            {
                switch(Function)
                {
                    case DatabaseManagementControllerFunction.CheckMsConnectionString:
                        var request = new CheckConnectionStringRequest(CheckMsConnectionStringParameters.IdConnectionString)
                        {
                            MessageOutput = CheckMsConnectionStringParameters.MessageOutput
                        };
                        
                        var syncTask = await dbController.CheckMSConnectionString(request);

                        result = syncTask;
                        if (result.GUID == dbController.Globals.LState_Error.GUID)
                        {
                            CheckMsConnectionStringParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }
                        break;
                        
                    case DatabaseManagementControllerFunction.CheckExistanceDbItems:
                        var checkRequest = new CheckExistanceDBItemsRequest(CheckExistanceDBItemsParameters.IdConfig, CheckExistanceDBItemsParameters.MasterPassword, CheckExistanceDBItemsParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = CheckExistanceDBItemsParameters.MessageOutput
                        };
                        
                        result = await dbController.CheckExistanceDBItems(checkRequest);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            CheckExistanceDBItemsParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }
                        break;

                    case DatabaseManagementControllerFunction.GetDBItemsCode:
                        var getRequest = new GetMSSqlCodeRequest(GetDBItemsCodeParameters.IdConfig, GetDBItemsCodeParameters.MasterPassword, GetDBItemsCodeParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = GetDBItemsCodeParameters.MessageOutput
                        };
                        result = await dbController.GetMSSqlCode(getRequest);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            CheckExistanceDBItemsParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }
                        break;

                    case DatabaseManagementControllerFunction.GetColumns:
                        var getColumnsRequest = new GetColumnsRequest(GetColumnsParameters.IdDbItem, GetColumnsParameters.MasterPassword)
                        {
                            MessageOutput = GetColumnsParameters.MessageOutput
                        };
                        var getColumnsResult = await dbController.GetColumns(getColumnsRequest);
                        if (getColumnsResult.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            GetColumnsParameters.MessageOutput?.OutputError(getColumnsResult.ResultState.Additional1);
                            return result;
                        }
                        break;
                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            try
            {
                switch (Function)
                {
                    case DatabaseManagementControllerFunction.CheckMsConnectionString:
                        var request = new CheckConnectionStringRequest(CheckMsConnectionStringParameters.IdConnectionString)
                        {
                            MessageOutput = CheckMsConnectionStringParameters.MessageOutput
                        };
                        var syncConnector = new DatabaseManagementModule.DatabaseManagementController(globals);
                        var syncTask = syncConnector.CheckMSConnectionString(request);
                        syncTask.Wait();

                        result = syncTask.Result;
                        if (result.GUID == syncConnector.Globals.LState_Error.GUID)
                        {
                            CheckMsConnectionStringParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }
                        break;

                    case DatabaseManagementControllerFunction.CheckExistanceDbItems:
                        var checkRequest = new CheckExistanceDBItemsRequest(CheckExistanceDBItemsParameters.IdConfig, CheckExistanceDBItemsParameters.MasterPassword, CheckExistanceDBItemsParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = CheckExistanceDBItemsParameters.MessageOutput
                        };
                        var checkConnector = new DatabaseManagementModule.DatabaseManagementController(globals);
                        var syncTask1 = checkConnector.CheckExistanceDBItems(checkRequest);
                        syncTask1.Wait();
                        result = syncTask1.Result;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            CheckExistanceDBItemsParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }
                        break;

                    case DatabaseManagementControllerFunction.GetDBItemsCode:
                        var getRequest = new GetMSSqlCodeRequest(GetDBItemsCodeParameters.IdConfig, GetDBItemsCodeParameters.MasterPassword, GetDBItemsCodeParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = CheckExistanceDBItemsParameters.MessageOutput
                        };
                        var databaseConnector = new DatabaseManagementModule.DatabaseManagementController(globals);
                        var taskResult = databaseConnector.GetMSSqlCode(getRequest);
                        taskResult.Wait();
                        result = taskResult.Result;

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            CheckExistanceDBItemsParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }
                        break;
                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public DatabaseManagementController(CheckMsConnectionStringParam syncParam, Globals globals) : base(globals)
        {
            Function = DatabaseManagementControllerFunction.CheckMsConnectionString;
            CheckMsConnectionStringParameters = syncParam;
            IsValid = true;
            var validationResult = CheckMsConnectionStringParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public DatabaseManagementController(CheckExistanceDBItemsParam syncParam, Globals globals) : base(globals)
        {

            Function = DatabaseManagementControllerFunction.CheckExistanceDbItems;
            IsValid = true;
            CheckExistanceDBItemsParameters = syncParam;
            IsValid = true;
            var validationResult = CheckExistanceDBItemsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public DatabaseManagementController(GetDBItemsCodeParam syncParam, Globals globals) : base(globals)
        {
            Function = DatabaseManagementControllerFunction.GetDBItemsCode;
            IsValid = true;
            GetDBItemsCodeParameters = syncParam;
            IsValid = true;
            var validationResult = GetDBItemsCodeParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public DatabaseManagementController(GetColumnsParam syncParam, Globals globals) : base(globals)
        {
            Function = DatabaseManagementControllerFunction.GetColumns;
            IsValid = true;
            GetColumnsParameters = syncParam;
            IsValid = true;
            var validationResult = GetColumnsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public DatabaseManagementController(CommandLine commandLine, Globals globals) : base(globals,commandLine)
        {

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You must provide a function";
            }
            else
            {
                if (commandLine.AdditionalParameters[key] == nameof(DatabaseManagementControllerFunction.CheckMsConnectionString))
                {
                    Function = DatabaseManagementControllerFunction.CheckMsConnectionString;
                }
                else if (commandLine.AdditionalParameters[key] == nameof(DatabaseManagementControllerFunction.CheckExistanceDbItems))
                {
                    Function = DatabaseManagementControllerFunction.CheckExistanceDbItems;
                }
                else if (CommandLine.AdditionalParameters[key] == nameof(DatabaseManagementControllerFunction.GetDBItemsCode))
                {
                    Function = DatabaseManagementControllerFunction.GetDBItemsCode;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += "You must provide a valid function";
                }
            }

            if (Function == DatabaseManagementControllerFunction.CheckMsConnectionString)
            {
                CheckMsConnectionStringParameters = new CheckMsConnectionStringParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = CheckMsConnectionStringParameters.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;

            }
            else if (Function == DatabaseManagementControllerFunction.CheckExistanceDbItems)
            {
                CheckExistanceDBItemsParameters = new CheckExistanceDBItemsParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = CheckExistanceDBItemsParameters.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;
            }
            else if (Function == DatabaseManagementControllerFunction.GetDBItemsCode)
            {
                GetDBItemsCodeParameters = new GetDBItemsCodeParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = GetDBItemsCodeParameters.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;
            }
            else if (Function == DatabaseManagementControllerFunction.GetColumns)
            {
                GetColumnsParameters = new GetColumnsParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = GetColumnsParameters.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public DatabaseManagementController() { }

    }
}
