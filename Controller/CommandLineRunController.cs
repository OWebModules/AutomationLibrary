﻿using CommandLineRunModule.Models;
using ImportExport_Module;
using MediaStore_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutomationLibrary.Attributes;
using OntoMsg_Module.Logging;

namespace AutomationLibrary.Controller
{
    public class CommandLineRunController : BaseController, IModuleController
    {

        public enum ModuleFunction
        {
            AddVariable = 0,
            CreateVarValuesForCommandLine = 1
        }
        public CommandLineRunAddVariableParam AddVariablesParameters { get; private set; } = new CommandLineRunAddVariableParam();
        public CommandLineRunCreateVarValuesParam CreateVarValuesParameters { get; private set; } = new CommandLineRunCreateVarValuesParam();

        public ModuleFunction Function { get; private set; }
        
        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"{nameof(CommandLineRunModule)} {nameof(Function)}:{nameof(ModuleFunction.AddVariable)} {nameof(CommandLineRunAddVariableParam.IdCodeItem)}:<Id of Command Line or Code Snipplet>");
                sbSyntax.AppendLine($"{nameof(CommandLineRunModule)} {nameof(Function)}:{nameof(ModuleFunction.CreateVarValuesForCommandLine)} {nameof(CommandLineRunCreateVarValuesParam.IdCommandLineRun)}:<Id of Command Line Run item> { nameof(CommandLineRunCreateVarValuesParam.ExcelFilePath)}:<Path of CSV-File which contains the Variable-Value mapping");
                return sbSyntax.ToString();
            }
        }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(CommandLineRunModule).ToLower();
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_CommandLineRun_Module;

        public List<Type> Functions => new List<Type> { typeof(CommandLineRunAddVariableParam), typeof(CommandLineRunCreateVarValuesParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;
            var syncConnector = new CommandLineRunModule.CommandLineRunController(globals);

            switch (Function)
            {
                case ModuleFunction.AddVariable:
                    var request = new AddVariablesRequest(AddVariablesParameters.IdCodeItem)
                    {
                        MessageOutput = AddVariablesParameters.MessageOutput
                    };

                    

                    try
                    {
                        var syncTask = await syncConnector.AddVariables(request);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Additional1 = $"Count of Variables in Code: {syncTask.Result.Variables.Count}";


                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }
                    
                case ModuleFunction.CreateVarValuesForCommandLine:
                    var requestCreateVarValues = new CreateVarValuesRequest
                    {
                        IdCommandlineRun = CreateVarValuesParameters.IdCommandLineRun,
                        ExcelFilePath = CreateVarValuesParameters.ExcelFilePath,
                        SheetName = CreateVarValuesParameters.SheetName,
                        MessageOutput = CreateVarValuesParameters.MessageOutput,
                        ColIndexValues = CreateVarValuesParameters.ColIndexValues,
                        ColIndexVariables = CreateVarValuesParameters.ColIndexVariables,
                        RemoveHeader = CreateVarValuesParameters.RemoveHeader,
                        NoImport = CreateVarValuesParameters.NoImport,
                        ExportPath = CreateVarValuesParameters.ExportPath,
                    };

                    var syncTaskCreateVarValues = await syncConnector.CreateVarValues(requestCreateVarValues);

                    result = syncTaskCreateVarValues.ResultState;
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Additional1 = $"Count of Values: {syncTaskCreateVarValues.Result.ValueCount}";
                    return result;
            }

            return result;
            
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            

            var syncConnector = new CommandLineRunModule.CommandLineRunController(globals);

            try
            {

                switch(Function)
                {
                    case ModuleFunction.AddVariable:
                        var requestAddVariables = new AddVariablesRequest(AddVariablesParameters.IdCodeItem);
                        var syncTaskAddVariables = syncConnector.AddVariables(requestAddVariables);
                        syncTaskAddVariables.Wait();

                        result = syncTaskAddVariables.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Additional1 = $"Count of Variables in Code: {syncTaskAddVariables.Result.Result.Variables.Count}";
                        break;
                    case ModuleFunction.CreateVarValuesForCommandLine:
                        var requestCreateVarValues = new CreateVarValuesRequest
                        {
                            IdCommandlineRun = CreateVarValuesParameters.IdCommandLineRun,
                            ExcelFilePath = CreateVarValuesParameters.ExcelFilePath,
                            SheetName = CreateVarValuesParameters.SheetName,
                            MessageOutput = CreateVarValuesParameters.MessageOutput,
                            ColIndexValues = CreateVarValuesParameters.ColIndexValues,
                            ColIndexVariables = CreateVarValuesParameters.ColIndexVariables,
                            RemoveHeader = CreateVarValuesParameters.RemoveHeader,
                            NoImport = CreateVarValuesParameters.NoImport,
                            ExportPath = CreateVarValuesParameters.ExportPath
                        };

                        var syncTaskCreateVarValues = syncConnector.CreateVarValues(requestCreateVarValues);
                        syncTaskCreateVarValues.Wait();

                        result = syncTaskCreateVarValues.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Additional1 = $"Count of Values: {syncTaskCreateVarValues.Result.Result.ValueCount}";
                        break;
                }
                


                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public CommandLineRunController(CommandLineRunAddVariableParam syncParam, Globals globals) : base(globals)
        {
            AddVariablesParameters = syncParam;
            IsValid = true;
            Function = ModuleFunction.AddVariable;
            var validationResult = AddVariablesParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public CommandLineRunController(CommandLineRunCreateVarValuesParam syncParam, Globals globals) : base(globals)
        {
            CreateVarValuesParameters = syncParam;
            IsValid = true;
            Function = ModuleFunction.CreateVarValuesForCommandLine;
            var validationResult = CreateVarValuesParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public CommandLineRunController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            IsValid = true;

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You have to provide an Function\n";
            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == ModuleFunction.AddVariable.ToString().ToLower())
                {
                    Function = ModuleFunction.AddVariable;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == ModuleFunction.CreateVarValuesForCommandLine.ToString().ToLower())
                {
                    Function = ModuleFunction.CreateVarValuesForCommandLine;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a valid Function\n";
                }
            }

            switch(Function)
            {
                case ModuleFunction.AddVariable:
                    AddVariablesParameters.MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath);
                    var validateResultAddVariables = AddVariablesParameters.ValidateParam(commandLine, globals);
                    IsValid = (validateResultAddVariables.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateResultAddVariables.Additional1;
                    break;
                case ModuleFunction.CreateVarValuesForCommandLine:
                    CreateVarValuesParameters.MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath);
                    var validateResultCreateVarValues = CreateVarValuesParameters.ValidateParam(commandLine, globals);
                    IsValid = (validateResultCreateVarValues.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateResultCreateVarValues.Additional1;
                    break;
            }   
        }

        public CommandLineRunController() { }
    }
}
