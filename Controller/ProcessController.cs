﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using OntoMsg_Module.Models;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    public class ProcessController : BaseController, IModuleController
    {
        public enum ModuleFunction
        {
            ExportHtmlProcess
        }

        public ModuleFunction Function { get; private set; }

        public ExportHtmlProcessParam ExportHtmlProcessParamters { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(ProcessModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                return $"Module:{nameof(ProcessModule)} {nameof(Function)}:[{nameof(ModuleFunction.ExportHtmlProcess)}] {nameof(ExportHtmlProcessParamters.IdProcess)}:<Id of Process> [{nameof(ExportHtmlProcessParamters.FullPath)}:<Fullpath of Process>]";
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_Process_Module;

        public List<Type> Functions => new List<Type> { typeof(ExportHtmlProcessParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new ProcessModule.ProcessExportController(globals);

            IMessageOutput messageOutput = null;
            try
            {
                switch (Function)
                {
                    case ModuleFunction.ExportHtmlProcess:
                        messageOutput = ExportHtmlProcessParamters.MessageOutput;
                        var request = new ProcessModule.Models.ExportHtmlProcessRequest(ExportHtmlProcessParamters.IdProcess) { ProcessFullPath = ExportHtmlProcessParamters.FullPath, MessageOutput = ExportHtmlProcessParamters.MessageOutput };
                        var exportResult = await syncConnector.ExportHtmlProcess(request);

                        result = exportResult.ResultState;
                        break;
                }

                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                messageOutput?.OutputError(result.Additional1);
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            
            var syncConnector = new ProcessModule.ProcessExportController(globals);

            IMessageOutput messageOutput = null;
            try
            {
                switch(Function)
                {
                    case ModuleFunction.ExportHtmlProcess:
                        messageOutput = ExportHtmlProcessParamters.MessageOutput;
                        var request = new ProcessModule.Models.ExportHtmlProcessRequest(ExportHtmlProcessParamters.IdProcess) { ProcessFullPath = ExportHtmlProcessParamters.FullPath, MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath) };
                        var exportResult = syncConnector.ExportHtmlProcess(request);
                        exportResult.Wait();

                        result = exportResult.Result.ResultState;
                        break;
                }
                
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                messageOutput?.OutputError(result.Additional1);
                return result;

            }
        }

        public ProcessController(ExportHtmlProcessParam syncParam, Globals globals) : base(globals)
        {
            ExportHtmlProcessParamters = syncParam;
            IsValid = true;
            var validationResult = ExportHtmlProcessParamters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public ProcessController(CommandLine commandLine, Globals globals) : base(globals,commandLine)
        {
            
            IsValid = true;

            ExportHtmlProcessParamters = new ExportHtmlProcessParam
            {
                MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
            };

            var validationResult = ExportHtmlProcessParamters.ValidateParam(commandLine, globals);

            IsValid = (validationResult.GUID == globals.LState_Success.GUID);
            ErrorMessage = validationResult.Additional1;

            if (!IsValid)
            {
                ExportHtmlProcessParamters.MessageOutput.OutputError(ErrorMessage);
            }
            
        }


        public ProcessController() { }
    }
}
