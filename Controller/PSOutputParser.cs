﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using PsScriptOutputParserModule;
using PsScriptOutputParserModule.Models;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    public class PSOutputParser : BaseController, IModuleController
    {
       public PSOutputParserParam Parameters { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(PsScriptOutputParserModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                return $"{nameof(PsScriptOutputParserModule)} IdConfig:<Id> IdUser:<Id of Masteruser> Password:<Password of Masteruser>";
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_PsScriptOutputParserModule;

        public List<Type> Functions => new List<Type> { typeof(PSOutputParserParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var cancellationToken = new CancellationToken();
            var request = new ScriptOutputRequest(Parameters.IdConfig, Parameters.IdUser, Parameters.Password, cancellationToken)
            {
                MessageOutput = Parameters.MessageOutput
            };
            var syncConnector = new PsScriptOutputParserController(globals);

            try
            {
                var syncTask = await syncConnector.PsScriptOutput(request);

                result = syncTask.ResultState;
                request.MessageOutput?.OutputInfo(syncTask.Output);
                if (result.GUID == globals.LState_Success.GUID)
                {
                    result.Additional1 = $"{syncTask.Output}";
                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var cancellationToken = new CancellationToken();
            var request = new ScriptOutputRequest(Parameters.IdConfig, Parameters.IdUser, Parameters.Password, cancellationToken)
            {
                MessageOutput = Parameters.MessageOutput
            };

            var syncConnector = new PsScriptOutputParserController(globals);

            try
            {
                var syncTask = syncConnector.PsScriptOutput(request);
                syncTask.Wait();

                result = syncTask.Result.ResultState;
                request.MessageOutput?.OutputInfo(syncTask.Result.Output);
                if (result.GUID == globals.LState_Success.GUID)
                {
                    result.Additional1 = $"{syncTask.Result.Output}";


                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public PSOutputParser(PSOutputParserParam param, Globals globals) : base(globals)
        {
            IsValid = true;
            Parameters = param;
            var validationResult = Parameters.ValidateParam(globals);
            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public PSOutputParser(CommandLine commandLine, Globals globals) :base(globals,commandLine)
        {
            IsValid = true;
            Parameters = new PSOutputParserParam()
            {
                MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
            };

            var validationResult = Parameters.ValidateParam(commandLine, globals);
            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public PSOutputParser() { }
    }
}
