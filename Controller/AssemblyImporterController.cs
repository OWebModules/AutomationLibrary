﻿using ImportExport_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    public class AssemblyImporterController : BaseController
    {
        public string Path { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(AssemblyImporter).ToLower();
        }

        public override string Syntax
        {
            get
            {
                return $"{nameof(AssemblyImporterController)} Path:<Path>";
            }
        }

        


        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new ImportExport_Module.AssemblyImporter(globals);

            try
            {

                var checkTask = syncConnector.CheckAssembly(Path);
                checkTask.Wait();

                result = checkTask.Result.ResultState;

                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var importResult = syncConnector.ImportAssembly(checkTask.Result.AssemblyType);
                importResult.Wait();

                result = importResult.Result.ResultState;

                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Additional1 = "Successful imported Assembly";
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public AssemblyImporterController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            
            
            IsValid = true;
            ErrorMessage = "";
            if (commandLine.AdditionalParameters.Keys.Count != 1)
            {
                IsValid = false;
                ErrorMessage += "Only Path must be provided\n";
                return;
            }
            

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == "Path".ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You have to provide a Path to the Assembly\n";
                return;

            }

            var path = commandLine.AdditionalParameters[key];

            if (!System.IO.File.Exists(path))
            {
                IsValid = false;
                ErrorMessage += "The File does not exist\n";
            }

            try
            {
                var assembly = Assembly.LoadFile(path);
            }
            catch (Exception ex)
            {

                IsValid = false;
                ErrorMessage += $"The Assembly is not valid: {ex.Message}";
            }

            Path = path;


        }

        public AssemblyImporterController() { }
    }
}
