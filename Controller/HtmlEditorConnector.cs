﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    public class HtmlEditorConnector : BaseController, IModuleController
    {
        public enum ModuleFunction
        {
            ExportAnonymous,
            SyncHistoryEntries
        }


        public ModuleFunction Function { get; set; }

        public ExportAnonymousParam ExportAnonymousParameters { get; private set; }
        public SyncHtmlHistoryEntriesParam SyncHtmlHistoryEntriesParameters { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(HtmlEditorModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();

                sbSyntax.AppendLine($"{nameof(HtmlEditorModule)} Function:{nameof(ModuleFunction.ExportAnonymous)} ExportPath:<Path>");
                sbSyntax.AppendLine($"{nameof(HtmlEditorModule)} Function:{nameof(ModuleFunction.SyncHistoryEntries)}");
                return sbSyntax.ToString();
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_HTMLEdit_Module;

        public List<Type> Functions => new List<Type> { typeof(ExportAnonymousParam), typeof(SyncHtmlHistoryEntriesParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            

            try
            {
                switch (Function)
                {
                    case ModuleFunction.ExportAnonymous:
                        try
                        {
                            var syncConnector = new HtmlEditorModule.ExportHtmlDocController(globals);
                            var request = new HtmlEditorModule.Models.ExportAnonymousHtmlRequest(ExportAnonymousParameters.IdConfig) { MessageOutput = ExportAnonymousParameters.MessageOutput, ExportHtmlPack = ExportAnonymousParameters.ExportHtmlPack };

                            var syncTask = await syncConnector.ExportAnonymousHtml(request, false);
                            result = syncTask;
                            if (result.GUID == globals.LState_Success.GUID)
                            {
                                result.Additional1 = $"Exported Anonymous Html-objects";
                                request.MessageOutput?.OutputInfo(result.Additional1);

                            }
                        }
                        catch (Exception ex)
                        {

                            result = globals.LState_Error.Clone();
                            result.Additional1 = ex.Message;
                            ExportAnonymousParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }
                        break;
                        
                    case ModuleFunction.SyncHistoryEntries:
                        try
                        {
                            var syncHistoryController = new HtmlEditorModule.HtmlEditorConnector(globals);
                            var syncHistoryRequest = new HtmlEditorModule.Models.SyncHtmlStateRequest { MessageOutput = SyncHtmlHistoryEntriesParameters.MessageOutput };
                            var syncHistoryTask = await syncHistoryController.SyncHtmlState(syncHistoryRequest);

                            result = syncHistoryTask.ResultState;
                            if (result.GUID == globals.LState_Success.GUID)
                            {
                                result.Additional1 = $"Synced history-entries";
                                syncHistoryRequest.MessageOutput?.OutputInfo(result.Additional1);

                            }
                            else if (result.GUID == globals.LState_Nothing.GUID)
                            {
                                result.Additional1 = $"No entries to Sync";
                                syncHistoryRequest.MessageOutput?.OutputInfo(result.Additional1);
                            }
                        }
                        catch (Exception ex)
                        {

                            result = globals.LState_Error.Clone();
                            result.Additional1 = ex.Message;
                            ExportAnonymousParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }
                        
                        break;

                }

                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }
        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;


            var syncConnector = new HtmlEditorModule.ExportHtmlDocController(globals);


            try
            {
                switch (Function)
                {
                    case ModuleFunction.ExportAnonymous:
                        var request = new HtmlEditorModule.Models.ExportAnonymousHtmlRequest(ExportAnonymousParameters.IdConfig) { MessageOutput = ExportAnonymousParameters.MessageOutput, ExportHtmlPack = ExportAnonymousParameters.ExportHtmlPack };
                        var syncTask = syncConnector.ExportAnonymousHtml(request, false);
                        syncTask.Wait();

                        result = syncTask.Result;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Exported Anonymous Html-objects";


                        }
                        break;
                    case ModuleFunction.SyncHistoryEntries:
                        var syncHistoryController = new HtmlEditorModule.HtmlEditorConnector(globals);
                        var syncHistoryRequest = new HtmlEditorModule.Models.SyncHtmlStateRequest { MessageOutput = ExportAnonymousParameters.MessageOutput };
                        var syncHistoryTask = syncHistoryController.SyncHtmlState(syncHistoryRequest);
                        syncHistoryTask.Wait();

                        result = syncHistoryTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Synced history-entries";
                            syncHistoryRequest.MessageOutput?.OutputInfo(result.Additional1);

                        }
                        else if (result.GUID == globals.LState_Nothing.GUID)
                        {
                            result.Additional1 = $"No entries to Sync";
                            syncHistoryRequest.MessageOutput?.OutputInfo(result.Additional1);
                        }
                        break;

                }

                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public HtmlEditorConnector(ExportAnonymousParam parameters, Globals globals) : base(globals)
        {

            IsValid = true;

            Function = ModuleFunction.ExportAnonymous;
            ExportAnonymousParameters = parameters;
            var validationResult = ExportAnonymousParameters.ValidateParam(globals);
            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
            return;

        }

        public HtmlEditorConnector(SyncHtmlHistoryEntriesParam parameters, Globals globals) : base(globals)
        {

            IsValid = true;

            Function = ModuleFunction.SyncHistoryEntries;
            SyncHtmlHistoryEntriesParameters = parameters;
            var validationResult = SyncHtmlHistoryEntriesParameters.ValidateParam(globals);
            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
            return;

        }

        public HtmlEditorConnector(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {

            IsValid = true;

            if (string.IsNullOrEmpty(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid!";
                IsValid = false;
                return;
            }

            if (!globals.is_GUID(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid (no GUID)!";
                IsValid = false;
                return;
            }

            var parameterResult = commandLine.GetAdditionalParamter(nameof(GeminiSyncParam.Function));

            if (parameterResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage += "The Funciton is not valid!\n";
                return;
            }

            if (parameterResult.Additional1.ToLower() == ModuleFunction.ExportAnonymous.ToString().ToLower())
            {
                Function = ModuleFunction.ExportAnonymous;
            }
            else
            {
                IsValid = false;
                ErrorMessage += "The Function is not valid!";
            }

            switch (Function)
            {
                case ModuleFunction.ExportAnonymous:
                    ExportAnonymousParameters = new ExportAnonymousParam
                    {
                        IdConfig = CommandLine.IdConfig,
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    break;
                case ModuleFunction.SyncHistoryEntries:
                    SyncHtmlHistoryEntriesParameters = new SyncHtmlHistoryEntriesParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    break;
            }

        }

        public HtmlEditorConnector() { }
    }
}
