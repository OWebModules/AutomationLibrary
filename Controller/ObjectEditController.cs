﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyItemsModule.Models;
using OntoMsg_Module.Logging;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    public class ObjectEditController : BaseController, IModuleController
    {
        
        public enum ControllerFunction
        {
            CopyRelations = 0,
            MergeObjectsByItems = 1,
            DeleteObjects
        }

        public ControllerFunction Function { get; private set; }
        public CopyRelationsParam CopyRelationsParameters { get; private set; }
        public MergeObjectsByItemsParam MergeObjectsByItemsParameters { get; private set; }
        public DeleteObjectsParam DeleteObjectsParameters { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(OntologyItemsModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbResult = new StringBuilder();

                sbResult.AppendLine($"{nameof(CommandLine.Module)}:{nameof(OntologyItemsModule)} {nameof(Function)}:{nameof(ControllerFunction.CopyRelations)} {nameof(CopyRelationsParam.IdConfig)}:<Id>");
                sbResult.AppendLine($"{nameof(CommandLine.Module)}:{nameof(OntologyItemsModule)} {nameof(Function)}:{nameof(ControllerFunction.MergeObjectsByItems)} {nameof(CopyRelationsParam.IdConfig)}:<Id>");
                sbResult.AppendLine($"{nameof(CommandLine.Module)}:{nameof(OntologyItemsModule)} {nameof(Function)}:{nameof(ControllerFunction.DeleteObjects)} {nameof(CopyRelationsParam.IdConfig)}:<Id>");
                return sbResult.ToString();
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_OntologyItemsModule;

        public List<Type> Functions => new List<Type> 
        { 
            typeof(CopyRelationsParam), 
            typeof(MergeObjectsByItemsParam),
            typeof(DeleteObjectsParam)
        };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            

            try
            {

                switch (Function)
                {
                    case ControllerFunction.CopyRelations:
                        var syncConnector = new OntologyItemsModule.ObjectEditController(globals);
                        var requestCopy = new CopyRelationsRequest(CopyRelationsParameters.IdConfig)
                        {
                            MessageOutput = CopyRelationsParameters.MessageOutput
                        };
                        var syncTask = await syncConnector.CopyRelations(requestCopy);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            CopyRelationsParameters.MessageOutput?.OutputInfo($"Copied {syncTask.Result.CountAttributes} attributes, {syncTask.Result.CountRelations} relations.");
                        }
                        return result;
                    case ControllerFunction.MergeObjectsByItems:
                        var mergeController = new OntologyItemsModule.OItemListController(globals);
                        var requestMerge = new MergeObjectsRequestByItems(MergeObjectsByItemsParameters.IdConfig)
                        {
                            MessageOutput = MergeObjectsByItemsParameters.MessageOutput
                        };

                        var mergeResult = await mergeController.MergeObjects(requestMerge);
                        result = mergeResult.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            MergeObjectsByItemsParameters.MessageOutput?.OutputInfo($"Merged {mergeResult.Result.MergedObjects.Count} objects, {mergeResult.Result.MergedAttributes} attributes, {mergeResult.Result.MergedRelations} relations.");
                            MergeObjectsByItemsParameters.MessageOutput?.OutputInfo($"Deleted {mergeResult.Result.DeletedObjects.Count} objects, {mergeResult.Result.DeletedAttributes} attributes, {mergeResult.Result.DeletedRelations} relations.");
                        }
                        return result;
                    case ControllerFunction.DeleteObjects:
                        var deleteController = new OntologyItemsModule.OItemListController(globals);
                        var deleteRequest = new DeleteObjectsRequest(DeleteObjectsParameters.IdConfig)
                        {
                            MessageOutput = DeleteObjectsParameters.MessageOutput
                        };

                        var deleteResult = await deleteController.DeleteObjectsAndRelations(deleteRequest);
                        result = deleteResult.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            DeleteObjectsParameters.MessageOutput?.OutputInfo($"Deleted {deleteResult.Result.CountDeletedObjects} objects with {deleteResult.Result.CountDeletedAttributes} Attributes and {deleteResult.Result.CountDeletedRelations} Relations.");
                        }

                        return result;

                    default:
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "No valid Function provided!";
                        return result;
                }


            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new OntologyItemsModule.ObjectEditController(globals);

            try
            {

                switch (Function)
                {
                    case ControllerFunction.CopyRelations:
                        var requestCopy = new CopyRelationsRequest(CopyRelationsParameters.IdConfig)
                        {
                            MessageOutput = CopyRelationsParameters.MessageOutput
                        };
                        var syncTask = syncConnector.CopyRelations(requestCopy);

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            CopyRelationsParameters.MessageOutput?.OutputInfo($"Copied {syncTask.Result.Result.CountAttributes} attributes, {syncTask.Result.Result.CountRelations} relations.");
                        }
                        return result;
                    case ControllerFunction.MergeObjectsByItems:
                        var mergeController = new OntologyItemsModule.OItemListController(globals);
                        var requestMerge = new MergeObjectsRequestByItems(MergeObjectsByItemsParameters.IdConfig)
                        {
                            MessageOutput = MergeObjectsByItemsParameters.MessageOutput
                        };

                        var mergeResult = mergeController.MergeObjects(requestMerge);
                        mergeResult.Wait();
                        result = mergeResult.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            MergeObjectsByItemsParameters.MessageOutput?.OutputInfo($"Merged {mergeResult.Result.Result.MergedObjects.Count} objects, {mergeResult.Result.Result.MergedAttributes} attributes, {mergeResult.Result.Result.MergedRelations} relations.");
                            MergeObjectsByItemsParameters.MessageOutput?.OutputInfo($"Deleted {mergeResult.Result.Result.DeletedObjects.Count} objects, {mergeResult.Result.Result.DeletedAttributes} attributes, {mergeResult.Result.Result.DeletedRelations} relations.");
                        }
                        return result;
                    case ControllerFunction.DeleteObjects:
                        var deleteController = new OntologyItemsModule.OItemListController(globals);
                        var deleteRequest = new DeleteObjectsRequest(DeleteObjectsParameters.IdConfig)
                        {
                            MessageOutput = DeleteObjectsParameters.MessageOutput
                        };

                        var deleteResult = deleteController.DeleteObjectsAndRelations(deleteRequest);
                        deleteResult.Wait();
                        result = deleteResult.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            DeleteObjectsParameters.MessageOutput?.OutputInfo($"Deleted {deleteResult.Result.Result.CountDeletedObjects} objects with {deleteResult.Result.Result.CountDeletedAttributes} Attributes and {deleteResult.Result.Result.CountDeletedRelations} Relations.");
                        }

                        return result;
                    default:
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "No valid Function provided!";
                        return result;
                }


            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public ObjectEditController(CopyRelationsParam syncParam, Globals globals) : base(globals)
        {
            CopyRelationsParameters = syncParam;
            IsValid = true;
            Function = ControllerFunction.CopyRelations;
            var validationResult = CopyRelationsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public ObjectEditController(MergeObjectsByItemsParam syncParam, Globals globals) : base(globals)
        {
            MergeObjectsByItemsParameters = syncParam;
            IsValid = true;
            Function = ControllerFunction.MergeObjectsByItems;
            var validationResult = MergeObjectsByItemsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public ObjectEditController(DeleteObjectsParam syncParam, Globals globals) : base(globals)
        {
            DeleteObjectsParameters = syncParam;
            IsValid = true;
            Function = ControllerFunction.DeleteObjects;
            var validationResult = DeleteObjectsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public ObjectEditController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {

            IsValid = true;

            var sbError = new StringBuilder();
            var key = commandLine.AdditionalParameters.Keys.SingleOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                sbError.AppendLine($"You have to provide a valid function!");
            }
            else
            {
                var function = commandLine.AdditionalParameters[key];
                if (function == nameof(ControllerFunction.CopyRelations))
                {
                    Function = ControllerFunction.CopyRelations;
                }
                else if (function == nameof(ControllerFunction.MergeObjectsByItems))
                {
                    Function = ControllerFunction.MergeObjectsByItems;
                }
                else if (function == nameof(ControllerFunction.DeleteObjects))
                {
                    Function = ControllerFunction.DeleteObjects;
                }
                else
                {
                    IsValid = false;
                    sbError.AppendLine($"The provided function is not valid!");
                }

            }
            ErrorMessage = sbError.ToString();
            if (!IsValid)
            {
                return;
            }
            if (Function == ControllerFunction.CopyRelations)
            {
                CopyRelationsParameters = new CopyRelationsParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                
                var validationResult = CopyRelationsParameters.ValidateParam(commandLine, globals);
                IsValid = (validationResult.GUID == globals.LState_Success.GUID);
                ErrorMessage = validationResult.Additional1;

            }
            else if (Function == ControllerFunction.MergeObjectsByItems)
            {
                MergeObjectsByItemsParameters = new MergeObjectsByItemsParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = MergeObjectsByItemsParameters.ValidateParam(commandLine, globals);
                IsValid = (validationResult.GUID == globals.LState_Success.GUID);
                ErrorMessage = validationResult.Additional1;
            }
            else if (Function == ControllerFunction.DeleteObjects)
            {
                DeleteObjectsParameters = new DeleteObjectsParam
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = DeleteObjectsParameters.ValidateParam(commandLine, globals);
                IsValid = (validationResult.GUID == globals.LState_Success.GUID);
                ErrorMessage = validationResult.Additional1;
            }
        }

        public ObjectEditController() { }
    }
}