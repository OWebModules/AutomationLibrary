﻿using MailModule;
using MailModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntoMsg_Module.Logging;

namespace AutomationLibrary.Controller
{
    public enum MailModuleFunctions
    {
        MailsSync = 0,
        ExportMailsToJson = 1
    }
    public class MailModuleController : BaseController, IModuleController
    {
        public MailsSyncParam MailsSyncParameters { get; private set; }
        public ExportMailsToJsonParam ExportMailsToJsonParameters { get; private set; }

        public MailModuleFunctions MailModuleFunctions { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(MailModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"{nameof(MailModule)} IdConfig:<Id> DeleteMails:<true|false> MasterPassword:<Password>");
                sbSyntax.AppendLine($"{nameof(MailModule)} IdConfig:<Id>");
                return sbSyntax.ToString();
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_MailModule;

        public List<Type> Functions => new List<Type> { typeof(MailsSyncParam), typeof(ExportMailsToJsonParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var taskResult = await Task.Run<clsOntologyItem>(async() =>
            {
                var result = base.DoWork();

                if (result.GUID == globals.LState_Error.GUID) return result;

                var syncConnector = new MailConnector(globals);

                try
                {
                    switch(MailModuleFunctions)
                    {
                        case MailModuleFunctions.MailsSync:
                            var request = new ImportMailsRequest
                            {
                                DeleteMails = MailsSyncParameters.DeleteMails,
                                IdConfiguration = MailsSyncParameters.IdConfig,
                                MasterPassword = MailsSyncParameters.MasterPassword,
                                MessageOutput = MailsSyncParameters.MessageOutput,
                                StartScan = MailsSyncParameters.StartScan,
                                EndScan = MailsSyncParameters.EndScan,
                                CancellationToken = MailsSyncParameters.CancellationTokenSource.Token
                            };

                            var syncTask = await syncConnector.ImportImapMails(request);

                            result = syncTask.ResultState;
                            if (result.GUID == globals.LState_Success.GUID)
                            {
                                result.Additional1 = $"Synced Mails";
                            }
                            return result;

                        case MailModuleFunctions.ExportMailsToJson:
                            var exportToJsonRequest = new ExportToJsonRequest(ExportMailsToJsonParameters.IdConfig, ExportMailsToJsonParameters.CancellationTokenSource.Token)
                            {
                                MessageOutput = ExportMailsToJsonParameters.MessageOutput
                            };
                            var exportResult = await syncConnector.ExportMailItemsToJson(exportToJsonRequest);
                            if (exportResult.ResultState.GUID == globals.LState_Success.GUID)
                            {
                                result.Additional1 = "Exported Mails!";
                            }
                            return result;
                    }
                }
                catch (Exception ex)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = ex.Message;
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new MailConnector(globals);

            try
            {
                switch (MailModuleFunctions)
                {
                    case MailModuleFunctions.MailsSync:
                        var request = new ImportMailsRequest
                        {
                            DeleteMails = MailsSyncParameters.DeleteMails,
                            IdConfiguration = MailsSyncParameters.IdConfig,
                            MasterPassword = MailsSyncParameters.MasterPassword,
                            MessageOutput = MailsSyncParameters.MessageOutput,
                            StartScan = MailsSyncParameters.StartScan,
                            EndScan = MailsSyncParameters.EndScan,
                            CancellationToken = MailsSyncParameters.CancellationTokenSource.Token
                        };

                        var syncTask = syncConnector.ImportImapMails(request);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Synced Mails";
                        }
                        return result;

                    case MailModuleFunctions.ExportMailsToJson:
                        var exportToJsonRequest = new ExportToJsonRequest(ExportMailsToJsonParameters.IdConfig, ExportMailsToJsonParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = ExportMailsToJsonParameters.MessageOutput
                        };
                        var exportResult = syncConnector.ExportMailItemsToJson(exportToJsonRequest);
                        exportResult.Wait();
                        if (exportResult.Result.ResultState.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = "Exported Mails!";
                        }
                        return result;
                }
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }

            return result;
        }

        public MailModuleController(MailsSyncParam syncParam, Globals globals) : base(globals)
        {
            MailsSyncParameters = syncParam;
            IsValid = true;
            var validationResult = MailsSyncParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public MailModuleController(ExportMailsToJsonParam syncParam, Globals globals) : base(globals)
        {
            ExportMailsToJsonParameters = syncParam;
            IsValid = true;
            var validationResult = ExportMailsToJsonParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public MailModuleController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {

            IsValid = true;

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());
            if (key == null)
            {
                IsValid = false;
                ErrorMessage += $"You have to provide a Function: {MailModuleFunctions.MailsSync}|{MailModuleFunctions.ExportMailsToJson}!\n";

            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == nameof(MailModuleFunctions.MailsSync).ToLower())
                {
                    MailModuleFunctions = MailModuleFunctions.MailsSync;
                    MailsSyncParameters = new MailsSyncParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    var mailsSync = MailsSyncParameters.ValidateParam(commandLine, globals);
                    IsValid = mailsSync.GUID == globals.LState_Success.GUID;
                    ErrorMessage = mailsSync.Additional1;
                } else if (commandLine.AdditionalParameters[key].ToLower() == nameof(MailModuleFunctions.ExportMailsToJson).ToLower())
                {
                    MailModuleFunctions = MailModuleFunctions.ExportMailsToJson;
                    ExportMailsToJsonParameters = new ExportMailsToJsonParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    var exportResult = ExportMailsToJsonParameters.ValidateParam(commandLine, globals);
                    IsValid = exportResult.GUID == globals.LState_Success.GUID;
                    ErrorMessage = exportResult.Additional1;
                }
            }
        }

        public MailModuleController() { }
    }
}
