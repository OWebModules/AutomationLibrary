﻿using CommandLineRunModule.Models;
using ImportExport_Module;
using MediaStore_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutomationLibrary.Attributes;
using OutlookMailConnector.Models;
using OntoMsg_Module.Logging;

namespace AutomationLibrary.Controller
{
    public class OutlookController : BaseController, IModuleController
    {

        public enum ModuleFunction
        {
            ExportMsgFiles = 0,
            ParseMsgFiles = 1,
            GetParserFields = 2
        }
        public ExportMsgFilesParam ExportMsgFilesParameters { get; private set; }
        public ParseMsgFilesParam ParseMsgFilesParameters { get; private set; }
        public GetParserFieldsParam GetParserFieldsParameters { get; private set; }

        public ModuleFunction Function { get; private set; }
        
        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"{nameof(OutlookMailConnector)} {nameof(Function)}:{nameof(ModuleFunction.ExportMsgFiles)} {nameof(ExportMsgFilesParam.DestPath)}:<Path to export msg-files to> {nameof(ExportMsgFilesParam.ExportSubFolders)}:<true|false>");
                sbSyntax.AppendLine($"{nameof(OutlookMailConnector)} {nameof(Function)}:{nameof(ModuleFunction.ParseMsgFiles)} {nameof(ParseMsgFilesParam.IdConfig)}:<Configuration of Parse>");
                sbSyntax.AppendLine($"{nameof(OutlookMailConnector)} {nameof(Function)}:{nameof(ModuleFunction.GetParserFields)}");
                return sbSyntax.ToString();
            }
        }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(OutlookMailConnector).ToLower();
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_Outlook_Connector_Module;

        public List<Type> Functions => new List<Type> { typeof(ExportMsgFilesParam), typeof(ParseMsgFilesParam), typeof(GetParserFieldsParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;
            var syncConnector = new OutlookMailConnector.OutlookMailConnector(globals);

            switch (Function)
            {
                case ModuleFunction.ExportMsgFiles:
                    var request = new ExportToMsgFilesRequest(ExportMsgFilesParameters.DestPath, ExportMsgFilesParameters.ExportSubFolders)
                    {
                        MessageOutput = ExportMsgFilesParameters.MessageOutput
                    };

                    try
                    {
                        var syncTask = await syncConnector.ExportToMsgFiles(request);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Additional1 = $"Exported {result.Count} Mailitems.";
                        ExportMsgFilesParameters.MessageOutput?.OutputInfo(result.Additional1);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }
                case ModuleFunction.ParseMsgFiles:
                    var requestParse = new ParseMsgFileRequest(ParseMsgFilesParameters.IdConfig)
                    {
                        OutputMessage = ParseMsgFilesParameters.MessageOutput
                    };

                    try
                    {
                        var syncTask = await syncConnector.ParseMsgFiles(requestParse);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ParseMsgFilesParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        result.Additional1 = $"Parsed {result.Count} Mailitems.";
                        ParseMsgFilesParameters.MessageOutput?.OutputInfo(result.Additional1);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        ParseMsgFilesParameters.MessageOutput?.OutputError(result.Additional1);
                        return result;

                    }
                case ModuleFunction.GetParserFields:
                    var taskResult = await syncConnector.GetParserFields();

                    result = taskResult.ResultState;
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    result.Additional1 = string.Join("\n\r", taskResult.Result.ParserFields);
                    foreach (var field in taskResult.Result.ParserFields)
                    {
                        GetParserFieldsParameters.MessageOutput?.OutputInfo(field);
                    }
                    return result;

            }

            return result;
            
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;
            var syncConnector = new OutlookMailConnector.OutlookMailConnector(globals);

            switch (Function)
            {
                case ModuleFunction.ExportMsgFiles:
                    var request = new ExportToMsgFilesRequest(ExportMsgFilesParameters.DestPath, ExportMsgFilesParameters.ExportSubFolders)
                    {
                        MessageOutput = ExportMsgFilesParameters.MessageOutput
                    };

                    try
                    {
                        var syncTask = syncConnector.ExportToMsgFiles(request);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Additional1 = $"Exported {result.Count} Mailitems.";
                        ExportMsgFilesParameters.MessageOutput?.OutputInfo(result.Additional1);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }
                case ModuleFunction.ParseMsgFiles:
                    var requestParse = new ParseMsgFileRequest(ParseMsgFilesParameters.IdConfig)
                    {
                        OutputMessage = ParseMsgFilesParameters.MessageOutput
                    };

                    try
                    {
                        var syncTask = syncConnector.ParseMsgFiles(requestParse);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Additional1 = $"Parsed {result.Count} Mailitems.";
                        ParseMsgFilesParameters.MessageOutput?.OutputInfo(result.Additional1);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }
                case ModuleFunction.GetParserFields:
                    var taskResult = syncConnector.GetParserFields();
                    taskResult.Wait();

                    result = taskResult.Result.ResultState;
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    result.Additional1 = string.Join("\n\r", taskResult.Result.Result.ParserFields);
                    foreach ( var field in taskResult.Result.Result.ParserFields)
                    {
                        ParseMsgFilesParameters.MessageOutput?.OutputInfo(field);
                    }
                    return result;
                    
            }

            return result;
        }

        public OutlookController(ExportMsgFilesParam syncParam, Globals globals) : base(globals)
        {
            ExportMsgFilesParameters = syncParam;
            IsValid = true;
            Function = ModuleFunction.ExportMsgFiles;
            var validationResult = ExportMsgFilesParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public OutlookController(ParseMsgFilesParam syncParam, Globals globals) : base(globals)
        {
            ParseMsgFilesParameters = syncParam;
            IsValid = true;
            Function = ModuleFunction.ParseMsgFiles;
            var validationResult = ParseMsgFilesParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public OutlookController(GetParserFieldsParam syncParam, Globals globals) : base(globals)
        {
            GetParserFieldsParameters = syncParam;
            IsValid = true;
            Function = ModuleFunction.GetParserFields;
            var validationResult = GetParserFieldsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }


        public OutlookController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            IsValid = true;

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You have to provide an Function\n";
            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == ModuleFunction.ExportMsgFiles.ToString().ToLower())
                {
                    Function = ModuleFunction.ExportMsgFiles;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == ModuleFunction.ParseMsgFiles.ToString().ToLower())
                {
                    Function = ModuleFunction.ParseMsgFiles;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == ModuleFunction.GetParserFields.ToString().ToLower())
                {
                    Function = ModuleFunction.ParseMsgFiles;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a valid Function\n";
                }
            }

            switch(Function)
            {
                case ModuleFunction.ExportMsgFiles:
                    ExportMsgFilesParameters = new ExportMsgFilesParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)

                    };
                    var validateResultAddVariables = ExportMsgFilesParameters.ValidateParam(commandLine, globals);
                    IsValid = (validateResultAddVariables.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateResultAddVariables.Additional1;
                    break;
                case ModuleFunction.ParseMsgFiles:
                    ParseMsgFilesParameters = new ParseMsgFilesParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };

                    var validateResultParse = ParseMsgFilesParameters.ValidateParam(CommandLine, globals);
                    IsValid = (validateResultParse.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateResultParse.Additional1;
                    break;
                case ModuleFunction.GetParserFields:
                    GetParserFieldsParameters = new GetParserFieldsParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };

                    var validateResultGet = GetParserFieldsParameters.ValidateParam(CommandLine, globals);
                    IsValid = (validateResultGet.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateResultGet.Additional1;
                    break;

            }   
        }

        public OutlookController() {  }
    }
}
