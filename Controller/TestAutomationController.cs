﻿using CommandLineRunModule.Models;
using ImportExport_Module;
using MediaStore_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    public class TestAutomationController : BaseController
    {

        public enum TestAutomationFunction
        {
            ApplicationFunctionTree = 0
        }

        public TestAutomationFunction Function { get; private set; }


        public override string Syntax
        {
            get
            {
                return $"{nameof(TestAutomationModule)} {nameof(Function)}:[{TestAutomationFunction.ApplicationFunctionTree}] {nameof(CommandLine.IdConfig)}:<Id of config-object>";
            }
        }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(TestAutomationModule).ToLower();
        }


        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            
            var syncConnector = new TestAutomationModule.TestAutomationController(globals);

            switch(TestAutomationFunction.ApplicationFunctionTree)
            {
                case TestAutomationFunction.ApplicationFunctionTree:
                    try
                    {
                        var request = new TestAutomationModule.Models.GetApplicationFunctionTreeRequest(CommandLine.IdConfig);

                        var syncTask = syncConnector.GetApplicationFunctionTree(request);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Additional1 = $"Count of Variables in Code: {syncTask.Result.Result.CountFunctionTreeItems}";


                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }
                    
                
                    
            }
            
        }

        public TestAutomationController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            IsValid = true;

            if (string.IsNullOrEmpty(commandLine.IdConfig))
            {
                IsValid = false;
                ErrorMessage += "The IdConfig is empty! You must provide an IdConfig!\n";
            }

            if (!string.IsNullOrEmpty(commandLine.IdConfig) && !globals.is_GUID(commandLine.IdConfig))
            {
                IsValid = false;
                ErrorMessage += "The IdConfig no valid Id!\n";
            }

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += $"You have to provide an Function: {nameof(TestAutomationFunction.ApplicationFunctionTree)}\n";

            }
            else
            {
                if (commandLine.AdditionalParameters[key] == nameof(TestAutomationFunction.ApplicationFunctionTree))
                {
                    Function = TestAutomationFunction.ApplicationFunctionTree;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += $"The Function is not valid!\n";
                }


            }

        }

        public TestAutomationController() { }
    }
}
