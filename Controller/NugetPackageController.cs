﻿using GoogleCalendarConnector;
using GoogleCalendarConnector.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NugetPackageLib.Models;
using NugetPackageLib;

namespace AutomationLibrary.Controller
{
    public class NugetPackageController : BaseController, IModuleController
    {
        
        public CreateNugetPackageByPathParam CreateNugetPackageByPathParameter { get; private set; }
        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(NugetPackageLib).ToLower();
        }

        public override string Syntax
        {
            get
            {
                return $"{nameof(NugetPackageLib)} IdConfig:<Id>";
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_Nuget_PackageCreator;

        public List<Type> Functions => new List<Type> { typeof(CreateNugetPackageByPathParam) };

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new CreatePackageRequest(CreateNugetPackageByPathParameter.IdConfig)
            {
                MessageOutput = CreateNugetPackageByPathParameter.MessageOutput
            };

            var syncConnector = new PackageCreator(globals);

            try
            {
                var syncTask = syncConnector.CreatePackage(request);
                syncTask.Wait();

                result = syncTask.Result.ResultState;
                if (result.GUID == globals.LState_Success.GUID)
                {
                    result.Additional1 = $"Synced {syncTask.Result.Result.Count} packages created.";
                    CreateNugetPackageByPathParameter.MessageOutput?.OutputInfo(result.Additional1);
                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = base.DoWork();
                if (result.GUID == globals.LState_Error.GUID) return result;

                var request = new CreatePackageRequest(CreateNugetPackageByPathParameter.IdConfig)
                {
                    MessageOutput = CreateNugetPackageByPathParameter.MessageOutput
                };

                var syncConnector = new PackageCreator(globals);

                try
                {
                    var syncTask = await syncConnector.CreatePackage(request);

                    result = syncTask.ResultState;
                    if (result.GUID == globals.LState_Success.GUID)
                    {
                        result.Additional1 = $"Synced {syncTask.Result.Count} packages created.";
                        CreateNugetPackageByPathParameter.MessageOutput?.OutputInfo(result.Additional1);


                    }
                    return result;
                }
                catch (Exception ex)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = ex.Message;
                    return result;

                }

            });

            return taskResult;
        }

        public NugetPackageController(CreateNugetPackageByPathParam syncParam, Globals globals) : base(globals)
        {
            CreateNugetPackageByPathParameter = syncParam;
            IsValid = true;
            var validationResult = CreateNugetPackageByPathParameter.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public NugetPackageController(CommandLine commandLine, Globals globals) :base(globals,commandLine)
        {
            IsValid = true;
            CreateNugetPackageByPathParameter = new CreateNugetPackageByPathParam();
            if (string.IsNullOrEmpty(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid!";
                IsValid = false;
                return;
            }

            if (!globals.is_GUID(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid (no GUID)!";
                IsValid = false;
                return;
            }

            CreateNugetPackageByPathParameter.IdConfig = commandLine.IdConfig;
        }

        public NugetPackageController() { }
    }
}
