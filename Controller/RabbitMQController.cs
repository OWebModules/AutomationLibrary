﻿using ImportExport_Module;
using MediaStore_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutomationLibrary.Attributes;
using RabbitMQModule.Models;
using RabbitMQModule;
using OntoMsg_Module.Logging;

namespace AutomationLibrary.Controller
{
    public class RabbitMQController : BaseController, IModuleController
    {

        public enum ModuleFunction
        {
            SendSyncStarterMessage = 0,
            InitStartModuleReceive = 1,
            CreateQueueAndExchange = 2
        }
        public SendStartModuleMessageParam SendStartModuleMessageParameters { get; private set; } = new SendStartModuleMessageParam();
        public InitStartModuleReceiveParam InitStartModuleReceiveParameters { get; private set; } = new InitStartModuleReceiveParam();

        public CreateQueueAndExchangeParam CreateQueueAndExchangeParameters { get; private set; } = new CreateQueueAndExchangeParam();

        public ModuleFunction Function { get; private set; }

        public event ReceivedStartModuleMessageHandler ReceivedStartModuleMessage;
        public RabbitMQConnection Connection { get; private set; }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"{nameof(RabbitMQModule)} {nameof(Function)}:{nameof(ModuleFunction.SendSyncStarterMessage)} {nameof(SendStartModuleMessageParam.IdMessage)}:<Id of Message> {nameof(SendStartModuleMessageParam.Password)}:<master-password>");
                sbSyntax.AppendLine($"{nameof(RabbitMQModule)} {nameof(Function)}:{nameof(ModuleFunction.InitStartModuleReceive)} {nameof(InitStartModuleReceiveParam.Password)}:<master-password>");
                sbSyntax.AppendLine($"{nameof(RabbitMQModule)} {nameof(Function)}:{nameof(ModuleFunction.CreateQueueAndExchange)} {nameof(InitStartModuleReceiveParam.Password)}:<master-password>");
                return sbSyntax.ToString();
            }
        }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(RabbitMQModule).ToLower();
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_RabbitMQ_Module;

        public List<Type> Functions => new List<Type> { typeof(SendStartModuleMessageParam), typeof(InitStartModuleReceiveParam), typeof(CreateQueueAndExchangeParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;
            var syncConnector = new RabbitMQModule.RabbitMQController(globals);

            switch (Function)
            {
                case ModuleFunction.SendSyncStarterMessage:
                    
                    var connectRequest1 = new ConnectionRequest(SendStartModuleMessageParameters.Password)
                    {
                        MessageOutput = InitStartModuleReceiveParameters.MessageOutput
                    };
                    try
                    {
                        if (Connection == null)
                        {
                            var connectResult = await syncConnector.ConnectToRabbitMQ(connectRequest1);

                            result = connectResult.ResultState;
                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                return result;
                            }

                            Connection = connectResult.Result;
                        }

                        var openResult = syncConnector.OpenConnection(Connection);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var request = new SendStartModuleMessageRequest(SendStartModuleMessageParameters.IdMessage, SendStartModuleMessageParameters.Password, Connection)
                        {
                            MessageOutput = SendStartModuleMessageParameters.MessageOutput
                        };
                        var syncTask = await syncConnector.SendStartModuleMessage(request);

                        result = syncTask;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Additional1 = $"Sended Message";


                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }
                case ModuleFunction.InitStartModuleReceive:
                    var connectRequest2 = new ConnectionRequest(InitStartModuleReceiveParameters.Password)
                    { 
                        MessageOutput = InitStartModuleReceiveParameters.MessageOutput
                    };

                    try
                    {
                        if (Connection == null)
                        {
                            var connectResult = await syncConnector.ConnectToRabbitMQ(connectRequest2);

                            result = connectResult.ResultState;
                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                return result;
                            }

                            Connection = connectResult.Result;
                        }

                        var openResult = syncConnector.OpenConnection(Connection);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        if (ReceivedStartModuleMessage != null)
                        {
                            Connection.ReceivedStartModuleMessage += (message) =>
                            {
                                var receiveResult = ReceivedStartModuleMessage(message);
                                return receiveResult;
                            };
                        }

                        var initResult = syncConnector.InitializeStartModuleHandler(Connection);

                        result = initResult;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }
                        
                        //await syncConnector.CloseRabbitMQConnection(Connection);
                        result.Additional1 = $"Receive attached";
                        InitStartModuleReceiveParameters.MessageOutput?.OutputInfo(result.Additional1);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }
                case ModuleFunction.CreateQueueAndExchange:
                    var connectRequest3 = new ConnectionRequest(CreateQueueAndExchangeParameters.Password)
                    {
                        MessageOutput = CreateQueueAndExchangeParameters.MessageOutput
                    };

                    try
                    {
                        if (Connection == null)
                        {
                            var connectResult = await syncConnector.ConnectToRabbitMQ(connectRequest3);

                            result = connectResult.ResultState;
                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                return result;
                            }

                            Connection = connectResult.Result;
                        }

                        var openResult = syncConnector.OpenConnection(Connection);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        if (ReceivedStartModuleMessage != null)
                        {
                            Connection.ReceivedStartModuleMessage += (message) =>
                            {
                                var receiveResult = ReceivedStartModuleMessage(message);
                                return receiveResult;
                            };
                        }

                        var createRequest = new CreateQueueAndExchangeRequest(CreateQueueAndExchangeParameters.Password)
                        {
                            MessageOutput = CreateQueueAndExchangeParameters.MessageOutput,
                            Connection = Connection
                        };

                        var initResult = await syncConnector.CreateQueueAndExchange(createRequest);

                        result = initResult.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        //await syncConnector.CloseRabbitMQConnection(Connection);
                        result.Additional1 = $"Created Queue {initResult.Result.Queue.Name} and Exchange {initResult.Result.Exchange.Name}";
                        CreateQueueAndExchangeParameters.MessageOutput?.OutputInfo(result.Additional1);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }


            }

            return result;
            
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;
            var syncConnector = new RabbitMQModule.RabbitMQController(globals);

            switch (Function)
            {
                case ModuleFunction.SendSyncStarterMessage:
                    var connectRequest1 = new ConnectionRequest(InitStartModuleReceiveParameters.Password)
                    {
                        MessageOutput = InitStartModuleReceiveParameters.MessageOutput
                    };
                    try
                    {
                        var connectResult = syncConnector.ConnectToRabbitMQ(connectRequest1);
                        connectResult.Wait();

                        result = connectResult.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var openResult = syncConnector.OpenConnection(Connection);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var request = new SendStartModuleMessageRequest(SendStartModuleMessageParameters.IdMessage, SendStartModuleMessageParameters.Password, connectResult.Result.Result)
                        {
                            MessageOutput = SendStartModuleMessageParameters.MessageOutput
                        };
                        var syncTask = syncConnector.SendStartModuleMessage(request);
                        syncTask.Wait();

                        result = syncTask.Result;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Additional1 = $"Sended Message";


                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }
                case ModuleFunction.InitStartModuleReceive:
                    var connectRequest = new ConnectionRequest(InitStartModuleReceiveParameters.Password)
                    {
                        MessageOutput = InitStartModuleReceiveParameters.MessageOutput
                    };

                    try
                    {
                        var syncTask = syncConnector.ConnectToRabbitMQ(connectRequest);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var openResult = syncConnector.OpenConnection(Connection);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        if (ReceivedStartModuleMessage != null)
                        {
                            syncTask.Result.Result.ReceivedStartModuleMessage += (message) =>
                            {
                                return ReceivedStartModuleMessage(message);
                            };
                        }

                        var initResult = syncConnector.InitializeStartModuleHandler(syncTask.Result.Result);

                        result = initResult;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var closeTask = syncConnector.CloseRabbitMQConnection(syncTask.Result.Result);
                        closeTask.Wait();
                        result.Additional1 = $"Receive attached";

                        return result;

                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }
                case ModuleFunction.CreateQueueAndExchange:
                    var connectRequest3 = new ConnectionRequest(CreateQueueAndExchangeParameters.Password)
                    {
                        MessageOutput = CreateQueueAndExchangeParameters.MessageOutput
                    };

                    try
                    {
                        if (Connection == null)
                        {
                            var connectResult = syncConnector.ConnectToRabbitMQ(connectRequest3);
                            connectResult.Wait();

                            result = connectResult.Result.ResultState;
                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                return result;
                            }

                            Connection = connectResult.Result.Result;
                        }

                        var openResult = syncConnector.OpenConnection(Connection);
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        if (ReceivedStartModuleMessage != null)
                        {
                            Connection.ReceivedStartModuleMessage += (message) =>
                            {
                                var receiveResult = ReceivedStartModuleMessage(message);
                                return receiveResult;
                            };
                        }

                        var createRequest = new CreateQueueAndExchangeRequest(CreateQueueAndExchangeParameters.Password)
                        {
                            MessageOutput = CreateQueueAndExchangeParameters.MessageOutput
                        };

                        var initResult = syncConnector.CreateQueueAndExchange(createRequest);
                        initResult.Wait();

                        result = initResult.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        //await syncConnector.CloseRabbitMQConnection(Connection);
                        result.Additional1 = $"Created Queue {initResult.Result.Result.Queue.Name} and Exchange {initResult.Result.Result.Exchange.Name}";
                        CreateQueueAndExchangeParameters.MessageOutput?.OutputInfo(result.Additional1);

                        return result;
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        return result;

                    }
            }

            return result;
        }

        public RabbitMQController(SendStartModuleMessageParam syncParam, Globals globals) : base(globals)
        {
            SendStartModuleMessageParameters = syncParam;
            IsValid = true;
            Function = ModuleFunction.SendSyncStarterMessage;
            var validationResult = SendStartModuleMessageParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public RabbitMQController(InitStartModuleReceiveParam syncParam, Globals globals) : base(globals)
        {
            InitStartModuleReceiveParameters = syncParam;
            IsValid = true;
            Function = ModuleFunction.InitStartModuleReceive;
            var validationResult = InitStartModuleReceiveParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public RabbitMQController(CreateQueueAndExchangeParam syncParam, Globals globals) : base(globals)
        {
            CreateQueueAndExchangeParameters = syncParam;
            IsValid = true;
            Function = ModuleFunction.CreateQueueAndExchange;
            var validationResult = CreateQueueAndExchangeParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public RabbitMQController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            IsValid = true;

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You have to provide an Function\n";
            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == ModuleFunction.SendSyncStarterMessage.ToString().ToLower())
                {
                    Function = ModuleFunction.SendSyncStarterMessage;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == ModuleFunction.InitStartModuleReceive.ToString().ToLower())
                {
                    Function = ModuleFunction.InitStartModuleReceive;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == ModuleFunction.CreateQueueAndExchange.ToString().ToLower())
                {
                    Function = ModuleFunction.CreateQueueAndExchange;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a valid Function\n";
                }
            }

            switch(Function)
            {
                case ModuleFunction.SendSyncStarterMessage:
                    SendStartModuleMessageParameters.MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath);
                    var sendMessageValidation = SendStartModuleMessageParameters.ValidateParam(commandLine, globals);
                    IsValid = (sendMessageValidation.GUID == globals.LState_Success.GUID);
                    ErrorMessage += sendMessageValidation.Additional1;
                    break;
                case ModuleFunction.InitStartModuleReceive:
                    InitStartModuleReceiveParameters.MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath);
                    var receiveMessageValidation = InitStartModuleReceiveParameters.ValidateParam(commandLine, globals);
                    IsValid = (receiveMessageValidation.GUID == globals.LState_Success.GUID);
                    ErrorMessage += receiveMessageValidation.Additional1;
                    break;
                case ModuleFunction.CreateQueueAndExchange:
                    CreateQueueAndExchangeParameters.MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath);
                    var createQueueAndExchangeValidation = CreateQueueAndExchangeParameters.ValidateParam(commandLine, globals);
                    IsValid = (createQueueAndExchangeValidation.GUID == globals.LState_Success.GUID);
                    ErrorMessage += createQueueAndExchangeValidation.Additional1;
                    break;

            }   
        }

        public RabbitMQController() { }
    }
}
