﻿using BCMCSVImporter;
using BCMCSVImporter.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{

    public enum CSVManipulationFunction
    {
        MoveField,
        CompareCSV
    }
    public enum CheckMethod
    {
        Length = 0,
        Regex = 1
    }

    public class CSVManipulationController : BaseController
    {
        public char Seperator { get; private set; }

        public string PathToCheck { get; private set; }

        public string PathToWrite { get; private set; }

        public string FieldToCheck { get; private set; }

        public string FieldToMoveTo { get; private set; }

        public int MaxLength { get; private set; }

        public string Regex { get; private set; }

        public CheckMethod CheckMethod { get; private set; }

        public CSVManipulationFunction Function { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(CSVManipulation).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbUsage = new StringBuilder();
                sbUsage.AppendLine($"Module:{nameof(CSVManipulation)} Function:<{CSVManipulationFunction.MoveField.ToString()}> {nameof(Seperator)}:<Seperator-char> PathToCheck:<Path of CSV-File to check> PathToWrite:<Path of CSV-File to save> FieldToCheck:<Field name to check length> FieldToMoveTo:<Field in which too long values should be moved> MaxLength:<Max length of checked value> Regex:<Regex to check value>");
                sbUsage.AppendLine($"Module:{nameof(CSVManipulation)} Function:<{CSVManipulationFunction.CompareCSV.ToString()}> {nameof(CommandLine.IdConfig)}:<Config of CSV-Manipulation-config>");
                return sbUsage.ToString();
            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            
            try
            {
                switch (Function)
                {
                    case CSVManipulationFunction.MoveField:
                        if (CheckMethod == CheckMethod.Length)
                        {
                            var request = new RequestMoveField<IFieldCheck>(Seperator,
                            PathToCheck,
                            PathToWrite,
                            FieldToCheck,
                            FieldToMoveTo,
                            new LengthCheck
                            {
                                MaxLength = MaxLength
                            });

                            result = request.CheckRequest(globals);

                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                return result;
                            }

                            var controller = new CSVManipulation(globals);

                            var controllerTask = controller.MoveCSVField(request);
                            controllerTask.Wait();

                            result = controllerTask.Result.ResultState;

                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                return result;
                            }

                            result.Additional1 = $"Moved values: {controllerTask.Result.Result.CountMoved}";
                        }
                        else if (CheckMethod == CheckMethod.Regex)
                        {
                            var request = new RequestMoveField<IFieldCheck>(Seperator,
                            PathToCheck,
                            PathToWrite,
                            FieldToCheck,
                            FieldToMoveTo,
                            new RegexCheck(Regex));

                            result = request.CheckRequest(globals);

                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                return result;
                            }

                            var controller = new CSVManipulation(globals);

                            var controllerTask = controller.MoveCSVField(request);
                            controllerTask.Wait();

                            result = controllerTask.Result.ResultState;

                            if (result.GUID == globals.LState_Error.GUID)
                            {
                                return result;
                            }

                            result.Additional1 = $"Moved values: {controllerTask.Result.Result.CountMoved}";
                        }
                        

                        
                        break;

                    case CSVManipulationFunction.CompareCSV:
                        var requestCompare = new CompareCSVRequest(CommandLine.IdConfig);

                        var controllerManipulate = new CSVManipulation(globals);
                        var compareTask = controllerManipulate.CompareCSV(requestCompare);
                        compareTask.Wait();

                        result = compareTask.Result;

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Additional1 = $"Compare done";

                        break;
                    default:

                        result = globals.LState_Error.Clone();
                        result.Additional1 = "Wrong Function!";
                        return result;
                }
                
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        

        public CSVManipulationController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {

            IsValid = true;

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());
            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You have to provide a Function\n";

            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == nameof(CSVManipulationFunction.MoveField).ToLower())
                {
                    Function = CSVManipulationFunction.MoveField;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == nameof(CSVManipulationFunction.CompareCSV).ToLower())
                {
                    Function = CSVManipulationFunction.CompareCSV;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += $"You have to provide a Valid Function: [{nameof(CSVManipulationFunction.MoveField)}]\n";
                }

            }

            if (Function == CSVManipulationFunction.MoveField)
            {
                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Seperator).ToLower());

                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a Seperator\n";

                }
                else
                {
                    char check;
                    if (char.TryParse(commandLine.AdditionalParameters[key], out check))
                    {
                        Seperator = check;
                    }
                    else
                    {
                        IsValid = false;
                        ErrorMessage += "The provided Seperator is not valid\n";
                    }
                }

                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(PathToCheck).ToLower());

                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a Path to a CSV-File to check\n";

                }
                else
                {
                    PathToCheck = commandLine.AdditionalParameters[key];
                    if (!System.IO.File.Exists(PathToCheck))
                    {
                        IsValid = false;
                        ErrorMessage += "You have to provide an existing CSV-File Path\n";
                    }
                }

                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(PathToWrite).ToLower());

                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a Path to a CSV-File to save\n";

                }
                else
                {
                    PathToWrite = commandLine.AdditionalParameters[key];
                    if (System.IO.File.Exists(PathToWrite))
                    {
                        IsValid = false;
                        ErrorMessage += "The provided CSV-File to save mustn't exists\n";
                    }
                }


                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(FieldToCheck).ToLower());

                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a Path a CSV-Field to check the Values\n";

                }
                else
                {
                    FieldToCheck = commandLine.AdditionalParameters[key];

                }

                key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(FieldToMoveTo).ToLower());

                if (key == null)
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a Path a CSV-Field to move too long values\n";

                }
                else
                {
                    FieldToMoveTo = commandLine.AdditionalParameters[key];

                }

                var keyLength = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(MaxLength).ToLower());

                var keyRegex = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Regex).ToLower());

                if (keyLength == null && keyRegex == null)
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide one of the Check-Methods, MaxLength or Regex \n";

                }
                else
                {
                    if (keyLength != null)
                    {
                        int maxLength;
                        if (!int.TryParse(commandLine.AdditionalParameters[keyLength], out maxLength))
                        {
                            IsValid = false;
                            ErrorMessage += "The provided max length is not valid\n";
                        }
                        else
                        {
                            MaxLength = maxLength;
                        }
                        CheckMethod = CheckMethod.Length;
                    }
                    else if (keyRegex != null)
                    {
                        Regex = commandLine.AdditionalParameters[keyRegex];

                        try
                        {
                            var regexCheck = new Regex(Regex);
                        }
                        catch (Exception ex)
                        {
                            IsValid = false;
                            ErrorMessage += $"No valid Regex: {ex.Message} \n";

                        }


                        CheckMethod = CheckMethod.Regex;
                    }



                }
            }
            else if (Function == CSVManipulationFunction.CompareCSV)
            {
                if (string.IsNullOrEmpty(commandLine.IdConfig))
                {
                    ErrorMessage = "IdConfig is invalid!";
                    IsValid = false;
                    return;
                }

                if (!globals.is_GUID(commandLine.IdConfig))
                {
                    ErrorMessage = "IdConfig is invalid (no GUID)!";
                    IsValid = false;
                    return;
                }

            }


        }

        public CSVManipulationController() { }
    }
}
