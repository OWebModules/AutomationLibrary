﻿using ImportExport_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ActiveDirectorySyncModule;
using AutomationLibrary.Attributes;
using OntoMsg_Module.Logging;
using ActiveDirectorySyncModule.Models;
using AzureServiceBusModule.Models;

namespace AutomationLibrary.Controller
{
    
    public enum AzureServiceBusFunction
    {
        SendJobRequest = 0,
        ReceiveJobMessage = 1
    }
    public class AzureServiceBusController : BaseController, IModuleController
    {
        public AzureServiceBusFunction Function { get; set; }        

        public SendJobRequestParam SendJobRequestParameters { get; private set; }
        public ReceiveJobMessageParam ReceiveJobMessageParameters { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(ActiveDirectorySyncModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();

                sbSyntax.AppendLine($"{nameof(AzureServiceBusModule)} {nameof(Functions)}:{AzureServiceBusFunction.SendJobRequest.ToString()}  [{nameof(SendJobRequestParam.IdConfig)}:<IdConfig>]");
                sbSyntax.AppendLine($"{nameof(AzureServiceBusModule)} {nameof(Functions)}:{AzureServiceBusFunction.ReceiveJobMessage.ToString()} [{nameof(ReceiveJobMessageParam.IdConfig)}:<IdConfig>]");

                return sbSyntax.ToString();

            }
        }

        public clsOntologyItem Module => AutomationLibrary.Modules.Config.LocalData.Object_AzureServiceBusModule;

        public List<Type> Functions => new List<Type> { typeof(SendJobRequestParam), typeof(ReceiveJobMessageParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new AzureServiceBusModule.AzureServiceBusConnector(globals);

            try
            {
                switch (Function)
                {
                    case AzureServiceBusFunction.SendJobRequest:
                        var request = new AzureServiceBusModule.Models.SendJobMessageRequest(SendJobRequestParameters.IdConfig)
                        {
                            MessageOutput = SendJobRequestParameters.MessageOutput
                        };

                        var taskQuery = await syncConnector.SendJobMessage(request);

                        if (taskQuery.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = taskQuery.ResultState;
                            return result;
                        }

                        result.Additional1 = "Sync is completed!";
                        return result;
                    case AzureServiceBusFunction.ReceiveJobMessage:
                        var receiveJobMessageRequest = new AzureServiceBusModule.Models.ReceiveJobMessageRequest(ReceiveJobMessageParameters.IdConfig, ReceiveJobMessageParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = ReceiveJobMessageParameters.MessageOutput
                        };

                        syncConnector.JobRequest += this.SyncConnector_JobRequest;
                        syncConnector.JobRequestCompleted += SyncConnector_JobRequestCompleted;
                        syncConnector.JsonError += SyncConnector_JsonError;
                        var receiveResult = await syncConnector.ReceiveJobMessage(receiveJobMessageRequest);

                        result = receiveResult.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ReceiveJobMessageParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        return result;

                }

                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        private void SyncConnector_JsonError(string message)
        {
            ReceiveJobMessageParameters.MessageOutput?.OutputError(message);
        }

        private void SyncConnector_JobRequestCompleted(AutomationEntity automationEntity)
        {
           
            
        }

        private void SyncConnector_JobRequest(AutomationEntity automationEntity)
        {
            var types = AppDomain.CurrentDomain.GetAssemblies().Where(ass => ass.FullName.Contains("AutomationLibrary") || ass.FullName.Contains("SyncStarterModule")).SelectMany(ass => ass.GetTypes()).ToList();
            var automationParamType = types.FirstOrDefault(typeItm => typeItm.FullName == automationEntity.ParameterNamespace);

            var parameterItem = (ModuleParamBase)Newtonsoft.Json.JsonConvert.DeserializeObject(automationEntity.JobJson, automationParamType);
            parameterItem.MessageOutput = ReceiveJobMessageParameters.MessageOutput;

            ExecuteController(parameterItem);
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new AzureServiceBusModule.AzureServiceBusConnector(globals);

            try
            {

                switch (Function)
                {
                    case AzureServiceBusFunction.SendJobRequest:
                        var request = new AzureServiceBusModule.Models.SendJobMessageRequest(SendJobRequestParameters.IdConfig)
                        {
                            MessageOutput = SendJobRequestParameters.MessageOutput
                        };

                        var taskQuery = syncConnector.SendJobMessage(request);
                        taskQuery.Wait();

                        if (taskQuery.Result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = taskQuery.Result.ResultState;
                            return result;
                        }

                        result.Additional1 = "Sync is completed!";
                        return result;
                    case AzureServiceBusFunction.ReceiveJobMessage:
                        var receiveJobMessageRequest = new AzureServiceBusModule.Models.ReceiveJobMessageRequest(ReceiveJobMessageParameters.IdConfig, ReceiveJobMessageParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = ReceiveJobMessageParameters.MessageOutput
                        };

                        syncConnector.JobRequest += this.SyncConnector_JobRequest;
                        syncConnector.JobRequestCompleted += SyncConnector_JobRequestCompleted;
                        syncConnector.JsonError += SyncConnector_JsonError;
                        var receiveResult = syncConnector.ReceiveJobMessage(receiveJobMessageRequest);
                        receiveResult.Wait();

                        result = receiveResult.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ReceiveJobMessageParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        return result;

                }

                return result;

            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public AzureServiceBusController(SendJobRequestParam syncParam, Globals globals) : base(globals)
        {
            SendJobRequestParameters = syncParam;
            IsValid = true;
            Function = AzureServiceBusFunction.SendJobRequest;
            var validationResult = SendJobRequestParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public AzureServiceBusController(ReceiveJobMessageParam syncParam, Globals globals) : base(globals)
        {
            ReceiveJobMessageParameters = syncParam;
            IsValid = true;
            Function = AzureServiceBusFunction.ReceiveJobMessage;
            var validationResult = ReceiveJobMessageParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public AzureServiceBusController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            
            
            IsValid = true;
            ErrorMessage = "";
            
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());
            if (key == null)
            {
                IsValid = false;
                ErrorMessage += $"You must provide a valid Function: [{nameof(AzureServiceBusFunction.SendJobRequest)}|{nameof(AzureServiceBusFunction.ReceiveJobMessage)}]\n";
                return;
            }

            if (commandLine.AdditionalParameters[key].ToLower() == nameof(AzureServiceBusFunction.SendJobRequest))
            {
                Function = AzureServiceBusFunction.SendJobRequest;
            }
            else if (commandLine.AdditionalParameters[key].ToLower() == nameof(AzureServiceBusFunction.ReceiveJobMessage))
            {
                Function = AzureServiceBusFunction.ReceiveJobMessage;
            }
            else
            {
                IsValid = false;
                ErrorMessage += $"You must provide a valid Function: [{nameof(AzureServiceBusFunction.SendJobRequest)}|{nameof(AzureServiceBusFunction.ReceiveJobMessage)}]\n";
                return;
            }

            switch (Function)
            {
                case AzureServiceBusFunction.SendJobRequest:
                    SendJobRequestParameters = new SendJobRequestParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };

                    var validateSyncADPartners = SendJobRequestParameters.ValidateParam(commandLine, globals);
                    IsValid = validateSyncADPartners.GUID == globals.LState_Success.GUID;
                    ErrorMessage = validateSyncADPartners.Additional1;
                    return;
                case AzureServiceBusFunction.ReceiveJobMessage:
                    ReceiveJobMessageParameters = new ReceiveJobMessageParam()
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };

                    var validateCompare = ReceiveJobMessageParameters.ValidateParam(commandLine, globals);
                    IsValid = validateCompare.GUID == globals.LState_Success.GUID;
                    ErrorMessage = validateCompare.Additional1;
                    return;

            }
        }

        public AzureServiceBusController() { }
    }
}
