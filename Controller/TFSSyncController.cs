﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TFSConnectorModule;
using TFSConnectorModule.Models;

namespace AutomationLibrary.Controller
{

    public class TFSSyncController : BaseController, IModuleController
    {
        
        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(TFSConnectorModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                return $"Module:{nameof(TFSConnectorModule)} {nameof(CommandLine.IdConfig)}:<Id> {nameof(TFSSyncParam.Password)}:<Password for master-user>";
            }
        }
        public TFSSyncParam TFSSyncParameters { get; private set; }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_TFSConnectorModule;

        public List<Type> Functions => new List<Type> { typeof(TFSSyncParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new SyncTFSRequest(TFSSyncParameters.IdConfig) { Password = TFSSyncParameters.Password, MessageOutput = TFSSyncParameters.MessageOutput };
            var syncConnector = new TFSConnector(globals);

            try
            {
                var syncTask = await syncConnector.SyncTFS(request);

                result = syncTask.ResultState;
                if (result.GUID == globals.LState_Success.GUID)
                {
                    result.Additional1 = $"Synced {syncTask.Result.ReleaseResults.Count} Releases\nSynced {syncTask.Result.ChangeSetResults.Count} Changesets";
                }

                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                TFSSyncParameters.MessageOutput?.OutputError(result.Additional1);
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new SyncTFSRequest(TFSSyncParameters.IdConfig) {  Password = TFSSyncParameters.Password , MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath) };
            var syncConnector = new TFSConnector(globals);

            try
            {
                var syncTask = syncConnector.SyncTFS(request);
                syncTask.Wait();

                result = syncTask.Result.ResultState;
                if (result.GUID == globals.LState_Success.GUID)
                {
                    result.Additional1 = $"Synced {syncTask.Result.Result.ReleaseResults.Count} Releases\nSynced {syncTask.Result.Result.ChangeSetResults.Count} Changesets";
                }
                
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public TFSSyncController(TFSSyncParam syncParam, Globals globals) : base(globals)
        {
            TFSSyncParameters = syncParam;
            IsValid = true;
            var validationResult = TFSSyncParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public TFSSyncController(CommandLine commandLine, Globals globals) :base(globals,commandLine)
        {
            IsValid = true;
            TFSSyncParameters = new TFSSyncParam();
            TFSSyncParameters.MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath);
            var sendMessageValidation = TFSSyncParameters.ValidateParam(commandLine, globals);
            IsValid = (sendMessageValidation.GUID == globals.LState_Success.GUID);
            ErrorMessage += sendMessageValidation.Additional1;
                
        }

        public TFSSyncController() { }
    }
}
