﻿using FileManagementModule;
using FileManagementModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Attributes;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TypedTaggingModule.Connectors;
using TypedTaggingModule.Models;
using System.Threading;

namespace AutomationLibrary.Controller
{
    public enum FileSystemModuleFunction
    {
        UpdateOntologies = 0,
        ExportOntologyAssemblies = 1,
        ImportFileAttributes = 2,
        BackupRelatedFiles = 3,
        CalculateHashValues = 4,
        ReplaceFileContent = 5,
        RelateExternalFiles = 6
    }

    public class FileSystemModuleController : BaseController, IModuleController
    {

        public UpdateOntologiesParam UpdateOntologiesParameters { get; private set; }
        public ExportOntologyAssembliesParam ExportOntologyAssembliesParameters { get; private set; }
        public ImportFileAttributesParam ImportFileAttributesParameters { get; private set; }

        public BackupRelatedFilesParam BackupRelatedFilesParameters { get; private set; }

        public CalculateHashParam CalculateHashParameters { get; private set; }

        public ReplaceFileContentParam ReplaceFileContentParameters { get; private set; }

        public RelateExternalFilesParam RelateExternalFilesParameters { get; private set; }
        public FileSystemModuleFunction Function { get; set; }


        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(FileManagementModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"Module:{nameof(FileManagementModule)} {nameof(Function)}:[{nameof(FileSystemModuleFunction.UpdateOntologies)}] [{nameof(UpdateOntologiesParam.RootPath)}:<Rootpath of ontology-assemblies> | {nameof(UpdateOntologiesParam.UseDbPath)}:<Use paths of db-items>] [{nameof(UpdateOntologiesParam.ExcludeRegex)}:<Optional Regex of path to exclude>] [{nameof(UpdateOntologiesParam.IncludeRegex)}:<Optional Regex of path to include>]");
                sbSyntax.AppendLine($"Module:{nameof(FileManagementModule)} {nameof(Function)}:[{nameof(FileSystemModuleFunction.ExportOntologyAssemblies)}] {nameof(ExportOntologyAssembliesParameters.NamespacesToExport)}:<optional namespaces to filter the export>");
                sbSyntax.AppendLine($"Module:{nameof(FileManagementModule)} {nameof(Function)}:[{nameof(FileSystemModuleFunction.ImportFileAttributes)}] {nameof(ImportFileAttributesParam.IdConfig)}:<config-id>");
                sbSyntax.AppendLine($"Module:{nameof(FileManagementModule)} {nameof(Function)}:[{nameof(FileSystemModuleFunction.BackupRelatedFiles)}] {nameof(BackupRelatedFilesParam.IdConfig)}:<config-id>");
                sbSyntax.AppendLine($"Module:{nameof(FileManagementModule)} {nameof(Function)}:[{nameof(FileSystemModuleFunction.CalculateHashValues)}] {nameof(CalculateHashParam.IdConfig)}:<config-id>");
                sbSyntax.AppendLine($"Module:{nameof(FileManagementModule)} {nameof(Function)}:[{nameof(FileSystemModuleFunction.ReplaceFileContent)}] {nameof(CalculateHashParam.IdConfig)}:<config-id>");
                sbSyntax.AppendLine($"Module:{nameof(FileManagementModule)} {nameof(Function)}:[{nameof(FileSystemModuleFunction.RelateExternalFiles)}] {nameof(CalculateHashParam.IdConfig)}:<config-id>");
                return sbSyntax.ToString(); ;
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_Filesystem_Management;

        public List<Type> Functions => new List<Type> { typeof(UpdateOntologiesParam), 
            typeof(ExportOntologyAssembliesParam), 
            typeof(ImportFileAttributesParam), 
            typeof(BackupRelatedFilesParam), 
            typeof(CalculateHashParam),
            typeof(ReplaceFileContentParam),
            typeof(RelateExternalFilesParam)};

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new FileManagementController(globals);

            switch (Function)
            {
                case FileSystemModuleFunction.UpdateOntologies:
                    var requestUpdate = new UpdateAssembliesRequest
                    {
                        Path = UpdateOntologiesParameters.RootPath,
                        UseDbPaths = UpdateOntologiesParameters.UseDbPath,
                        ExcludeRegex = UpdateOntologiesParameters.ExcludeRegex,
                        IncludeRegex = UpdateOntologiesParameters.IncludeRegex,
                        MessageOutput = UpdateOntologiesParameters.MessageOutput,
                        Test = UpdateOntologiesParameters.Test
                    };

                    try
                    {
                        var syncTask = await syncConnector.UpdateOntologyGraph(requestUpdate);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Updated Ontologies: {syncTask.Result.OntologyVersions.Count}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case FileSystemModuleFunction.ExportOntologyAssemblies:
                    var requestExport = new ExportOntologyVersionsRequest
                    {
                        PathToExportTo = ExportOntologyAssembliesParameters.RootPath,
                        NamespacesToExport = ExportOntologyAssembliesParameters.NamespacesToExport.Select(nameSpace => nameSpace.Name).ToList(),
                        MessageOutput = ExportOntologyAssembliesParameters.MessageOutput
                    };

                    try
                    {
                        var syncTask = await syncConnector.ExportOntologyVersions(requestExport);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Exported Ontologies: {syncTask.Result.OntologyVersions.Count}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case FileSystemModuleFunction.ImportFileAttributes:
                    var requestImportFileAttributes = new ImportFileAttributesRequest(ImportFileAttributesParameters.IdConfig) { MessageOutput = ImportFileAttributesParameters.MessageOutput };

                    try
                    {
                        var syncTask = await syncConnector.ImportFileAttributes(requestImportFileAttributes);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Imported Files: {syncTask.Result.FileCount}, Errors: {syncTask.Result.ErrorCount}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case FileSystemModuleFunction.BackupRelatedFiles:
                    var backupRelatedFilesRequest = new BackupRelatedFilesRequest(BackupRelatedFilesParameters.IdConfig, BackupRelatedFilesParameters.CancellationTokenSource.Token) { MessageOutput = BackupRelatedFilesParameters.MessageOutput };

                    try
                    {
                        var syncTask = await syncConnector.BackupRelatedFiles(backupRelatedFilesRequest);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Saved Files: {syncTask.Result.ConfigResults.Count}";
                            BackupRelatedFilesParameters.MessageOutput?.OutputInfo(result.Additional1);
                            foreach (var configResult in syncTask.Result.ConfigResults)
                            {
                                if (configResult.ResultState.GUID == globals.LState_Error.GUID)
                                {
                                    BackupRelatedFilesParameters.MessageOutput?.OutputError(configResult.ResultState.Additional1);
                                }
                                else
                                {
                                    BackupRelatedFilesParameters.MessageOutput?.OutputInfo($"Backup successful for {configResult.ConfigItem.Name}, {configResult.FilesSaved.Count} files saved!");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;

                case FileSystemModuleFunction.CalculateHashValues:
                    var calculateHasRequest = new CalculateHashForBlobFilesRequest(CalculateHashParameters.IdConfig, CalculateHashParameters.CancellationTokenSource.Token) { MessageOutput = CalculateHashParameters.MessageOutput };

                    try
                    {
                        var syncTask = await syncConnector.CalculateHashForBlobFiles(calculateHasRequest);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Calculated Files: {syncTask.Result.FileCount}";
                            BackupRelatedFilesParameters.MessageOutput?.OutputInfo(result.Additional1);
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case FileSystemModuleFunction.ReplaceFileContent:
                    var requestReplaceContent = new ReplaceContentRequest(ReplaceFileContentParameters.IdConfig, ReplaceFileContentParameters.CancellationTokenSource.Token) { MessageOutput = ReplaceFileContentParameters.MessageOutput };

                    try
                    {
                        var syncTask = await syncConnector.ReplaceContent(requestReplaceContent, false);

                        result = syncTask;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Replace finished. For result check the Replace-Log.";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case FileSystemModuleFunction.RelateExternalFiles:
                    var requestRelate = new RelateExternalFilesRequest(RelateExternalFilesParameters.IdConfig, RelateExternalFilesParameters.CancellationTokenSource.Token)
                    {
                        MessageOutput = RelateExternalFilesParameters.MessageOutput
                    };

                    try
                    {
                        var syncTask = await syncConnector.RelateExternalFile(requestRelate);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Related files: {syncTask.Result.FileItems.Count}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                default:
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The function is not valid!";

                    break;
            }

            return result;
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new FileManagementController(globals);

            switch (Function)
            {
                case FileSystemModuleFunction.UpdateOntologies:
                    var requestUpdate = new UpdateAssembliesRequest
                    {
                        Path = UpdateOntologiesParameters.RootPath,
                        UseDbPaths = UpdateOntologiesParameters.UseDbPath,
                        ExcludeRegex = UpdateOntologiesParameters.ExcludeRegex,
                        IncludeRegex = UpdateOntologiesParameters.IncludeRegex,
                        MessageOutput = UpdateOntologiesParameters.MessageOutput
                    };

                    try
                    {
                        var syncTask = syncConnector.UpdateOntologyGraph(requestUpdate);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Updated Ontologies: {syncTask.Result.Result.OntologyVersions.Count}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case FileSystemModuleFunction.ExportOntologyAssemblies:
                    var requestExport = new ExportOntologyVersionsRequest
                    {
                        PathToExportTo = ExportOntologyAssembliesParameters.RootPath,
                        NamespacesToExport = ExportOntologyAssembliesParameters.NamespacesToExport.Select(nameSpace => nameSpace.Name).ToList(),
                        MessageOutput = ExportOntologyAssembliesParameters.MessageOutput
                    };

                    try
                    {
                        var syncTask = syncConnector.ExportOntologyVersions(requestExport);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Exported Ontologies: {syncTask.Result.Result.OntologyVersions.Count}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case FileSystemModuleFunction.ImportFileAttributes:
                    var requestImportFileAttributes = new ImportFileAttributesRequest(ImportFileAttributesParameters.IdConfig) { MessageOutput = ImportFileAttributesParameters.MessageOutput };

                    try
                    {
                        var syncTask = syncConnector.ImportFileAttributes(requestImportFileAttributes);

                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Imported Files: {syncTask.Result.Result.FileCount}, Errors: {syncTask.Result.Result.ErrorCount}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case FileSystemModuleFunction.BackupRelatedFiles:
                    var backupRelatedFilesRequest = new BackupRelatedFilesRequest(BackupRelatedFilesParameters.IdConfig, BackupRelatedFilesParameters.CancellationTokenSource.Token) { MessageOutput = ImportFileAttributesParameters.MessageOutput };

                    try
                    {
                        var syncTask = syncConnector.BackupRelatedFiles(backupRelatedFilesRequest);

                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Saved Files: {syncTask.Result.Result.ConfigResults.Count}";
                            BackupRelatedFilesParameters.MessageOutput?.OutputInfo(result.Additional1);
                            foreach (var configResult in syncTask.Result.Result.ConfigResults)
                            {
                                if (configResult.ResultState.GUID == globals.LState_Error.GUID)
                                {
                                    BackupRelatedFilesParameters.MessageOutput?.OutputError(configResult.ResultState.Additional1);
                                }
                                else
                                {
                                    BackupRelatedFilesParameters.MessageOutput?.OutputInfo($"Backup successful for {configResult.ConfigItem.Name}, {configResult.FilesSaved.Count} files saved!");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;

                case FileSystemModuleFunction.CalculateHashValues:
                    var calculateHasRequest = new CalculateHashForBlobFilesRequest(CalculateHashParameters.IdConfig, CalculateHashParameters.CancellationTokenSource.Token) { MessageOutput = BackupRelatedFilesParameters.MessageOutput };

                    try
                    {
                        var syncTask = syncConnector.CalculateHashForBlobFiles(calculateHasRequest);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Calculated Files: {syncTask.Result.Result.FileCount}";
                            BackupRelatedFilesParameters.MessageOutput?.OutputInfo(result.Additional1);
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case FileSystemModuleFunction.ReplaceFileContent:
                    var requestReplaceContent = new ReplaceContentRequest(ReplaceFileContentParameters.IdConfig, ReplaceFileContentParameters.CancellationTokenSource.Token) { MessageOutput = ImportFileAttributesParameters.MessageOutput };

                    try
                    {
                        var syncTask = syncConnector.ReplaceContent(requestReplaceContent, false);
                        syncTask.Wait();

                        result = syncTask.Result;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Replace finished. For result check the Replace-Log.";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case FileSystemModuleFunction.RelateExternalFiles:
                    var requestRelate = new RelateExternalFilesRequest(RelateExternalFilesParameters.IdConfig, RelateExternalFilesParameters.CancellationTokenSource.Token)
                    {
                        MessageOutput = ExportOntologyAssembliesParameters.MessageOutput
                    };

                    try
                    {
                        var syncTask = syncConnector.RelateExternalFile(requestRelate);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Related files: {syncTask.Result.Result.FileItems.Count}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                default:
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The function is not valid!";

                    break;
            }

            return result;
        }

        public FileSystemModuleController(UpdateOntologiesParam syncParam, Globals globals) : base(globals)
        {
            Function = FileSystemModuleFunction.UpdateOntologies;
            UpdateOntologiesParameters = syncParam;
            IsValid = true;
            var validationResult = UpdateOntologiesParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public FileSystemModuleController(ExportOntologyAssembliesParam syncParam, Globals globals) : base(globals)
        {
            Function = FileSystemModuleFunction.ExportOntologyAssemblies;
            ExportOntologyAssembliesParameters = syncParam;
            IsValid = true;
            var validationResult = ExportOntologyAssembliesParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public FileSystemModuleController(ImportFileAttributesParam syncParam, Globals globals) : base(globals)
        {
            Function = FileSystemModuleFunction.ImportFileAttributes;
            ImportFileAttributesParameters = syncParam;
            IsValid = true;
            var validationResult = ImportFileAttributesParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public FileSystemModuleController(BackupRelatedFilesParam syncParam, Globals globals) : base(globals)
        {
            Function = FileSystemModuleFunction.BackupRelatedFiles;
            BackupRelatedFilesParameters = syncParam;
            IsValid = true;
            var validationResult = BackupRelatedFilesParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public FileSystemModuleController(CalculateHashParam syncParam, Globals globals) : base(globals)
        {
            Function = FileSystemModuleFunction.CalculateHashValues;
            CalculateHashParameters = syncParam;
            IsValid = true;
            var validationResult = CalculateHashParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public FileSystemModuleController(ReplaceFileContentParam syncParam, Globals globals) : base(globals)
        {
            Function = FileSystemModuleFunction.ReplaceFileContent;
            ReplaceFileContentParameters = syncParam;
            IsValid = true;
            var validationResult = ReplaceFileContentParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public FileSystemModuleController(RelateExternalFilesParam syncParam, Globals globals) : base(globals)
        {
            Function = FileSystemModuleFunction.RelateExternalFiles;
            RelateExternalFilesParameters = syncParam;
            IsValid = true;
            var validationResult = RelateExternalFilesParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public FileSystemModuleController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {

            IsValid = true;

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You must provide a function";

            }
            else
            {
                if (commandLine.AdditionalParameters[key] == nameof(FileSystemModuleFunction.UpdateOntologies))
                {
                    Function = FileSystemModuleFunction.UpdateOntologies;
                }
                else if (commandLine.AdditionalParameters[key] == nameof(FileSystemModuleFunction.ExportOntologyAssemblies))
                {
                    Function = FileSystemModuleFunction.ExportOntologyAssemblies;
                }
                else if (CommandLine.AdditionalParameters[key] == nameof(FileSystemModuleFunction.ImportFileAttributes))
                {
                    Function = FileSystemModuleFunction.ImportFileAttributes;
                }
                else if (CommandLine.AdditionalParameters[key] == nameof(FileSystemModuleFunction.BackupRelatedFiles))
                {
                    Function = FileSystemModuleFunction.BackupRelatedFiles;
                }
                else if (commandLine.AdditionalParameters[key] == nameof(FileSystemModuleFunction.CalculateHashValues))
                {
                    Function = FileSystemModuleFunction.CalculateHashValues;
                }
                else if (commandLine.AdditionalParameters[key]== nameof(FileSystemModuleFunction.ReplaceFileContent))
                {
                    Function = FileSystemModuleFunction.ReplaceFileContent;
                }
                else if (CommandLine.AdditionalParameters[key] == nameof(FileSystemModuleFunction.RelateExternalFiles))
                {
                    Function = FileSystemModuleFunction.RelateExternalFiles;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += "You must provide a valid function";
                }
            }

            if (Function == FileSystemModuleFunction.UpdateOntologies)
            {
                UpdateOntologiesParameters = new UpdateOntologiesParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = UpdateOntologiesParameters.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;

            }
            else if (Function == FileSystemModuleFunction.ExportOntologyAssemblies)
            {
                ExportOntologyAssembliesParameters = new ExportOntologyAssembliesParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = ExportOntologyAssembliesParameters.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;

            }
            else if (Function == FileSystemModuleFunction.ImportFileAttributes)
            {
                ImportFileAttributesParameters = new ImportFileAttributesParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = ImportFileAttributesParameters.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;
            }
            else if (Function == FileSystemModuleFunction.BackupRelatedFiles)
            {
                BackupRelatedFilesParameters = new BackupRelatedFilesParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = ImportFileAttributesParameters.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;
            }
            else if (Function == FileSystemModuleFunction.CalculateHashValues)
            {
                CalculateHashParameters = new CalculateHashParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = CalculateHashParameters.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;
            }
            else if (Function == FileSystemModuleFunction.ReplaceFileContent)
            {
                ReplaceFileContentParameters = new ReplaceFileContentParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = ReplaceFileContentParameters.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;
            }
            else if (Function == FileSystemModuleFunction.RelateExternalFiles)
            {
                RelateExternalFilesParameters = new RelateExternalFilesParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath),
                };
                var validationResult = RelateExternalFilesParameters.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public FileSystemModuleController() { }
    }
}
