﻿using OntologyAppDBConnector;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;

namespace AutomationLibrary.Controller
{
    public delegate void ExecuteControllerHandler(ModuleParamBase param);
    public abstract class BaseController 
    {

        public event ExecuteControllerHandler ExecuteControllerEvent;
        public CommandLine CommandLine { get; private set; }

        protected Globals globals;

        public bool IsValid { get; protected set; }

        public string ErrorMessage { get; set; }

        public virtual bool IsResponsible(string module)
        { 
            return false;
        }

        public virtual string Syntax
        {
            get
            {
                return "";
            }
        }

        public override string ToString()
        {

            var result = $"{base.ToString()}\n\n";
            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                result += $"{ErrorMessage}\n\n";
            }
            result = Syntax;
            return result;

        }

        public virtual async Task<clsOntologyItem> DoWorkAsync()
        {
            if (!IsValid)
            {
                var result = globals.LState_Error.Clone();
                result.Additional1 = "The requirement for work is not valid!";
                return result;
            }

            return globals.LState_Success.Clone();
        }

        public virtual clsOntologyItem DoWork()
        {
            if (!IsValid)
            {
                var result = globals.LState_Error.Clone();
                result.Additional1 = "The requirement for work is not valid!";
                return result;
            }

            return globals.LState_Success.Clone();
        }

        public void ExecuteController(ModuleParamBase param)
        {
            ExecuteControllerEvent?.Invoke(param);
        }
        
        public BaseController(Globals globals)
        {
            this.globals = globals;
        }
        public BaseController(Globals globals, CommandLine commandLine)
        {
            this.globals = globals;
            CommandLine = commandLine;
        }

        public BaseController() { }
    }
}
