﻿using GoogleCalendarConnector;
using GoogleCalendarConnector.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TextParserModule.Models;
using TextParserModule.Controller;
using TextParserModule.Analyze;

namespace AutomationLibrary.Controller
{
    public enum TextParserFunction
    {
        ExecuteTextParser = 0,
        ReIndexFields = 1,
        ConvertPdfToText = 2,
        IndexPDFDocuments = 3,
        AnalyzeOModuleLogs = 4,
        MeasureTextparser = 5,
        ReIndexPatternSourceFields = 6
    }
    
    public class TextParserController : BaseController, IModuleController
    {
        
        public TextParserFunction Function { get; private set; }
        public TextParserExecutePram ParametersExecute { get; private set; }
        public ReIndexTextParserFieldsParam ParametersReIndex { get; private set; }

        public ConvertPdfToTextParam ConvertPdfToTextParameters { get; private set; }

        public IndexPDFDocumentsParam IndexPDFDocumentsParameters { get; private set; }

        public AnalyzeOModulesLogsPram AnalyzeOModulesLogsPrameters { get; private set; }

        public ReIndexTextParserPatternSourceFieldsParam ReIndexTextParserPatternSourceFieldsParameters { get; private set; }

        public MeasureTextParserParam MeasureTextParserParameters { get; private set; }
        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(TextParserModule).ToLower();   
        }

        public override string Syntax
        {
            get
            {
                var sb = new StringBuilder();
                sb.AppendLine($"{nameof(TextParserModule)} {nameof(Function)}:{TextParserFunction.ExecuteTextParser.ToString()} {nameof(TextParserExecutePram.IdTextParser)}:<Id of a Textparser to execute (parse)>");
                sb.AppendLine($"{nameof(TextParserModule)} {nameof(Function)}:{TextParserFunction.ReIndexFields.ToString()} {nameof(ReIndexTextParserFieldsParam.IdTextParser)}:<Id of a Textparser to execute (parse)> {nameof(ReIndexTextParserFieldsParam.IdsFields)}:<Comma-seperated list of ids of fields to reindex>");
                sb.AppendLine($"{nameof(TextParserModule)} {nameof(Function)}:{TextParserFunction.ConvertPdfToText.ToString()} {nameof(ConvertPdfToTextParam.IdConfig)}:<Id Configuration>");
                sb.AppendLine($"{nameof(TextParserModule)} {nameof(Function)}:{TextParserFunction.IndexPDFDocuments.ToString()}");
                sb.AppendLine($"{nameof(TextParserModule)} {nameof(Function)}:{TextParserFunction.AnalyzeOModuleLogs.ToString()}");
                sb.AppendLine($"{nameof(TextParserModule)} {nameof(Function)}:{TextParserFunction.MeasureTextparser.ToString()} {nameof(MeasureTextParserParam.IdConfig)}:<Id Configuration>");
                sb.AppendLine($"{nameof(TextParserModule)} {nameof(Function)}:{TextParserFunction.ReIndexPatternSourceFields.ToString()} {nameof(ReIndexTextParserPatternSourceFieldsParam.IdTextParser)}:<Id TextParser>");
                return sb.ToString();
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_TextParser;

        public List<Type> Functions => new List<Type> 
        { 
            typeof(TextParserExecutePram), 
            typeof(ReIndexTextParserFieldsParam), 
            typeof(ConvertPdfToTextParam), 
            typeof(IndexPDFDocumentsParam),
            typeof(AnalyzeOModulesLogsPram),
            typeof(MeasureTextParserParam),
            typeof(ReIndexTextParserPatternSourceFieldsParam)
        };

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            
            var syncConnector = new TextParserModule.TextParserController(globals);

            try
            {
                switch (Function)
                {
                    case TextParserFunction.ExecuteTextParser:
                        var requestExecute = new ExecuteTextParserRequest
                        {
                            IdTextParser = ParametersExecute.IdTextParser,
                            DeleteIndex = ParametersExecute.Delete,
                            MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                        };
                        var syncTask = syncConnector.ExecuteTextParser(requestExecute);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Synced {syncTask.Result.Result.DocParsed}/{syncTask.Result.Result.DocCount} Docs";


                        }
                        break;
                    case TextParserFunction.ReIndexFields:
                        var cancellationTokenSource = new CancellationTokenSource();
                        var requestReIndex = new ReIndexTextParserFieldsRequest(ParametersReIndex.IdTextParser, ParametersReIndex.Query, cancellationTokenSource)
                        {
                            MessageOutput = ParametersReIndex.MessageOutput,
                            IdsFields = ParametersReIndex.IdsFields.Select(id => id.Id).ToList()
                        };
                        var reIndexResult = syncConnector.ReIndexTextParserFields(requestReIndex);
                        reIndexResult.Wait();
                        result = reIndexResult.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Re-Indexed";


                        }

                        break;
                    case TextParserFunction.ConvertPdfToText:
                        var requestPdfConvert = new ConvertPdfToTextRequest(ConvertPdfToTextParameters.IdConfig)
                        {
                            MessageOutput = ParametersReIndex.MessageOutput
                        };
                        var convertConnector = new PDFConverter(globals);
                        var pdfConvertResult = convertConnector.ConvertPDFFileToText(requestPdfConvert);
                        pdfConvertResult.Wait();
                        result = pdfConvertResult.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            var errorCount = pdfConvertResult.Result.Result.Count(conv => conv.Result.GUID == globals.LState_Error.GUID);
                            result.Additional1 = $"Converted {pdfConvertResult.Result.Result.Count} files, {errorCount} errors.";
                        }

                        break;
                    case TextParserFunction.IndexPDFDocuments:
                        try
                        {
                            var cancellationTokenSourceIndex = new CancellationTokenSource();
                            var requestPdfIndex = new IndexPDFRequest(IndexPDFDocumentsParameters.IdConfig)
                            {
                                MessageOutput = IndexPDFDocumentsParameters.MessageOutput,
                                CancellationToken = cancellationTokenSourceIndex.Token
                            };
                            var pdfIndexer = new PDFIndexer(globals);
                            var pdfIndexResult = pdfIndexer.IndexPDFfiles(requestPdfIndex);
                            pdfIndexResult.Wait();
                            result = pdfIndexResult.Result.ResultState;
                        }
                        catch (Exception ex)
                        {

                            IndexPDFDocumentsParameters.MessageOutput.OutputError(ex.Message);
                        }
                        break;
                    case TextParserFunction.MeasureTextparser:
                        var measureTextParserRequest = new MeasureTextParserRequest(MeasureTextParserParameters.IdConfig, MeasureTextParserParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = MeasureTextParserParameters.MessageOutput
                        };
                        var textParserController = new TextParserModule.TextParserController(globals);
                        var measureResult = textParserController.MeasureTextParser(measureTextParserRequest);
                        measureResult.Wait();
                        result = measureResult.Result;

                        break;
                    case TextParserFunction.ReIndexPatternSourceFields:
                        var reindexPatternSourceFieldsRequest = new ReIndexPatternSourceFieldsRequest(ReIndexTextParserPatternSourceFieldsParameters.IdTextParser, ReIndexTextParserPatternSourceFieldsParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = ReIndexTextParserPatternSourceFieldsParameters.MessageOutput
                        };
                        var textParserControllerForReindexing = new TextParserModule.TextParserController(globals);
                        var reindexResult = textParserControllerForReindexing.ReIndexPatternSourceFields(reindexPatternSourceFieldsRequest);
                        reindexResult.Wait();
                        result = reindexResult.Result.ResultState;

                        break;
                }
                
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = base.DoWork();
                if (result.GUID == globals.LState_Error.GUID) return result;


                var syncConnector = new TextParserModule.TextParserController(globals);

                try
                {
                    switch (Function)
                    {
                        case TextParserFunction.ExecuteTextParser:
                            var requestExecute = new ExecuteTextParserRequest
                            {
                                IdTextParser = ParametersExecute.IdTextParser,
                                MessageOutput = ParametersExecute.MessageOutput,
                                DeleteIndex = ParametersExecute.Delete,
                                CancellationToken = ParametersExecute.CancellationTokenSource.Token
                            };
                            var syncTask = await syncConnector.ExecuteTextParser(requestExecute);

                            result = syncTask.ResultState;
                            if (result.GUID == globals.LState_Success.GUID)
                            {
                                result.Additional1 = $"Synced {syncTask.Result.DocParsed}/{syncTask.Result.DocCount} Docs";


                            }
                            break;
                        case TextParserFunction.ReIndexFields:
                            
                            var requestReIndex = new ReIndexTextParserFieldsRequest(ParametersReIndex.IdTextParser, ParametersReIndex.Query, ParametersReIndex.CancellationTokenSource)
                            {
                                MessageOutput = ParametersReIndex.MessageOutput,
                                IdsFields = ParametersReIndex.IdsFields.Select(id => id.Id).ToList()
                            };
                            var reIndexResult = await syncConnector.ReIndexTextParserFields(requestReIndex);
                            result = reIndexResult.ResultState;
                            if (result.GUID == globals.LState_Success.GUID)
                            {
                                result.Additional1 = $"Re-Indexed";


                            }

                            break;
                        case TextParserFunction.ConvertPdfToText:
                            var requestPdfConvert = new ConvertPdfToTextRequest(ConvertPdfToTextParameters.IdConfig)
                            {
                                MessageOutput = ConvertPdfToTextParameters.MessageOutput
                            };
                            var convertConnector = new PDFConverter(globals);
                            var pdfConvertResult = await convertConnector.ConvertPDFFileToText(requestPdfConvert);
                            result = pdfConvertResult.ResultState;
                            if (result.GUID == globals.LState_Success.GUID)
                            {
                                var errorCount = pdfConvertResult.Result.Count(conv => conv.Result.GUID == globals.LState_Error.GUID);
                                result.Additional1 = $"Converted {pdfConvertResult.Result.Count} files, {errorCount} errors.";
                            }

                            break;
                        case TextParserFunction.IndexPDFDocuments:
                            try
                            {
                                var requestPdfIndex = new IndexPDFRequest(IndexPDFDocumentsParameters.IdConfig)
                                {
                                    MessageOutput = IndexPDFDocumentsParameters.MessageOutput,
                                    CancellationToken = IndexPDFDocumentsParameters.CancellationTokenSource.Token
                                };
                                var pdfIndexer = new PDFIndexer(globals);
                                var pdfIndexResult = await pdfIndexer.IndexPDFfiles(requestPdfIndex);
                                result = pdfIndexResult.ResultState;
                            }
                            catch (Exception ex)
                            {

                                IndexPDFDocumentsParameters.MessageOutput.OutputError(ex.Message);
                            }

                            break;
                        case TextParserFunction.AnalyzeOModuleLogs:
                            try
                            {
                                var analyzeController = new AnalyzeOModulesLogController(globals);
                                var textParserControllerAnalyze = new TextParserModule.TextParserController(globals);
                                var executeTextParserRequest = new ExecuteTextParserRequest
                                {
                                    IdTextParser = analyzeController.TextParser.GUID,
                                    DeleteIndex = true,
                                    CancellationToken = AnalyzeOModulesLogsPrameters.CancellationTokenSource.Token,
                                    MessageOutput = AnalyzeOModulesLogsPrameters.MessageOutput
                                };

                                AnalyzeOModulesLogsPrameters.MessageOutput?.OutputInfo("Parse Logfiles...");

                                var parseResult = await textParserControllerAnalyze.ExecuteTextParser(executeTextParserRequest);

                                if (parseResult.ResultState.GUID == globals.LState_Error.GUID)
                                {
                                    throw new Exception(parseResult.ResultState.Additional1);
                                }
                                AnalyzeOModulesLogsPrameters.MessageOutput?.OutputInfo("Logfiles parsed.");

                                var analyzeResult = await analyzeController.Analyze(AnalyzeOModulesLogsPrameters.CancellationTokenSource.Token, AnalyzeOModulesLogsPrameters.MessageOutput);
                                
                                result = analyzeResult;
                            }
                            catch (Exception ex)
                            {

                                AnalyzeOModulesLogsPrameters.MessageOutput?.OutputError(ex.Message);
                            }

                            break;
                        case TextParserFunction.MeasureTextparser:
                            var measureTextParserRequest = new MeasureTextParserRequest(MeasureTextParserParameters.IdConfig, MeasureTextParserParameters.CancellationTokenSource.Token)
                            {
                                MessageOutput = MeasureTextParserParameters.MessageOutput
                            };
                            var textParserController = new TextParserModule.TextParserController(globals);
                            var measureResult = await textParserController.MeasureTextParser(measureTextParserRequest);
                            result = measureResult;
                            
                            break;
                        case TextParserFunction.ReIndexPatternSourceFields:
                            var reindexPatternSourceFieldsRequest = new ReIndexPatternSourceFieldsRequest(ReIndexTextParserPatternSourceFieldsParameters.IdTextParser, ReIndexTextParserPatternSourceFieldsParameters.CancellationTokenSource.Token)
                            {
                                MessageOutput = ReIndexTextParserPatternSourceFieldsParameters.MessageOutput
                            };
                            var textParserControllerForReindexing = new TextParserModule.TextParserController(globals);
                            var reindexResult = await textParserControllerForReindexing.ReIndexPatternSourceFields(reindexPatternSourceFieldsRequest);
                            result = reindexResult.ResultState;

                            break;
                    }

                    return result;
                }
                catch (Exception ex)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = ex.Message;
                    return result;

                }

            });

            return taskResult;
        }

        public TextParserController(ReIndexTextParserFieldsParam syncParam, Globals globals) : base(globals)
        {
            Function = TextParserFunction.ReIndexFields;
            ParametersReIndex = syncParam;
            IsValid = true;
            var validationResult = ParametersReIndex.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public TextParserController(TextParserExecutePram syncParam, Globals globals) : base(globals)
        {
            Function = TextParserFunction.ExecuteTextParser;
            ParametersExecute = syncParam;
            IsValid = true;
            var validationResult = ParametersExecute.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public TextParserController(ConvertPdfToTextParam syncParam, Globals globals) : base(globals)
        {
            Function = TextParserFunction.ConvertPdfToText;
            ConvertPdfToTextParameters = syncParam;
            IsValid = true;
            var validationResult = ConvertPdfToTextParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public TextParserController(IndexPDFDocumentsParam syncParam, Globals globals) : base(globals)
        {
            Function = TextParserFunction.IndexPDFDocuments;
            IndexPDFDocumentsParameters = syncParam;
            IsValid = true;
            var validationResult = IndexPDFDocumentsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public TextParserController(AnalyzeOModulesLogsPram syncParam, Globals globals) : base(globals)
        {
            Function = TextParserFunction.AnalyzeOModuleLogs;
            AnalyzeOModulesLogsPrameters = syncParam;
            IsValid = true;
            var validationResult = AnalyzeOModulesLogsPrameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public TextParserController(MeasureTextParserParam syncParam, Globals globals) : base(globals)
        {
            Function = TextParserFunction.MeasureTextparser;
            MeasureTextParserParameters = syncParam;
            IsValid = true;
            var validationResult = MeasureTextParserParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public TextParserController(ReIndexTextParserPatternSourceFieldsParam syncParam, Globals globals) : base(globals)
        {
            Function = TextParserFunction.ReIndexPatternSourceFields;
            ReIndexTextParserPatternSourceFieldsParameters = syncParam;
            IsValid = true;
            var validationResult = ReIndexTextParserPatternSourceFieldsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public TextParserController(CommandLine commandLine, Globals globals) :base(globals,commandLine)
        {
            IsValid = true;
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You have to provide an Function\n";
            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == TextParserFunction.ExecuteTextParser.ToString().ToLower())
                {
                    Function = TextParserFunction.ExecuteTextParser;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == TextParserFunction.ReIndexFields.ToString().ToLower())
                {
                    Function = TextParserFunction.ReIndexFields;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == TextParserFunction.ConvertPdfToText.ToString().ToLower())
                {
                    Function = TextParserFunction.ConvertPdfToText;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == TextParserFunction.AnalyzeOModuleLogs.ToString().ToLower())
                {
                    Function = TextParserFunction.AnalyzeOModuleLogs;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == TextParserFunction.ReIndexPatternSourceFields.ToString().ToLower())
                {
                    Function = TextParserFunction.AnalyzeOModuleLogs;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a valid Function\n";
                }
            }

            switch (Function)
            {
                case TextParserFunction.ExecuteTextParser:
                    ParametersExecute = new TextParserExecutePram();
                    ParametersExecute.MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath);
                    var validateResultAddVariables = ParametersExecute.ValidateParam(commandLine, globals);
                    IsValid = (validateResultAddVariables.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateResultAddVariables.Additional1;
                    break;
                case TextParserFunction.ReIndexFields:
                    ParametersReIndex = new ReIndexTextParserFieldsParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    var validateResult = ParametersReIndex.ValidateParam(commandLine, globals);
                    IsValid = (validateResult.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateResult.Additional1;
                    break;
                case TextParserFunction.ConvertPdfToText:
                    ConvertPdfToTextParameters = new ConvertPdfToTextParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    var validateConvertResult = ConvertPdfToTextParameters.ValidateParam(commandLine, globals);
                    IsValid = (validateConvertResult.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateConvertResult.Additional1;
                    break;
                case TextParserFunction.IndexPDFDocuments:
                    IndexPDFDocumentsParameters = new IndexPDFDocumentsParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    var validateIndexResult = IndexPDFDocumentsParameters.ValidateParam(commandLine, globals);
                    IsValid = (validateIndexResult.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateIndexResult.Additional1;
                    break;
                case TextParserFunction.AnalyzeOModuleLogs:
                    AnalyzeOModulesLogsPrameters = new AnalyzeOModulesLogsPram
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    var validateAnalyzeResult = AnalyzeOModulesLogsPrameters.ValidateParam(commandLine, globals);
                    IsValid = (validateAnalyzeResult.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateAnalyzeResult.Additional1;
                    break;
                case TextParserFunction.ReIndexPatternSourceFields:
                    ReIndexTextParserPatternSourceFieldsParameters = new ReIndexTextParserPatternSourceFieldsParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    var validateReindexPSResult = ReIndexTextParserPatternSourceFieldsParameters.ValidateParam(commandLine, globals);
                    IsValid = (validateReindexPSResult.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateReindexPSResult.Additional1;
                    break;
            }

        }
        public TextParserController() { }
    }
}
