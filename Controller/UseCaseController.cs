﻿using ImportExport_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutomationLibrary.Attributes;
using OntoMsg_Module.Logging;

namespace AutomationLibrary.Controller
{
    public enum UseCaseFunction
    {
        GetUseCasesByTypedTags,
        CreateGraphMLFile
    }
    public class UseCaseController : BaseController, IModuleController
    {
        public UseCaseFunction Function { get; set; }        

        public GetUseCaseListByTypedTagParam GetUseCaseListByTypedTagParameters { get; private set; }
        public CreateUseCaseGraphMLFileParam CreateUseCaseGraphMLFileParameters { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(UseCaseModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbResult = new StringBuilder();

                sbResult.AppendLine($"{nameof(UseCaseModule)} {nameof(Function)}:{nameof(UseCaseFunction.GetUseCasesByTypedTags)} {nameof(GetUseCaseListByTypedTagParam.IdRef)}:<id of reference with typed-tags to use cases>");
                sbResult.AppendLine($"{nameof(UseCaseModule)} {nameof(Function)}:{nameof(UseCaseFunction.CreateGraphMLFile)} {nameof(CreateUseCaseGraphMLFileParam.IdConfig)}:<id of config for export>");
                return sbResult.ToString();
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_UseCase_Module;

        public List<Type> Functions => new List<Type> { typeof(GetUseCaseListByTypedTagParam), typeof(CreateUseCaseGraphMLFileParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new UseCaseModule.UseCaseConnector(globals);

            try
            {

                switch(Function)
                {
                    case UseCaseFunction.GetUseCasesByTypedTags:
                        var request = new UseCaseModule.Models.GetUseCaseListRequestTypedTag(GetUseCaseListByTypedTagParameters.IdRef)
                        {
                            MessageOutput = GetUseCaseListByTypedTagParameters.MessageOutput
                        };

                        var taskQuery = await syncConnector.GetUseCaseListByTypedTag(request);

                        if (taskQuery.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = taskQuery.ResultState;
                            return result;
                        }

                        if (!taskQuery.Result.UseCases.Any())
                        {
                            result.Additional1 = "No UseCases found!";
                            GetUseCaseListByTypedTagParameters.MessageOutput?.OutputInfo(result.Additional1);
                            return result;
                        }


                        GetUseCaseListByTypedTagParameters.MessageOutput?.OutputInfo($"found {taskQuery.Result.UseCases.Count} Use Cases.");
                        break;
                    case UseCaseFunction.CreateGraphMLFile:
                        var requestCreateFile = new UseCaseModule.Models.CreateUseCaseGraphMLFileRequest(CreateUseCaseGraphMLFileParameters.IdConfig)
                        {
                            MessageOutput = CreateUseCaseGraphMLFileParameters.MessageOutput
                        };

                        var createResult = await syncConnector.CreateUseCaseGraphMLFile(requestCreateFile);

                        if (createResult.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = createResult.ResultState;
                            CreateUseCaseGraphMLFileParameters.MessageOutput?.OutputInfo(result.Additional1);
                            return result;
                        }

                        CreateUseCaseGraphMLFileParameters.MessageOutput?.OutputInfo($"created {createResult.Result.Count} paths.");
                        break;
                }
                
                return result;

            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new UseCaseModule.UseCaseConnector(globals);

            try
            {

                switch (Function)
                {
                    case UseCaseFunction.GetUseCasesByTypedTags:
                        var request = new UseCaseModule.Models.GetUseCaseListRequestTypedTag(GetUseCaseListByTypedTagParameters.IdRef)
                        {
                            MessageOutput = GetUseCaseListByTypedTagParameters.MessageOutput
                        };

                        var taskQuery = syncConnector.GetUseCaseListByTypedTag(request);
                        taskQuery.Wait();

                        if (taskQuery.Result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = taskQuery.Result.ResultState;
                            return result;
                        }

                        if (!taskQuery.Result.Result.UseCases.Any())
                        {
                            result.Additional1 = "No UseCases found!";
                            GetUseCaseListByTypedTagParameters.MessageOutput?.OutputInfo(result.Additional1);
                            return result;
                        }


                        GetUseCaseListByTypedTagParameters.MessageOutput?.OutputInfo($"found {taskQuery.Result.Result.UseCases.Count} Use Cases.");
                        break;
                    case UseCaseFunction.CreateGraphMLFile:
                        var requestCreateFile = new UseCaseModule.Models.CreateUseCaseGraphMLFileRequest(CreateUseCaseGraphMLFileParameters.IdConfig)
                        {
                            MessageOutput = CreateUseCaseGraphMLFileParameters.MessageOutput
                        };

                        var createResult = syncConnector.CreateUseCaseGraphMLFile(requestCreateFile);
                        createResult.Wait();

                        if (createResult.Result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = createResult.Result.ResultState;
                            return result;
                        }

                        if (createResult.Result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = createResult.Result.ResultState;
                            CreateUseCaseGraphMLFileParameters.MessageOutput?.OutputInfo(result.Additional1);
                            return result;
                        }

                        CreateUseCaseGraphMLFileParameters.MessageOutput?.OutputInfo($"created {createResult.Result.Result.Count} paths.");
                        break;
                }

                return result;

            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public UseCaseController(GetUseCaseListByTypedTagParam syncParam, Globals globals) : base(globals)
        {
            GetUseCaseListByTypedTagParameters = syncParam;
            IsValid = true;
            Function = UseCaseFunction.GetUseCasesByTypedTags;
            var validationResult = GetUseCaseListByTypedTagParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public UseCaseController(CreateUseCaseGraphMLFileParam syncParam, Globals globals) : base(globals)
        {
            CreateUseCaseGraphMLFileParameters = syncParam;
            IsValid = true;
            Function = UseCaseFunction.CreateGraphMLFile;
            var validationResult = CreateUseCaseGraphMLFileParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public UseCaseController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            
            
            IsValid = true;
            ErrorMessage = "";
            
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());
            if (key == null)
            {
                IsValid = false;
                ErrorMessage += $"You must provide a valid Function: [{nameof(UseCaseFunction.GetUseCasesByTypedTags)}]\n";
                return;
            }

            if (commandLine.AdditionalParameters[key].ToLower() == nameof(UseCaseFunction.GetUseCasesByTypedTags))
            {
                Function = UseCaseFunction.GetUseCasesByTypedTags;
            }
            else if (commandLine.AdditionalParameters[key].ToLower() == nameof(UseCaseFunction.CreateGraphMLFile))
            {
                Function = UseCaseFunction.CreateGraphMLFile;
            }
            else
            {
                IsValid = false;
                ErrorMessage += $"You must provide a valid Function: [{nameof(UseCaseFunction.GetUseCasesByTypedTags)}]\n";
                return;
            }

            switch (Function)
            {
                case UseCaseFunction.GetUseCasesByTypedTags:
                    GetUseCaseListByTypedTagParameters = new GetUseCaseListByTypedTagParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };

                    var validateSyncADPartners = GetUseCaseListByTypedTagParameters.ValidateParam(commandLine, globals);
                    IsValid = validateSyncADPartners.GUID == globals.LState_Success.GUID;
                    ErrorMessage = validateSyncADPartners.Additional1;
                    return;
                case UseCaseFunction.CreateGraphMLFile:
                    CreateUseCaseGraphMLFileParameters = new CreateUseCaseGraphMLFileParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    var validateCreateGraphMLFile = CreateUseCaseGraphMLFileParameters.ValidateParam(commandLine, globals);
                    IsValid = validateCreateGraphMLFile.GUID == globals.LState_Success.GUID;
                    ErrorMessage = validateCreateGraphMLFile.Additional1;
                    return;
            }
        }

        public UseCaseController() { }
    }
}
