﻿using FileManagementModule;
using FileManagementModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Attributes;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TypedTaggingModule.Connectors;
using TypedTaggingModule.Models;
using LogModule;

namespace AutomationLibrary.Controller
{
    public enum LogControllerFunction
    {
        SyncWindowsLogEntries = 0,
        SyncTextParser = 1,
        CheckUrls = 2,
        ImportCubewareLogs = 3
    }

    public class LogController : BaseController, IModuleController
    {

        public LogControllerFunction Function { get; private set; }

        public SyncWindowsLogEntriesToApplicationInsightsParam SyncWindowsLogEntriesToApplicationInsightsParameter { get; private set; }
        public SyncTextParserToApplicationInsightsParam SyncTextParserToApplicationInsightsParameter { get; private set; }
        public CheckUrlsParam CheckUrlsParameters { get; private set; }
        public ImportCubewareLogParam ImportCubewareLogParameter { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(LogModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"Module:{nameof(LogModule)} {nameof(Function)}:[{nameof(LogControllerFunction.SyncTextParser)}]");
                sbSyntax.AppendLine($"Module:{nameof(LogModule)} {nameof(Function)}:[{nameof(LogControllerFunction.SyncWindowsLogEntries)}]");
                sbSyntax.AppendLine($"Module:{nameof(LogModule)} {nameof(Function)}:[{nameof(LogControllerFunction.CheckUrls)}]");
                sbSyntax.AppendLine($"Module:{nameof(LogModule)} {nameof(Function)}:[{nameof(LogControllerFunction.ImportCubewareLogs)}] {nameof(ImportCubewareLogParam.IdConfig)}:<Import-Config>");
                return sbSyntax.ToString(); ;
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_Log_Module;

        public List<Type> Functions => new List<Type> 
        { 
            typeof(SyncTextParserToApplicationInsightsParam), 
            typeof(SyncWindowsLogEntriesToApplicationInsightsParam), 
            typeof(CheckUrlsParam),
            typeof(ImportCubewareLogParam)
        };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new LogModule.LogController(globals);

            switch (Function)
            {
                case LogControllerFunction.SyncWindowsLogEntries:
                    var requestImport = new LogModule.Models.SyncLogEntriesToAppInsightRequest(SyncWindowsLogEntriesToApplicationInsightsParameter.IdConfig, SyncWindowsLogEntriesToApplicationInsightsParameter.IdUser, SyncWindowsLogEntriesToApplicationInsightsParameter.Password)
                    {
                        MessageOutput = SyncWindowsLogEntriesToApplicationInsightsParameter.MessageOutput
                    };

                    try
                    {
                        var syncTask = await syncConnector.SyncLogEntriesToAppInsight(requestImport);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Updated Ontologies: {syncTask.Result.CountEntries}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case LogControllerFunction.SyncTextParser:
                    var requestTextParser = new LogModule.Models.SyncTextParserToApplicationInsightsRequest(SyncTextParserToApplicationInsightsParameter.IdConfig)
                    {
                        MessageOutput = SyncTextParserToApplicationInsightsParameter.MessageOutput
                    };

                    try
                    {
                        var syncTask = await syncConnector.SyncTextParserToApplicationInsights(requestTextParser);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Updated Ontologies: {syncTask.Result.DocumentCountSynced}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case LogControllerFunction.CheckUrls:
                    var checkUrlsRequest = new LogModule.Models.CheckUrlsRequest(CheckUrlsParameters.IdConfig, CheckUrlsParameters.CancellationTokenSource.Token)
                    {
                        MessageOutput = CheckUrlsParameters.MessageOutput
                    };

                    try
                    {
                        var syncTask = await syncConnector.CheckUrls(checkUrlsRequest);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Checked Urls: {syncTask.Result.UrlChecks.Count}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case LogControllerFunction.ImportCubewareLogs:
                    var importCubewareLogs = new LogModule.Models.ImportCubewareLogsRequest(ImportCubewareLogParameter.IdConfig, ImportCubewareLogParameter.CancellationTokenSource.Token)
                    {
                        MessageOutput = ImportCubewareLogParameter.MessageOutput
                    };

                    try
                    {
                        var importController = new CubewareLogImporter(globals);
                        var syncTask = await importController.ImportCubewareLogs(importCubewareLogs);

                        result = syncTask;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Finished!";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                default:
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The function is not valid!";

                    break;
            }

            return result;
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new LogModule.LogController(globals);

            switch (Function)
            {
                case LogControllerFunction.SyncWindowsLogEntries:
                    var requestImport = new LogModule.Models.SyncLogEntriesToAppInsightRequest(SyncWindowsLogEntriesToApplicationInsightsParameter.IdConfig, SyncWindowsLogEntriesToApplicationInsightsParameter.IdUser, SyncWindowsLogEntriesToApplicationInsightsParameter.Password)
                    {
                        MessageOutput = SyncWindowsLogEntriesToApplicationInsightsParameter.MessageOutput
                    };

                    try
                    {
                        var syncTask = syncConnector.SyncLogEntriesToAppInsight(requestImport);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Updated Ontologies: {syncTask.Result.Result.CountEntries}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case LogControllerFunction.SyncTextParser:
                    var requestTextParser = new LogModule.Models.SyncTextParserToApplicationInsightsRequest(SyncTextParserToApplicationInsightsParameter.IdConfig)
                    {
                        MessageOutput = SyncTextParserToApplicationInsightsParameter.MessageOutput
                    };

                    try
                    {
                        var syncTask = syncConnector.SyncTextParserToApplicationInsights(requestTextParser);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Updated Ontologies: {syncTask.Result.Result.DocumentCountSynced}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case LogControllerFunction.CheckUrls:
                    var checkUrlsRequest = new LogModule.Models.CheckUrlsRequest(CheckUrlsParameters.IdConfig, CheckUrlsParameters.CancellationTokenSource.Token)
                    {
                        MessageOutput = SyncTextParserToApplicationInsightsParameter.MessageOutput
                    };

                    try
                    {
                        var syncTask = syncConnector.CheckUrls(checkUrlsRequest);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Checked Urls: {syncTask.Result.Result.UrlChecks.Count}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case LogControllerFunction.ImportCubewareLogs:
                    var importCubewareLogs = new LogModule.Models.ImportCubewareLogsRequest(ImportCubewareLogParameter.IdConfig, ImportCubewareLogParameter.CancellationTokenSource.Token)
                    {
                        MessageOutput = ImportCubewareLogParameter.MessageOutput
                    };

                    try
                    {
                        var importController = new CubewareLogImporter(globals);
                        var syncTask = importController.ImportCubewareLogs(importCubewareLogs);
                        syncTask.Wait();

                        result = syncTask.Result;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Finished!";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                default:
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The function is not valid!";

                    break;
            }

            return result;
        }

        public LogController(SyncTextParserToApplicationInsightsParam syncParam, Globals globals) : base(globals)
        {
            SyncTextParserToApplicationInsightsParameter = syncParam;
            IsValid = true;
            Function = LogControllerFunction.SyncTextParser;
            var validationResult = SyncTextParserToApplicationInsightsParameter.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public LogController(SyncWindowsLogEntriesToApplicationInsightsParam syncParam, Globals globals) : base(globals)
        {
            SyncWindowsLogEntriesToApplicationInsightsParameter = syncParam;
            IsValid = true;
            Function = LogControllerFunction.SyncWindowsLogEntries;
            var validationResult = SyncWindowsLogEntriesToApplicationInsightsParameter.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public LogController(CheckUrlsParam syncParam, Globals globals) : base(globals)
        {
            CheckUrlsParameters = syncParam;
            IsValid = true;
            Function = LogControllerFunction.CheckUrls;
            var validationResult = CheckUrlsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public LogController(ImportCubewareLogParam syncParam, Globals globals) : base(globals)
        {
            ImportCubewareLogParameter = syncParam;
            IsValid = true;
            Function = LogControllerFunction.ImportCubewareLogs;
            var validationResult = ImportCubewareLogParameter.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public LogController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {

            IsValid = true;

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You must provide a function";

            }
            else
            {
                if (commandLine.AdditionalParameters[key] == nameof(LogControllerFunction.SyncTextParser))
                {
                    Function = LogControllerFunction.SyncTextParser;
                }
                else if (commandLine.AdditionalParameters[key] == nameof(LogControllerFunction.SyncWindowsLogEntries))
                {
                    Function = LogControllerFunction.SyncWindowsLogEntries;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += "You must provide a valid function";
                }
            }

            if (Function == LogControllerFunction.SyncTextParser)
            {
                SyncTextParserToApplicationInsightsParameter = new SyncTextParserToApplicationInsightsParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = SyncTextParserToApplicationInsightsParameter.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;

            }
            else if (Function == LogControllerFunction.SyncWindowsLogEntries)
            {
                SyncWindowsLogEntriesToApplicationInsightsParameter = new SyncWindowsLogEntriesToApplicationInsightsParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = SyncWindowsLogEntriesToApplicationInsightsParameter.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;

            }
            else if (Function == LogControllerFunction.CheckUrls)
            {
                CheckUrlsParameters = new CheckUrlsParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = CheckUrlsParameters.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;

            }
            else if (Function == LogControllerFunction.ImportCubewareLogs)
            {
                ImportCubewareLogParameter = new ImportCubewareLogParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = ImportCubewareLogParameter.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public LogController() { }
    }
}
