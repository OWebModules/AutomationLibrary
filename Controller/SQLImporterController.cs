﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using SQLImporter;
using SQLImporter.Models;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    public class SQLImporterController : BaseController, IModuleController
    {

        public SQLImporterParam SQLImporterParameters { get; private set; }
        
        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(SQLImporter).ToLower();
        }

        public override string Syntax
        {
            get
            {
                return $"{nameof(SQLImporter)} IdConfig:<Id> IdUser:<Id of Masteruser> Password:<Password of Masteruser>";
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_SQLImporter;

        public List<Type> Functions => new List<Type> { typeof(SQLImporterParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new ImportSQLRequest(SQLImporterParameters.CancellationTokenSource.Token, SQLImporterParameters.IdConfig)
            {
                IdUser = SQLImporterParameters.IdUser,
                Password = SQLImporterParameters.Password,
                MessageOutput = SQLImporterParameters.MessageOutput
            };
            var syncConnector = new SQLImporterConnector(globals);

            try
            {
                var syncTask = await syncConnector.ImportSQL(request);

                result = syncTask.ResultState;
                if (result.GUID == globals.LState_Success.GUID)
                {
                    result.Additional1 = $"Imported SQL";


                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new ImportSQLRequest(SQLImporterParameters.CancellationTokenSource.Token,SQLImporterParameters.IdConfig)
            {
                IdUser = SQLImporterParameters.IdUser,
                Password = SQLImporterParameters.Password,
                MessageOutput = SQLImporterParameters.MessageOutput
            };

            var syncConnector = new SQLImporterConnector(globals);

            try
            {
                var syncTask = syncConnector.ImportSQL(request);
                syncTask.Wait();

                result = syncTask.Result.ResultState;
                if (result.GUID == globals.LState_Success.GUID)
                {
                    result.Additional1 = $"Imported SQL";


                }
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public SQLImporterController(SQLImporterParam syncParam, Globals globals) : base(globals)
        {
            SQLImporterParameters = syncParam;
            IsValid = true;
            var validationResult = SQLImporterParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public SQLImporterController(CommandLine commandLine, Globals globals) : base(globals,commandLine)
        {
            
            IsValid = true;

            SQLImporterParameters = new SQLImporterParam()
            {
                MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
            };

            var validationResult = SQLImporterParameters.ValidateParam(commandLine.Globals);

            IsValid = validationResult.GUID == globals.LState_Success.GUID;
            
        }


        public SQLImporterController() { }
    }
}
