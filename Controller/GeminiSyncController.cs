﻿using GeminiConnectorModule;
using GeminiConnectorModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TFSConnectorModule;

namespace AutomationLibrary.Controller
{
    public enum GeminiFunction
    {
        SyncIssues,
        SyncTFSComments
    }
    public class GeminiSyncController : BaseController, IModuleController
    {
        
       public GeminiSyncParam Parameters { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(GeminiConnectorModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"{nameof(GeminiConnectorModule)} IdConfig:<Id> Function:{nameof(GeminiFunction.SyncIssues)} IdMasterPassword:<Id> MasterPassword:<password>");
                sbSyntax.AppendLine($"{nameof(GeminiConnectorModule)} IdConfig:<Id> Function:{nameof(GeminiFunction.SyncTFSComments)} IdMasterPassword:<Id> MasterPassword:<password> IdTFSConfig:<Id> Iduser:<Id> IdGroup:<Id>");
                return sbSyntax.ToString();
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_GeminiConnectorModule;

        public List<Type> Functions => new List<Type> { typeof(GeminiSyncParamProjects), typeof(GeminiSyncTFSCommentsParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            try
            {
                var syncConnector = new GeminiConnector(globals);
                switch (Parameters.Function)
                {
                    case GeminiFunction.SyncIssues:
                        var request = new SyncIssuesRequest(Parameters.GeminiSyncParamProjects.IdConfig, Parameters.GeminiSyncParamProjects.IdMasterUser, Parameters.GeminiSyncParamProjects.MasterPassword)
                        {
                            MessageOutput = Parameters.GeminiSyncParamProjects.MessageOutput
                        };

                        var syncTask = await syncConnector.SyncIssues(request);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            var sbResult = new StringBuilder();
                            sbResult.AppendLine($"Synced {syncTask.Result.CountSyncedIssues} Issues");
                            foreach (var item in syncTask.Result.ClosedIssueIds)
                            {
                                sbResult.AppendLine($"Closed Issue {item}");
                            }
                            result.Additional1 = sbResult.ToString();


                        }
                        return result;
                    case GeminiFunction.SyncTFSComments:
                        var tfsConnector = new TFSConnector(globals);
                        var syncTfsRequest = new TFSConnectorModule.Models.SyncTFSRequest(Parameters.GeminiSyncTFSCommentsParam.IdTFSConfig)
                        {
                            MessageOutput = Parameters.GeminiSyncTFSCommentsParam.MessageOutput
                        };

                        var syncResult = await tfsConnector.SyncTFS(syncTfsRequest);

                        result = syncResult.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var commentRequest = new TFSConnectorModule.Models.GetTFSCommentsRequest(Parameters.GeminiSyncTFSCommentsParam.IdTFSConfig) { RegexComment = @"#\S+-\d+" };
                        var getResult = await tfsConnector.GetTFSComments(commentRequest);

                        result = getResult.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        if (getResult.Result.ChangeSets.Any())
                        {
                            var syncRequest = new GeminiConnectorModule.Models.SyncIssueCommentsRequest(Parameters.GeminiSyncTFSCommentsParam.IdConfig,
                                getResult.Result.ChangeSets.Select(changeSet => new IssueComment
                                {
                                    Commit = changeSet.ChangeSet,
                                    Comment = changeSet.Description.Val_String,
                                    CreateDate = changeSet.CreateDate.Val_Date.Value,
                                    Url = changeSet.Url.Name
                                }).ToList(), Parameters.GeminiSyncTFSCommentsParam.IdMasterUser, Parameters.GeminiSyncTFSCommentsParam.MasterPassword, Parameters.GeminiSyncTFSCommentsParam.IdUser, Parameters.GeminiSyncTFSCommentsParam.IdGroup);

                            var syncCommentsTask = syncConnector.SyncSourceCodeComments(syncRequest);
                            syncCommentsTask.Wait();
                        }

                        return result;
                    default:
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "No valid Function provided!";
                        return result;
                }

            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }
        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            

            try
            {
                var syncConnector = new GeminiConnector(globals);
                switch (Parameters.Function)
                {
                    case GeminiFunction.SyncIssues:
                        var request = new SyncIssuesRequest(Parameters.GeminiSyncParamProjects.IdConfig, Parameters.GeminiSyncParamProjects.IdMasterUser, Parameters.GeminiSyncParamProjects.MasterPassword);
                        
                        var syncTask = syncConnector.SyncIssues(request);
                        
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            var sbResult = new StringBuilder();
                            sbResult.AppendLine($"Synced {syncTask.Result.Result.CountSyncedIssues} Issues");
                            foreach (var item in syncTask.Result.Result.ClosedIssueIds)
                            {
                                sbResult.AppendLine($"Closed Issue {item}");
                            }
                            result.Additional1 = sbResult.ToString();


                        }
                        return result;
                    case GeminiFunction.SyncTFSComments:
                        var tfsConnector = new TFSConnector(globals);
                        var syncTfsRequest = new TFSConnectorModule.Models.SyncTFSRequest(Parameters.GeminiSyncTFSCommentsParam.IdTFSConfig);

                        var syncResult = tfsConnector.SyncTFS(syncTfsRequest);
                        syncResult.Wait();

                        result = syncResult.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var commentRequest = new TFSConnectorModule.Models.GetTFSCommentsRequest(Parameters.GeminiSyncTFSCommentsParam.IdTFSConfig) { RegexComment = @"#\S+-\d+" };
                        var getResult = tfsConnector.GetTFSComments(commentRequest);
                        getResult.Wait();

                        result = getResult.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        if (getResult.Result.Result.ChangeSets.Any())
                        {
                            var syncRequest = new GeminiConnectorModule.Models.SyncIssueCommentsRequest(CommandLine.IdConfig,
                                getResult.Result.Result.ChangeSets.Select(changeSet => new IssueComment
                                {
                                    Commit = changeSet.ChangeSet,
                                    Comment = changeSet.Description.Val_String,
                                    CreateDate = changeSet.CreateDate.Val_Date.Value,
                                    Url = changeSet.Url.Name
                                }).ToList(), Parameters.GeminiSyncTFSCommentsParam.IdMasterUser, Parameters.GeminiSyncTFSCommentsParam.MasterPassword, Parameters.GeminiSyncTFSCommentsParam.IdUser, Parameters.GeminiSyncTFSCommentsParam.IdGroup);

                            var syncCommentsTask = syncConnector.SyncSourceCodeComments(syncRequest);
                            syncCommentsTask.Wait();
                        }

                        return result;
                    default:
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "No valid Function provided!";
                        return result;
                }
                
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }



        }

        public GeminiSyncController(GeminiSyncTFSCommentsParam syncParam, Globals globals) : base(globals)
        {
            
            Parameters = new GeminiSyncParam
            {
                GeminiSyncParamProjects = syncParam,
                MessageOutput = syncParam.MessageOutput
            };

            Parameters.Function = GeminiFunction.SyncTFSComments;

            IsValid = true;
            var validationResult = Parameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public GeminiSyncController(GeminiSyncParamProjects syncParam, Globals globals): base(globals)
        {
            Parameters = new GeminiSyncParam
            {
                GeminiSyncParamProjects = syncParam,
                MessageOutput = syncParam.MessageOutput
            };

            Parameters.Function = GeminiFunction.SyncIssues;

            IsValid = true;
            var validationResult = Parameters.ValidateParam(globals);
            
            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
                
        }

        public GeminiSyncController(CommandLine commandLine, Globals globals) :base(globals,commandLine)
        {
            Parameters = new GeminiSyncParam();

            var idConfig = commandLine.IdConfig;
            var parameterResult = commandLine.GetAdditionalParamter(nameof(GeminiSyncParamProjects.IdMasterUser));

            if (parameterResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage += "The MasterUser-Id is not valid!";
                return;
            }

            var idMasterUser = parameterResult.Additional1;

            parameterResult = commandLine.GetAdditionalParamter(nameof(GeminiSyncParamProjects.MasterPassword));

            if (parameterResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage += "The MasterPassword is not valid!";
                return;
            }

            var masterPassword = parameterResult.Additional1;

            parameterResult = commandLine.GetAdditionalParamter(nameof(GeminiSyncParam.Function));

            if (parameterResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage += "Function is not valid!";
                return;
            }

            var function = parameterResult.Additional1;

            if (function == nameof(GeminiFunction.SyncTFSComments))
            {
                Parameters.Function = GeminiFunction.SyncTFSComments;

                var paramResult = commandLine.GetAdditionalParamter(nameof(GeminiSyncTFSCommentsParam.IdTFSConfig));
                if (paramResult.GUID == globals.LState_Error.GUID)
                {
                    IsValid = false;
                    ErrorMessage += paramResult.Additional1 + '\n';
                }
                var idTfsConfig = paramResult.Additional1;

                paramResult = commandLine.GetAdditionalParamter(nameof(GeminiSyncTFSCommentsParam.IdUser));
                if (paramResult.GUID == globals.LState_Error.GUID)
                {
                    IsValid = false;
                    ErrorMessage += paramResult.Additional1 + '\n';
                }

                var idUser = paramResult.Additional1;

                paramResult = commandLine.GetAdditionalParamter(nameof(GeminiSyncTFSCommentsParam.IdGroup));
                if (paramResult.GUID == globals.LState_Error.GUID)
                {
                    IsValid = false;
                    ErrorMessage += paramResult.Additional1 + '\n';
                }
                var idGroup = paramResult.Additional1;

                Parameters.GeminiSyncTFSCommentsParam = new GeminiSyncTFSCommentsParam()
                {
                    IdConfig = idConfig,
                    IdMasterUser = idMasterUser,
                    MasterPassword = masterPassword,
                    IdUser = idUser,
                    IdGroup = idGroup,
                    IdTFSConfig = idTfsConfig
                };
            }
            else if (function == nameof(GeminiFunction.SyncIssues))
            {
                Parameters.Function = GeminiFunction.SyncIssues;
                Parameters.GeminiSyncParamProjects = new GeminiSyncParamProjects
                {
                    IdConfig = idConfig,
                    IdMasterUser = idMasterUser,
                    MasterPassword = masterPassword,
                };
            }

            var validation = Parameters.ValidateParam(globals);

            if (validation.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage += validation.Additional1;
            }
        }

        public GeminiSyncController() {  }
    }

}
