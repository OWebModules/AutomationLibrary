﻿using FileManagementModule;
using FileManagementModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Attributes;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TypedTaggingModule.Connectors;
using TypedTaggingModule.Models;

namespace AutomationLibrary.Controller
{
    public enum LocalizationFunction
    {
        ImportCultures = 0,
        CompleteZeitpunktOntologies = 1
    }

    public class LocalizationController : BaseController, IModuleController
    {

        public LocalizationFunction Function { get; private set; }

        public ImportCulturesParam ImportCulturesParamter { get; private set; }
        public CompleteZeitpunktOntologiesParam CompleteZeitpunktOntologiesParameters { get; private set; }


        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(LocalizationModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"Module:{nameof(LocalizationModule)} {nameof(Function)}:[{nameof(LocalizationFunction.ImportCultures)}]");
                sbSyntax.AppendLine($"Module:{nameof(LocalizationModule)} {nameof(Function)}:[{nameof(LocalizationFunction.CompleteZeitpunktOntologies)}] [{nameof(CompleteZeitpunktOntologiesParam.IdConfig)}:<Config-id>]");
                return sbSyntax.ToString(); ;
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_Localizing_Manager;

        public List<Type> Functions => new List<Type> { typeof(ImportCulturesParam), typeof(CompleteZeitpunktOntologiesParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new LocalizationModule.LocalizationController(globals);

            switch (Function)
            {
                case LocalizationFunction.ImportCultures:
                    var requestImport = new LocalizationModule.Models.ImportCulturesRequest()
                    {
                        MessageOutput = ImportCulturesParamter.MessageOutput
                    };

                    try
                    {
                        var syncTask = await syncConnector.ImportCultures(requestImport);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Updated Ontologies: {syncTask.Result.CountImported}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;

                case LocalizationFunction.CompleteZeitpunktOntologies:
                    var zeitPunktController = new LocalizationModule.ZeitpunktController(globals);
                    var completeRequest = new LocalizationModule.Models.CompleteZeitpunktOntologiesByNamesRequest()
                    {
                        MessageOutput = CompleteZeitpunktOntologiesParameters.MessageOutput,
                        IdConfig = CompleteZeitpunktOntologiesParameters.IdConfig
                    };

                    try
                    {
                        var syncTask = await zeitPunktController.CompleteZeitpunktOntologiesByNames(completeRequest);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Completed Ontologies: {syncTask.Result.SucceededCompletes.Count}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                default:
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The function is not valid!";

                    break;
            }

            return result;
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new LocalizationModule.LocalizationController(globals);

            switch (Function)
            {
                case LocalizationFunction.ImportCultures:
                    var requestImport = new LocalizationModule.Models.ImportCulturesRequest()
                    {
                        MessageOutput = ImportCulturesParamter.MessageOutput
                    };
                    try
                    {
                        var syncTask = syncConnector.ImportCultures(requestImport);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Updated Ontologies: {syncTask.Result.Result.CountImported}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                case LocalizationFunction.CompleteZeitpunktOntologies:
                    var zeitPunktController = new LocalizationModule.ZeitpunktController(globals);
                    var completeRequest = new LocalizationModule.Models.CompleteZeitpunktOntologiesByNamesRequest()
                    {
                        MessageOutput = ImportCulturesParamter.MessageOutput,
                        IdConfig = CompleteZeitpunktOntologiesParameters.IdConfig
                    };

                    try
                    {
                        var syncTask = zeitPunktController.CompleteZeitpunktOntologiesByNames(completeRequest);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            result.Additional1 = $"Completed Ontologies: {syncTask.Result.Result.SucceededCompletes.Count}";
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;

                    }
                    break;
                default:
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The function is not valid!";

                    break;
            }

            return result;
        }

        public LocalizationController(ImportCulturesParam syncParam, Globals globals) : base(globals)
        {
            ImportCulturesParamter = syncParam;
            Function = LocalizationFunction.ImportCultures;
            IsValid = true;
            var validationResult = ImportCulturesParamter.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public LocalizationController(CompleteZeitpunktOntologiesParam syncParam, Globals globals) : base(globals)
        {
            CompleteZeitpunktOntologiesParameters = syncParam;
            Function = LocalizationFunction.CompleteZeitpunktOntologies;
            IsValid = true;
            var validationResult = CompleteZeitpunktOntologiesParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public LocalizationController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {

            IsValid = true;

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You must provide a function";

            }
            else
            {
                if (commandLine.AdditionalParameters[key] == nameof(LocalizationFunction.ImportCultures))
                {
                    Function = LocalizationFunction.ImportCultures;
                }
                else if (commandLine.AdditionalParameters[key] == nameof(LocalizationFunction.CompleteZeitpunktOntologies))
                {
                    Function = LocalizationFunction.CompleteZeitpunktOntologies;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += "You must provide a valid function";
                }
            }

            if (Function == LocalizationFunction.ImportCultures)
            {
                ImportCulturesParamter = new ImportCulturesParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = ImportCulturesParamter.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;

            }
            else if (Function == LocalizationFunction.CompleteZeitpunktOntologies)
            {
                CompleteZeitpunktOntologiesParameters = new CompleteZeitpunktOntologiesParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = CompleteZeitpunktOntologiesParameters.ValidateParam(commandLine, globals);
                IsValid = validationResult.GUID == globals.LState_Success.GUID;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public LocalizationController() { }
    }
}
