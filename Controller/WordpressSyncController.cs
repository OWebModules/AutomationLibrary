﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordPressSyncModule.Models;

namespace AutomationLibrary.Controller
{
    public class WordpressSyncController : BaseController, IModuleController
    {
        public clsOntologyItem Module => Modules.Config.LocalData.Object_WordpressSync_Module;

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(WordPressSyncModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                return $"{nameof(WordPressSyncModule)} [{nameof(SyncWordpressPostsParam.IdConfig)}:<id-config>";
            }
        }

        public List<Type> Functions => new List<Type> { typeof(SyncWordpressPostsParam) };

        public SyncWordpressPostsParam SyncWordpressPostsParameters { get; private set; }

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new WordPressSyncModule.WordpressSyncConnector(globals);

            try
            {

                var request = new SyncWordpressPostsRequest(SyncWordpressPostsParameters.IdConfig, SyncWordpressPostsParameters.MasterPassword)
                {
                    MessageOutput = SyncWordpressPostsParameters.MessageOutput
                };

                var taskQuery = await syncConnector.SyncWordpressPosts(request);

                if (taskQuery.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result = taskQuery.ResultState;
                    return result;
                }

                if (!taskQuery.Result.PostItems.Any())
                {
                    result.Additional1 = "No Posts found!";
                    return result;
                }

                return result;

            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }
        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new WordPressSyncModule.WordpressSyncConnector(globals);

            try
            {

                var request = new SyncWordpressPostsRequest(SyncWordpressPostsParameters.IdConfig, SyncWordpressPostsParameters.MasterPassword)
                {
                    MessageOutput = SyncWordpressPostsParameters.MessageOutput
                };

                var taskQuery = syncConnector.SyncWordpressPosts(request);
                taskQuery.Wait();

                if (taskQuery.Result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result = taskQuery.Result.ResultState;
                    return result;
                }

                if (!taskQuery.Result.Result.PostItems.Any())
                {
                    result.Additional1 = "No Posts found!";
                    return result;
                }

                return result;

            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public WordpressSyncController(SyncWordpressPostsParam syncParam, Globals globals) : base(globals)
        {
            SyncWordpressPostsParameters = syncParam;
            IsValid = true;
            var validationResult = SyncWordpressPostsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public WordpressSyncController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            IsValid = true;
            ErrorMessage = "";

            var request = new SyncWordpressPostsParam
            {
                MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
            };

            var validateSyncADPartners = request.ValidateParam(commandLine, globals);
            IsValid = validateSyncADPartners.GUID == globals.LState_Success.GUID;
            ErrorMessage = validateSyncADPartners.Additional1;
            return;
        }

        public WordpressSyncController() { }
    }
}
