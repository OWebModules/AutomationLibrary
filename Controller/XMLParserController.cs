﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    public enum XMLParserFunction
    {
        ParseXMLField = 0
    }

    public class XMLParserController : BaseController, IModuleController
    {
        

        public XMLParserFunction ParserFunction { get; private set; }

        public ParseXMLFieldParam ParseXMLFieldParameters { get; private set; }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_XMLParserModule;

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(XMLParserModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"{nameof(XMLParserModule)} {nameof(CommandLine.IdConfig)}:<Id> Function:{nameof(XMLParserFunction.ParseXMLField)}");
                return sbSyntax.ToString();
            }
        }

        public List<Type> Functions => new List<Type> { typeof(ParseXMLFieldParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            
            var syncConnector = new XMLParserModule.XMLParserController(globals);

            try
            {
                switch (ParserFunction)
                {
                    case XMLParserFunction.ParseXMLField:
                        var request = new XMLParserModule.Models.ParseXMLFieldRequest(ParseXMLFieldParameters.IdConfig);
                        var parseResult = await syncConnector.ParseXMLField(request);

                        result = parseResult.ResultState;

                        return result;
                    default:
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "No valid Function provided!";
                        return result;
                }

            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }



        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;


            var syncConnector = new XMLParserModule.XMLParserController(globals);

            try
            {
                switch (ParserFunction)
                {
                    case XMLParserFunction.ParseXMLField:
                        var request = new XMLParserModule.Models.ParseXMLFieldRequest(ParseXMLFieldParameters.IdConfig);
                        var parseResult = syncConnector.ParseXMLField(request);
                        parseResult.Wait();

                        result = parseResult.Result.ResultState;

                        return result;
                    default:
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "No valid Function provided!";
                        return result;
                }

            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }



        }

        public XMLParserController(ParseXMLFieldParam parameters, Globals globals) : base(globals)
        {

            IsValid = true;

            ParserFunction = XMLParserFunction.ParseXMLField;
            ParseXMLFieldParameters = parameters;
            var validationResult = ParseXMLFieldParameters.ValidateParam(globals);
            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
            return;

        }

        public XMLParserController(CommandLine commandLine, Globals globals) :base(globals,commandLine)
        {
            IsValid = true;
            if (string.IsNullOrEmpty(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid!";
                IsValid = false;
                return;
            }

            if (!globals.is_GUID(commandLine.IdConfig))
            {
                ErrorMessage = "IdConfig is invalid (no GUID)!";
                IsValid = false;
                return;
            }

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());
            if (key == null)
            {
                IsValid = false;
                ErrorMessage += $"You have to provide a Function: [{nameof(XMLParserFunction.ParseXMLField)}]\n";

            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == nameof(XMLParserFunction.ParseXMLField).ToLower())
                {
                    ParserFunction = XMLParserFunction.ParseXMLField;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += $"You have to provide a Function: [{nameof(XMLParserFunction.ParseXMLField)}]\n";
                }

            }

            if (ParserFunction == XMLParserFunction.ParseXMLField)
            {
                ParseXMLFieldParameters = new ParseXMLFieldParam { IdConfig = commandLine.IdConfig, MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath) };
                var validationResult = ParseXMLFieldParameters.ValidateParam(globals);
                if (validationResult.GUID == globals.LState_Error.GUID)
                {
                    IsValid = false;
                    ErrorMessage += validationResult.Additional1;
                }
            }
            
        }

        public XMLParserController() { }
    }
}
