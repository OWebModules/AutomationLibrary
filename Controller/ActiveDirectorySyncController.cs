﻿using ImportExport_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ActiveDirectorySyncModule;
using AutomationLibrary.Attributes;
using OntoMsg_Module.Logging;
using ActiveDirectorySyncModule.Models;

namespace AutomationLibrary.Controller
{
    public enum ActiveDirectorySyncFunction
    {
        SyncPartners = 0,
        CompareGroupMembers = 1,
        GetUserGroups = 2
    }
    public class ActiveDirectorySyncController : BaseController, IModuleController
    {
        public ActiveDirectorySyncFunction Function { get; set; }        

        public SyncADPartnersParam SyncADPartnersParameters { get; private set; }
        public CompareAdGroupMembersParam CompareAdGroupMembersParameters { get; private set; }

        public GetUserGroupsParam GetUserGroupsParameters { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(ActiveDirectorySyncModule).ToLower();   
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();

                sbSyntax.AppendLine($"{nameof(ActiveDirectorySyncModule)} {nameof(Functions)}:{ActiveDirectorySyncFunction.SyncPartners.ToString()}  [{nameof(SyncADPartnersParam.PartnerIds)}:<PartnerId,PartnerId,PartnerId...>|{nameof(SyncADPartnersParam.AllPartners)}:true]");
                sbSyntax.AppendLine($"{nameof(ActiveDirectorySyncModule)} {nameof(Functions)}:{ActiveDirectorySyncFunction.CompareGroupMembers.ToString()}  {nameof(CompareAdGroupMembersParam.SrcContextPrincipalName)}:<Name of the Context-Principal> {nameof(CompareAdGroupMembersParam.SrcGroupFilter)}:<Filter for Group-search> {nameof(CompareAdGroupMembersParam.SrcPrincipalPath)}:<Path to search groups>");
                sbSyntax.AppendLine($"{nameof(ActiveDirectorySyncModule)} {nameof(Functions)}:{ActiveDirectorySyncFunction.GetUserGroups.ToString()}  {nameof(GetUserGroupsParam.IdConfig)}:<id of configuration>");
                sbSyntax.AppendLine($"{nameof(CompareAdGroupMembersParam.DstContextPrincipalName)}:<Name of the Context-Principal> {nameof(CompareAdGroupMembersParam.DstGroupFilter)}:<Filter for Group-search> {nameof(CompareAdGroupMembersParam.DstPrincipalPath)}:<Path to search groups>");

                return sbSyntax.ToString();

            }
        }

        public clsOntologyItem Module => AutomationLibrary.Modules.Config.LocalData.Object_ActiveDirectorySyncModule;

        public List<Type> Functions => new List<Type> { typeof(SyncADPartnersParam), typeof(CompareAdGroupMembersParam), typeof(GetUserGroupsParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new ActiveDirectorySyncModule.ActiveDirectorySyncController(globals);

            try
            {
                switch (Function)
                {
                    case ActiveDirectorySyncFunction.SyncPartners:
                        var request = new ActiveDirectorySyncModule.Models.ComQueryRequest
                        {
                            IdPartners = SyncADPartnersParameters.PartnerIds.Select(partner => partner.Id).ToList(),
                            AllPartners = SyncADPartnersParameters.AllPartners,
                            MessageOutput = SyncADPartnersParameters.MessageOutput
                        };

                        var taskQuery = await syncConnector.QueryActiveDirectory(request);

                        if (taskQuery.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = taskQuery.ResultState;
                            return result;
                        }

                        if (!taskQuery.Result.PartnerList.Any())
                        {
                            result.Additional1 = "No Partners found!";
                            return result;
                        }

                        var taskSync = await syncConnector.UpdatePartners(taskQuery.Result);

                        if (taskSync.GUID == globals.LState_Error.GUID)
                        {
                            result = taskQuery.ResultState;
                            return result;
                        }

                        result.Additional1 = "Sync is completed!";
                        return result;
                    case ActiveDirectorySyncFunction.CompareGroupMembers:
                        var queryRequestSrc = new GetGroupMembersRequest(CompareAdGroupMembersParameters.SrcGroupFilter, CompareAdGroupMembersParameters.SrcPrincipalPath, CompareAdGroupMembersParameters.SrcContextPrincipalName)
                        {
                            MessageOutput = CompareAdGroupMembersParameters.MessageOutput
                        };
                        var queryResultSrc = await syncConnector.GetGroupMembers(queryRequestSrc);

                        result = queryResultSrc.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            CompareAdGroupMembersParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }


                        var queryRequestDst = new GetGroupMembersRequest(CompareAdGroupMembersParameters.DstGroupFilter, CompareAdGroupMembersParameters.DstPrincipalPath, CompareAdGroupMembersParameters.DstContextPrincipalName)
                        {
                            MessageOutput = CompareAdGroupMembersParameters.MessageOutput
                        };
                        var queryResultDst = await syncConnector.GetGroupMembers(queryRequestDst);

                        result = queryResultDst.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            CompareAdGroupMembersParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        var compareRequest = new CompareGroupMembersRequest(queryResultSrc.Result.GroupMembers.Select(res => res.Result).ToList(), queryResultDst.Result.GroupMembers.Select(res => res.Result).ToList())
                        {
                            MessageOutput = CompareAdGroupMembersParameters.MessageOutput
                        };

                        var compareResult = await syncConnector.CompareGroupMembers(compareRequest);

                        result = compareResult.ResultState;

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            CompareAdGroupMembersParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"Groups in Src: {compareResult.Result.SrcGroups.Count}");
                        CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"Groups in Dst: {compareResult.Result.DstGroups.Count}");
                        CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"Members in Src: {compareResult.Result.InSrc.Count}");
                        CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"Members in Dst: {compareResult.Result.InDst.Count}");
                        CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"Distinct Names in Src: {compareResult.Result.SrcDistinguishedNames.Count}");
                        CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"Distinct Names in Dst: {compareResult.Result.DstDistinguishedNames.Count}");

                        foreach (var inDstNotInSrc in compareResult.Result.InDstNotInSrc)
                        {
                            CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"In Dest not in Source: {inDstNotInSrc.SamAccountName}");
                        }

                        foreach (var inSrcNotInDst in compareResult.Result.InSrcNotInDst)
                        {
                            CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"In Source not in Dest: {inSrcNotInDst.SamAccountName}");
                        }

                        if (!compareResult.Result.InDstNotInSrc.Any() && !compareResult.Result.InSrcNotInDst.Any())
                        {
                            CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"Equal.");
                        }

                        return result;
                    case ActiveDirectorySyncFunction.GetUserGroups:
                        var requestGetUserGroups = new ActiveDirectorySyncModule.Models.GetUserGroupsRequest(GetUserGroupsParameters.IdConfig)
                        {
                            MessageOutput = GetUserGroupsParameters.MessageOutput
                        };

                        var taskUserGroups = await syncConnector.GetUserGroups(requestGetUserGroups);

                        if (taskUserGroups.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = taskUserGroups.ResultState;
                            return result;
                        }
                        
                        if (!taskUserGroups.Result.Any())
                        {
                            result.Additional1 = "No Groups found!";
                            return result;
                        }

                        result.Additional1 = "Sync is completed!";
                        return result;

                }

                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new ActiveDirectorySyncModule.ActiveDirectorySyncController(globals);

            try
            {

                switch (Function)
                {
                    case ActiveDirectorySyncFunction.SyncPartners:
                        var request = new ActiveDirectorySyncModule.Models.ComQueryRequest
                        {
                            IdPartners = SyncADPartnersParameters.PartnerIds.Select(partner => partner.Id).ToList(),
                            AllPartners = SyncADPartnersParameters.AllPartners,
                            MessageOutput = SyncADPartnersParameters.MessageOutput
                        };

                        var taskQuery = syncConnector.QueryActiveDirectory(request);
                        taskQuery.Wait();

                        if (taskQuery.Result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = taskQuery.Result.ResultState;
                            return result;
                        }

                        if (!taskQuery.Result.Result.PartnerList.Any())
                        {
                            result.Additional1 = "No Partners found!";
                            return result;
                        }

                        var taskSync = syncConnector.UpdatePartners(taskQuery.Result.Result);
                        taskSync.Wait();

                        if (taskSync.Result.GUID == globals.LState_Error.GUID)
                        {
                            result = taskQuery.Result.ResultState;
                            return result;
                        }

                        result.Additional1 = "Sync is completed!";
                        return result;
                    case ActiveDirectorySyncFunction.CompareGroupMembers:
                        var queryRequestSrc = new GetGroupMembersRequest(CompareAdGroupMembersParameters.SrcGroupFilter, CompareAdGroupMembersParameters.SrcPrincipalPath, CompareAdGroupMembersParameters.SrcContextPrincipalName)
                        {
                            MessageOutput = CompareAdGroupMembersParameters.MessageOutput
                        };
                        var queryResultSrc = syncConnector.GetGroupMembers(queryRequestSrc);
                        queryResultSrc.Wait();

                        result = queryResultSrc.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            CompareAdGroupMembersParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }


                        var queryRequestDst = new GetGroupMembersRequest(CompareAdGroupMembersParameters.DstGroupFilter, CompareAdGroupMembersParameters.DstPrincipalPath, CompareAdGroupMembersParameters.DstContextPrincipalName)
                        {
                            MessageOutput = CompareAdGroupMembersParameters.MessageOutput
                        };
                        var queryResultDst = syncConnector.GetGroupMembers(queryRequestDst);
                        queryResultDst.Wait();

                        result = queryResultDst.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            CompareAdGroupMembersParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        var compareRequest = new CompareGroupMembersRequest(queryResultSrc.Result.Result.GroupMembers.Select(res => res.Result).ToList(), queryResultDst.Result.Result.GroupMembers.Select(res => res.Result).ToList())
                        {
                            MessageOutput = CompareAdGroupMembersParameters.MessageOutput
                        };

                        var compareResult = syncConnector.CompareGroupMembers(compareRequest);
                        compareResult.Wait();

                        result = compareResult.Result.ResultState;

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            CompareAdGroupMembersParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"Groups in Src: {compareResult.Result.Result.SrcGroups.Count}");
                        CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"Groups in Dst: {compareResult.Result.Result.DstGroups.Count}");
                        CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"Members in Src: {compareResult.Result.Result.InSrc.Count}");
                        CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"Members in Dst: {compareResult.Result.Result.InDst.Count}");
                        CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"Distinct Names in Src: {compareResult.Result.Result.SrcDistinguishedNames.Count}");
                        CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"Distinct Names in Dst: {compareResult.Result.Result.DstDistinguishedNames.Count}");
                        foreach (var inDstNotInSrc in compareResult.Result.Result.InDstNotInSrc)
                        {
                            CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"In Dest not in Source: {inDstNotInSrc.SamAccountName}");
                        }

                        foreach (var inSrcNotInDst in compareResult.Result.Result.InSrcNotInDst)
                        {
                            CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"In Source not in Dest: {inSrcNotInDst.SamAccountName}");
                        }

                        if (!compareResult.Result.Result.InDstNotInSrc.Any() && !compareResult.Result.Result.InSrcNotInDst.Any())
                        {
                            CompareAdGroupMembersParameters.MessageOutput?.OutputInfo($"Equal.");
                        }

                        return result;

                }

                return result;

            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public ActiveDirectorySyncController(SyncADPartnersParam syncParam, Globals globals) : base(globals)
        {
            SyncADPartnersParameters = syncParam;
            IsValid = true;
            Function = ActiveDirectorySyncFunction.SyncPartners;
            var validationResult = SyncADPartnersParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public ActiveDirectorySyncController(CompareAdGroupMembersParam syncParam, Globals globals) : base(globals)
        {
            CompareAdGroupMembersParameters = syncParam;
            IsValid = true;
            Function = ActiveDirectorySyncFunction.CompareGroupMembers;
            var validationResult = CompareAdGroupMembersParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public ActiveDirectorySyncController(GetUserGroupsParam syncParam, Globals globals) : base(globals)
        {
            GetUserGroupsParameters = syncParam;
            IsValid = true;
            Function = ActiveDirectorySyncFunction.GetUserGroups;
            var validationResult = GetUserGroupsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public ActiveDirectorySyncController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            
            
            IsValid = true;
            ErrorMessage = "";
            
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());
            if (key == null)
            {
                IsValid = false;
                ErrorMessage += $"You must provide a valid Function: [{nameof(ActiveDirectorySyncFunction.SyncPartners)}]\n";
                return;
            }

            if (commandLine.AdditionalParameters[key].ToLower() == nameof(ActiveDirectorySyncFunction.SyncPartners))
            {
                Function = ActiveDirectorySyncFunction.SyncPartners;
            }
            else if (commandLine.AdditionalParameters[key].ToLower() == nameof(ActiveDirectorySyncFunction.CompareGroupMembers))
            {
                Function = ActiveDirectorySyncFunction.CompareGroupMembers;
            }
            else if (commandLine.AdditionalParameters[key].ToLower() == nameof(ActiveDirectorySyncFunction.GetUserGroups))
            {
                Function = ActiveDirectorySyncFunction.GetUserGroups;
            }
            else
            {
                IsValid = false;
                ErrorMessage += $"You must provide a valid Function: [{nameof(ActiveDirectorySyncFunction.SyncPartners)}]\n";
                return;
            }

            switch (Function)
            {
                case ActiveDirectorySyncFunction.SyncPartners:
                    SyncADPartnersParameters = new SyncADPartnersParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };

                    var validateSyncADPartners = SyncADPartnersParameters.ValidateParam(commandLine, globals);
                    IsValid = validateSyncADPartners.GUID == globals.LState_Success.GUID;
                    ErrorMessage = validateSyncADPartners.Additional1;
                    return;
                case ActiveDirectorySyncFunction.CompareGroupMembers:
                    CompareAdGroupMembersParameters = new CompareAdGroupMembersParam()
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };

                    var validateCompare = CompareAdGroupMembersParameters.ValidateParam(commandLine, globals);
                    IsValid = validateCompare.GUID == globals.LState_Success.GUID;
                    ErrorMessage = validateCompare.Additional1;
                    return;
                case ActiveDirectorySyncFunction.GetUserGroups:
                    GetUserGroupsParameters = new GetUserGroupsParam()
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };

                    var validateUserGroups = CompareAdGroupMembersParameters.ValidateParam(commandLine, globals);
                    IsValid = validateUserGroups.GUID == globals.LState_Success.GUID;
                    ErrorMessage = validateUserGroups.Additional1;
                    return;

            }
        }

        public ActiveDirectorySyncController() { }
    }
}
