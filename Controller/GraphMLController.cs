﻿using ImportExport_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutomationLibrary.Attributes;
using OntoMsg_Module.Logging;

namespace AutomationLibrary.Controller
{
    public enum GraphMLFunction
    {
        CreateOntologyGraphMLFile,
        ExportTypedTags,
        ExportERModel
    }
    public class GraphMLController : BaseController, IModuleController
    {
        public GraphMLFunction Function { get; set; }        
        public CreateOntologyGraphMLFileParam CreateOntologyGraphMLFileParameters { get; private set; }
        public ExportTypedTagsToGraphMLParam ExportTypedTagsToGraphMLParameters { get; private set; }

        public ExportERModelToGraphMLParam ExportERModelToGraphMLParameters { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(GraphMLConnector).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbResult = new StringBuilder();

                sbResult.AppendLine($"{nameof(GraphMLConnector)} {nameof(Function)}:{nameof(GraphMLFunction.CreateOntologyGraphMLFile)} {nameof(CreateOntologyGraphMLFileParam.IdOntology)}:<id of ontology>");
                sbResult.AppendLine($"{nameof(GraphMLConnector)} {nameof(Function)}:{nameof(GraphMLFunction.ExportTypedTags)} {nameof(ExportTypedTagsToGraphMLParam.IdConfig)}:<id of config>");
                sbResult.AppendLine($"{nameof(GraphMLConnector)} {nameof(Function)}:{nameof(GraphMLFunction.ExportERModel)} {nameof(ExportTypedTagsToGraphMLParam.IdConfig)}:<id of config>");
                return sbResult.ToString();
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_GraphControllerModule;

        public List<Type> Functions => new List<Type> { typeof(CreateOntologyGraphMLFileParam), typeof(ExportTypedTagsToGraphMLParam), typeof(ExportERModelToGraphMLParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new GraphMLConnector.GraphMLConnector(globals);

            try
            {

                switch(Function)
                {
                    case GraphMLFunction.CreateOntologyGraphMLFile:
                        var requestCreateFile = new GraphMLConnector.Models.ExportOntologyToMLGraphRequest(
                            CreateOntologyGraphMLFileParameters.IdOntology, 
                            CreateOntologyGraphMLFileParameters.PathGraphMLFile, 
                            CreateOntologyGraphMLFileParameters.GraphType)
                        {
                            MessageOutput = CreateOntologyGraphMLFileParameters.MessageOutput
                        };

                        var createResult = await syncConnector.ExportOntologyToMLGraph(requestCreateFile);

                        if (createResult.GUID == globals.LState_Error.GUID)
                        {
                            result = createResult;
                            CreateOntologyGraphMLFileParameters.MessageOutput?.OutputInfo(result.Additional1);
                            return result;
                        }

                        CreateOntologyGraphMLFileParameters.MessageOutput?.OutputInfo($"created {CreateOntologyGraphMLFileParameters.PathGraphMLFile}.");
                        break;
                    case GraphMLFunction.ExportTypedTags:

                        var requestExportTypedTags = new GraphMLConnector.Models.ExportTypedTagsToGraphMLRequest(ExportTypedTagsToGraphMLParameters.IdConfig)
                        {
                            MessageOutput = ExportTypedTagsToGraphMLParameters.MessageOutput
                        };

                        var exportTypedTagsResult = await syncConnector.ExportTypedTagsToGraphML(requestExportTypedTags);

                        if (exportTypedTagsResult.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = exportTypedTagsResult.ResultState;
                            ExportTypedTagsToGraphMLParameters.MessageOutput?.OutputInfo(result.Additional1);
                            return result;
                        }

                        ExportTypedTagsToGraphMLParameters.MessageOutput?.OutputInfo($"created {exportTypedTagsResult.Result.FilePath}.");
                        break;
                    case GraphMLFunction.ExportERModel:

                        var requestExportErModel = new GraphMLConnector.Models.ExportERModelRequest(ExportERModelToGraphMLParameters.IdConfig, ExportERModelToGraphMLParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = ExportERModelToGraphMLParameters.MessageOutput
                        };

                        var exportErModelResult = await syncConnector.ExportErModelToGraphML(requestExportErModel);

                        if (exportErModelResult.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = exportErModelResult.ResultState;
                            ExportERModelToGraphMLParameters.MessageOutput?.OutputInfo(result.Additional1);
                            return result;
                        }
                        break;
                }
                
                return result;

            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new GraphMLConnector.GraphMLConnector(globals);

            try
            {

                switch (Function)
                {
                    case GraphMLFunction.CreateOntologyGraphMLFile:
                        var requestCreateFile = new GraphMLConnector.Models.ExportOntologyToMLGraphRequest(CreateOntologyGraphMLFileParameters.IdOntology, 
                            CreateOntologyGraphMLFileParameters.PathGraphMLFile, 
                            CreateOntologyGraphMLFileParameters.GraphType)
                        {
                            MessageOutput = CreateOntologyGraphMLFileParameters.MessageOutput
                        };

                        var createResult = syncConnector.ExportOntologyToMLGraph(requestCreateFile);
                        createResult.Wait();

                        if (createResult.Result.GUID == globals.LState_Error.GUID)
                        {
                            result = createResult.Result;
                            CreateOntologyGraphMLFileParameters.MessageOutput?.OutputInfo(result.Additional1);
                            return result;
                        }

                        CreateOntologyGraphMLFileParameters.MessageOutput?.OutputInfo($"created {CreateOntologyGraphMLFileParameters.PathGraphMLFile}.");
                        break;
                    case GraphMLFunction.ExportTypedTags:

                        var requestExportTypedTags = new GraphMLConnector.Models.ExportTypedTagsToGraphMLRequest(ExportTypedTagsToGraphMLParameters.IdConfig)
                        {
                            MessageOutput = ExportTypedTagsToGraphMLParameters.MessageOutput
                        };

                        var exportTypedTagsResult = syncConnector.ExportTypedTagsToGraphML(requestExportTypedTags);
                        exportTypedTagsResult.Wait();

                        if (exportTypedTagsResult.Result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = exportTypedTagsResult.Result.ResultState;
                            ExportTypedTagsToGraphMLParameters.MessageOutput?.OutputInfo(result.Additional1);
                            return result;
                        }

                        ExportTypedTagsToGraphMLParameters.MessageOutput?.OutputInfo($"created {exportTypedTagsResult.Result.Result.FilePath}.");
                        break;
                    case GraphMLFunction.ExportERModel:

                        var requestExportErModel = new GraphMLConnector.Models.ExportERModelRequest(ExportERModelToGraphMLParameters.IdConfig, ExportERModelToGraphMLParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = ExportERModelToGraphMLParameters.MessageOutput
                        };

                        var exportErModelResult = syncConnector.ExportErModelToGraphML(requestExportErModel);
                        exportErModelResult.Wait();

                        if (exportErModelResult.Result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result = exportErModelResult.Result.ResultState;
                            ExportERModelToGraphMLParameters.MessageOutput?.OutputInfo(result.Additional1);
                            return result;
                        }
                        break;
                }

                return result;

            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public GraphMLController(CreateOntologyGraphMLFileParam syncParam, Globals globals) : base(globals)
        {
            CreateOntologyGraphMLFileParameters = syncParam;
            IsValid = true;
            Function = GraphMLFunction.CreateOntologyGraphMLFile;
            var validationResult = CreateOntologyGraphMLFileParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public GraphMLController(ExportTypedTagsToGraphMLParam syncParam, Globals globals) : base(globals)
        {
            ExportTypedTagsToGraphMLParameters = syncParam;
            IsValid = true;
            Function = GraphMLFunction.ExportTypedTags;
            var validationResult = ExportTypedTagsToGraphMLParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public GraphMLController(ExportERModelToGraphMLParam syncParam, Globals globals) : base(globals)
        {
            ExportERModelToGraphMLParameters = syncParam;
            IsValid = true;
            Function = GraphMLFunction.ExportERModel;
            var validationResult = ExportERModelToGraphMLParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public GraphMLController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            
            
            IsValid = true;
            ErrorMessage = "";
            
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());
            if (key == null)
            {
                IsValid = false;
                ErrorMessage += $"You must provide a valid Function: [{nameof(GraphMLFunction.CreateOntologyGraphMLFile)}]\n";
                return;
            }

            if (commandLine.AdditionalParameters[key].ToLower() == nameof(GraphMLFunction.CreateOntologyGraphMLFile))
            {
                Function = GraphMLFunction.CreateOntologyGraphMLFile;
            }
            else
            {
                IsValid = false;
                ErrorMessage += $"You must provide a valid Function: [{nameof(GraphMLFunction.CreateOntologyGraphMLFile)}]\n";
                return;
            }

            switch (Function)
            {
                case GraphMLFunction.CreateOntologyGraphMLFile:
                    CreateOntologyGraphMLFileParameters = new CreateOntologyGraphMLFileParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };

                    var validateSyncADPartners = CreateOntologyGraphMLFileParameters.ValidateParam(commandLine, globals);
                    IsValid = validateSyncADPartners.GUID == globals.LState_Success.GUID;
                    ErrorMessage = validateSyncADPartners.Additional1;
                    return;
                case GraphMLFunction.ExportTypedTags:
                    ExportTypedTagsToGraphMLParameters = new ExportTypedTagsToGraphMLParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };

                    var validateSyncExportTypedTags = ExportTypedTagsToGraphMLParameters.ValidateParam(commandLine, globals);
                    IsValid = validateSyncExportTypedTags.GUID == globals.LState_Success.GUID;
                    ErrorMessage = validateSyncExportTypedTags.Additional1;
                    return;
                case GraphMLFunction.ExportERModel:
                    ExportERModelToGraphMLParameters = new ExportERModelToGraphMLParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    var validationExportParam = ExportERModelToGraphMLParameters.ValidateParam(commandLine, globals);
                    IsValid = validationExportParam.GUID == globals.LState_Success.GUID;
                    ErrorMessage = validationExportParam.Additional1;
                    return;
            }
        }

        public GraphMLController() { }
    }
}
