﻿using ImportExport_Module;
using MediaViewerModule.Connectors;
using MediaViewerModule.Models;
using MediaViewerModule.Primitives;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    
    public enum MediaViewerFunction
    {
        ExportMediaList = 0,
        RelateFilesWithMediaItemsAndRef = 1,
        TagMediaItems = 2,
        OCRImages = 3
        
    }
    public class MedieViewerController : BaseController, IModuleController
    {
        public MediaViewerFunction Function { get; private set; }

        public ExportMediaListParam ExportMediaListParameters { get; private set; }

        public RelateFilesWithMediaItemsAndRefParam RelateFilesWithMediaItemsAndRefParameters { get; private set; }

        public OCRImagesParam OCRImagesParameters { get; private set; }

        public TagMediaItemsParam TagMediaItemsParameters { get; private set; }


        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(MediaViewerModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"{nameof(Function)}{nameof(MediaViewerFunction.ExportMediaList)} {nameof(ExportMediaListParam.IdMediaList)}:<Id> {nameof(ExportMediaListParam.MediaType)}:<Audio|Video|Image|Pdf> {nameof(ExportMediaListParam.MediaStorePath)}:<Path>");
                sbSyntax.AppendLine($"{nameof(Function)}{nameof(MediaViewerFunction.RelateFilesWithMediaItemsAndRef)} {nameof(RelateFilesWithMediaItemsAndRefParameters.ConfigFile)}:<Path to input-file>");
                sbSyntax.AppendLine($"{nameof(Function)}{nameof(MediaViewerFunction.OCRImages)} {nameof(OCRImagesParam.IdConfig)}:<Id of OCR configuration>");
                return sbSyntax.ToString();
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_MediaViewer_Module;

        public List<Type> Functions => new List<Type> { typeof(ExportMediaListParam), typeof(RelateFilesWithMediaItemsAndRefParam), typeof(TagMediaItemsParam), typeof(OCRImagesParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var mediaViewerConnector = new MediaViewerConnector(globals);
            var mediaStoreConnector = new MediaStore_Module.MediaStoreConnector(globals);

            try
            {
                if (Function == MediaViewerFunction.ExportMediaList)
                {
                    var downloadFileController = new MediaStore_Module.FileDownloadController(globals);
                    var pathResult = mediaStoreConnector.GetMediaPath();

                    if (pathResult.GUID == globals.LState_Error.GUID || pathResult.GUID == globals.LState_Nothing.GUID)
                    {
                        result.Additional1 = "No MediaStore-path can be found!";
                        return result;
                    }

                    var mediaStorePath = mediaStoreConnector.MediaPath;

                    var oItemResult = mediaViewerConnector.GetOItem(ExportMediaListParameters.IdMediaList, globals.Type_Object);

                    result = oItemResult.ResultState;

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error hile getting the MediaList!";
                        return result;
                    }


                    var mediaListTask = await mediaViewerConnector.GetMediaListItems(new List<clsOntologyItem> { oItemResult.Result.OItem }, ExportMediaListParameters.MediaStorePath, "", ExportMediaListParameters.MediaItemType, messageOutput: ExportMediaListParameters.MessageOutput);
                    result = mediaListTask.Result;

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while getting the Media-List";
                        return result;
                    }

                    var fileList = mediaListTask.MediaListItems.Select(mediaItem => new clsOntologyItem
                    {
                        GUID = mediaItem.IdFile,
                        Name = mediaItem.NameFile,
                        GUID_Parent = mediaStoreConnector.ClassFile.GUID,
                        Type = globals.Type_Object,
                        Val_Long = mediaItem.OrderId
                    }).ToList();

                    if (fileList.Any())
                    {

                        var request = new MediaStore_Module.Models.GetDownloadFilesRequest(ExportMediaListParameters.MediaStorePath, fileList, oItemResult.Result.OItem.Name);

                        if (!string.IsNullOrEmpty(ExportMediaListParameters.Sort))
                        {
                            if (fileList.Count < 10)
                            {
                                request.OrderFormat = "0";
                            }
                            else if (fileList.Count < 100)
                            {
                                request.OrderFormat = "00";
                            }
                            else if (fileList.Count < 1000)
                            {
                                request.OrderFormat = "000";
                            }
                            else if (fileList.Count < 10000)
                            {
                                request.OrderFormat = "0000";
                            }
                            else if (fileList.Count < 100000)
                            {
                                request.OrderFormat = "00000";
                            }
                            else if (fileList.Count < 1000000)
                            {
                                request.OrderFormat = "000000";
                            }
                            else
                            {
                                request.OrderFormat = "0000000";
                            }

                            if (ExportMediaListParameters.Sort == "asc")
                            {
                                request.SortOrder = MediaStore_Module.Models.DownloadSort.Asc;

                            }
                            else
                            {
                                request.SortOrder = MediaStore_Module.Models.DownloadSort.Desc;
                            }
                        }

                        request.MessageOutput = ExportMediaListParameters.MessageOutput;

                        var exportResult = await downloadFileController.GetDownloadFiles(request);

                        result = exportResult.ResultState;

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ExportMediaListParameters.MessageOutput?.OutputError(result.Additional1);
                            return result;
                        }

                        var path = Path.Combine(ExportMediaListParameters.ExportPath, exportResult.ResultState.Additional1);
                        using (var fileStream = new FileStream(path, FileMode.CreateNew))
                        {
                            exportResult.Result.CopyTo(fileStream);
                        }
                    }
                    else
                    {
                        result = globals.LState_Nothing.Clone();
                        result.Additional1 = "No media-items found!";
                        return result;
                    }

                }
                else if (Function == MediaViewerFunction.RelateFilesWithMediaItemsAndRef)
                {
                    var request = new RelateFilesWithMediaItemsAndRefRequest
                    {
                        RefItem = RelateFilesWithMediaItemsAndRefParameters.RefItem,
                        FileItems = RelateFilesWithMediaItemsAndRefParameters.FileItems,
                        MediaType = RelateFilesWithMediaItemsAndRefParameters.MediaType
                    };

                    var resultRelate = await mediaViewerConnector.RelateFilesWithMediaItemsAndRef(request);

                    result = resultRelate.ResultState;

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Additional1 = $"Related {resultRelate.Result.MediaItems.Count} FileItems!";
                    return result;
                }
                else if (Function == MediaViewerFunction.TagMediaItems)
                {
                    var request = new TagMediaItemsRequest(TagMediaItemsParameters.MediaItemType, TagMediaItemsParameters.MediaStorePath)
                    {
                        ItemFilter = TagMediaItemsParameters.ItemGuidFilter,
                        Related = TagMediaItemsParameters.RelatedGuidFilter,
                        NameRegex = TagMediaItemsParameters.NameRegex,
                        MessageOutput = TagMediaItemsParameters.MessageOutput
                    };

                    var taggingController = new MediaTaggingController(globals);

                    var resultRelate = await taggingController.TagMediaItems(request);

                    result = resultRelate.ResultState;

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }


                    result.Additional1 = $"Tagged {resultRelate.Result.Tagged.Count} FileItems!";
                    TagMediaItemsParameters.MessageOutput?.OutputInfo(result.Additional1);
                    return result;
                }
                else if (Function == MediaViewerFunction.OCRImages)
                {
                    var request = new OCRImagesRequest(OCRImagesParameters.IdConfig)
                    {
                        MessageOutput = OCRImagesParameters.MessageOutput,
                        CancellationToken = OCRImagesParameters.CancellationToken
                    };

                    var ocrController = new OCRController(globals);

                    var resultRelate = await ocrController.OCRImages(request);

                    result = resultRelate.ResultState;

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }


                    result.Additional1 = $"Recognized {resultRelate.Result.OCRResult.Count} Images!";
                    OCRImagesParameters.MessageOutput?.OutputInfo(result.Additional1);
                    return result;
                }
                else
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The function is not valid!";
                    return result;

                }


                result.Additional1 = $"Successful exported Media-List";
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }


        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var mediaViewerConnector = new MediaViewerConnector(globals);
            var mediaStoreConnector = new MediaStore_Module.MediaStoreConnector(globals);

            

            try
            {

                if (Function == MediaViewerFunction.ExportMediaList)
                {
                    var downloadFileController = new MediaStore_Module.FileDownloadController(globals);
                    var pathResult = mediaStoreConnector.GetMediaPath();

                    if (pathResult.GUID == globals.LState_Error.GUID || pathResult.GUID == globals.LState_Nothing.GUID)
                    {
                        result.Additional1 = "No MediaStore-path can be found!";
                        return result;
                    }

                    var mediaStorePath = mediaStoreConnector.MediaPath;

                    var oItemResult = mediaViewerConnector.GetOItem(ExportMediaListParameters.IdMediaList, globals.Type_Object);

                    result = oItemResult.ResultState;

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error hile getting the MediaList!";
                        return result;
                    }


                    var mediaListTask = mediaViewerConnector.GetMediaListItems(new List<clsOntologyItem> { oItemResult.Result.OItem }, ExportMediaListParameters.MediaStorePath, "", ExportMediaListParameters.MediaItemType, messageOutput: ExportMediaListParameters.MessageOutput);
                    mediaListTask.Wait();
                    result = mediaListTask.Result.Result;

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while getting the Media-List";
                        return result;
                    }

                    var fileList = mediaListTask.Result.MediaListItems.Select(mediaItem => new clsOntologyItem
                    {
                        GUID = mediaItem.IdFile,
                        Name = mediaItem.NameFile,
                        GUID_Parent = mediaStoreConnector.ClassFile.GUID,
                        Type = globals.Type_Object,
                        Val_Long = mediaItem.OrderId
                    }).ToList();

                    if (fileList.Any())
                    {

                        var request = new MediaStore_Module.Models.GetDownloadFilesRequest(ExportMediaListParameters.MediaStorePath, fileList, oItemResult.Result.OItem.Name);

                        if (!string.IsNullOrEmpty(ExportMediaListParameters.Sort))
                        {
                            if (fileList.Count < 10)
                            {
                                request.OrderFormat = "0";
                            }
                            else if (fileList.Count < 100)
                            {
                                request.OrderFormat = "00";
                            }
                            else if (fileList.Count < 1000)
                            {
                                request.OrderFormat = "000";
                            }
                            else if (fileList.Count < 10000)
                            {
                                request.OrderFormat = "0000";
                            }
                            else if (fileList.Count < 100000)
                            {
                                request.OrderFormat = "00000";
                            }
                            else if (fileList.Count < 1000000)
                            {
                                request.OrderFormat = "000000";
                            }
                            else
                            {
                                request.OrderFormat = "0000000";
                            }

                            if (ExportMediaListParameters.Sort == "asc")
                            {
                                request.SortOrder = MediaStore_Module.Models.DownloadSort.Asc;

                            }
                            else
                            {
                                request.SortOrder = MediaStore_Module.Models.DownloadSort.Desc;
                            }
                        }

                        var exportResult = downloadFileController.GetDownloadFiles(request);
                        exportResult.Wait();

                        result = exportResult.Result.ResultState;

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var path = Path.Combine(ExportMediaListParameters.ExportPath, exportResult.Result.ResultState.Additional1);
                        using (var fileStream = new FileStream(path, FileMode.CreateNew))
                        {
                            exportResult.Result.Result.CopyTo(fileStream);
                        }
                    }
                    else
                    {
                        result = globals.LState_Nothing.Clone();
                        result.Additional1 = "No media-items found!";
                        return result;
                    }
                    
                }
                else if (Function == MediaViewerFunction.RelateFilesWithMediaItemsAndRef)
                {
                    var request = new RelateFilesWithMediaItemsAndRefRequest
                    {
                        RefItem = RelateFilesWithMediaItemsAndRefParameters.RefItem,
                        FileItems = RelateFilesWithMediaItemsAndRefParameters.FileItems,
                        MediaType = RelateFilesWithMediaItemsAndRefParameters.MediaType
                    };

                    var resultRelate = mediaViewerConnector.RelateFilesWithMediaItemsAndRef(request);
                    resultRelate.Wait();

                    result = resultRelate.Result.ResultState;

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Additional1 = $"Related {resultRelate.Result.Result.MediaItems.Count} FileItems!";
                    return result;
                }
                else if (Function == MediaViewerFunction.TagMediaItems)
                {
                    var request = new TagMediaItemsRequest(TagMediaItemsParameters.MediaItemType, TagMediaItemsParameters.MediaStorePath)
                    {
                        ItemFilter = TagMediaItemsParameters.ItemGuidFilter,
                        Related = TagMediaItemsParameters.RelatedGuidFilter,
                        NameRegex = TagMediaItemsParameters.NameRegex,
                        MessageOutput = TagMediaItemsParameters.MessageOutput
                    };

                    var taggingController = new MediaTaggingController(globals);

                    var resultRelate = taggingController.TagMediaItems(request);
                    resultRelate.Wait();

                    result = resultRelate.Result.ResultState;

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }


                    result.Additional1 = $"Tagged {resultRelate.Result.Result.Tagged.Count} FileItems!";
                    TagMediaItemsParameters.MessageOutput?.OutputInfo(result.Additional1);
                    return result;
                }
                else if (Function == MediaViewerFunction.OCRImages)
                {
                    var request = new OCRImagesRequest(OCRImagesParameters.IdConfig)
                    {
                        MessageOutput = OCRImagesParameters.MessageOutput,
                        CancellationToken = OCRImagesParameters.CancellationToken
                    };

                    var ocrController = new OCRController(globals);

                    var resultRelate = ocrController.OCRImages(request);
                    resultRelate.Wait();

                    result = resultRelate.Result.ResultState;

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }


                    result.Additional1 = $"Recognized {resultRelate.Result.Result.OCRResult.Count} Images!";
                    TagMediaItemsParameters.MessageOutput?.OutputInfo(result.Additional1);
                    return result;
                }
                else
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The function is not valid!";
                    return result;

                }
                

                result.Additional1 = $"Successful exported Media-List";
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }


        }

        public MedieViewerController(ExportMediaListParam param, Globals globals) : base(globals)
        {
            Function = MediaViewerFunction.ExportMediaList;
            IsValid = true;
            ExportMediaListParameters = param;
            var validationResult = ExportMediaListParameters.ValidateParam(globals);
            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public MedieViewerController(TagMediaItemsParam param, Globals globals) : base(globals)
        {
            Function = MediaViewerFunction.TagMediaItems;
            IsValid = true;
            TagMediaItemsParameters = param;
            var validationResult = TagMediaItemsParameters.ValidateParam(globals);
            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public MedieViewerController(RelateFilesWithMediaItemsAndRefParam param, Globals globals) : base(globals)
        {
            Function = MediaViewerFunction.RelateFilesWithMediaItemsAndRef;
            IsValid = true;
            RelateFilesWithMediaItemsAndRefParameters = param;
            var validationResult = ExportMediaListParameters.ValidateParam(globals);
            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public MedieViewerController(OCRImagesParam param, Globals globals) : base(globals)
        {
            Function = MediaViewerFunction.OCRImages;
            IsValid = true;
            OCRImagesParameters = param;
            var validationResult = OCRImagesParameters.ValidateParam(globals);
            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public MedieViewerController(CommandLine commandLine, Globals globals) : base(globals,commandLine)
        {

            IsValid = true;
            ErrorMessage = "";

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You must provide a function\n";
                return;
            }

            var function = commandLine.AdditionalParameters[key];

            if (function.ToLower() == nameof(MediaViewerFunction.ExportMediaList).ToLower())
            {
                Function = MediaViewerFunction.ExportMediaList;
            }
            else if (function.ToLower() == nameof(MediaViewerFunction.RelateFilesWithMediaItemsAndRef).ToLower())
            {
                Function = MediaViewerFunction.RelateFilesWithMediaItemsAndRef;
            }
            else if (function.ToLower() == nameof(MediaViewerFunction.TagMediaItems).ToLower())
            {
                Function = MediaViewerFunction.TagMediaItems;
            }
            else if (function.ToLower() == nameof(MediaViewerFunction.OCRImages).ToLower())
            {
                Function = MediaViewerFunction.OCRImages;
            }
            else
            {
                IsValid = false;
                ErrorMessage += "No valid Function!";
                return;
            }

            if (Function == MediaViewerFunction.ExportMediaList)
            {
                ExportMediaListParameters = new ExportMediaListParam()
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };
                var validationResult = ExportMediaListParameters.ValidateParam(commandLine, globals);
                IsValid = (validationResult.GUID != globals.LState_Error.GUID);
                ErrorMessage += validationResult.Additional1;
            }
            else if ( Function == MediaViewerFunction.RelateFilesWithMediaItemsAndRef)
            {
                RelateFilesWithMediaItemsAndRefParameters = new RelateFilesWithMediaItemsAndRefParam
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };

                var validationResult = RelateFilesWithMediaItemsAndRefParameters.ValidateParam(commandLine, globals);
                IsValid = (validationResult.GUID != globals.LState_Error.GUID);
                ErrorMessage += validationResult.Additional1;
            }
            else if (Function == MediaViewerFunction.TagMediaItems)
            {
                TagMediaItemsParameters = new TagMediaItemsParam
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };

                var validationResult = TagMediaItemsParameters.ValidateParam(commandLine, globals);
                IsValid = (validationResult.GUID != globals.LState_Error.GUID);
                ErrorMessage += validationResult.Additional1;
            }
            else if (Function == MediaViewerFunction.OCRImages)
            {
                OCRImagesParameters = new OCRImagesParam
                {
                    MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                };

                var validationResult = OCRImagesParameters.ValidateParam(commandLine, globals);
                IsValid = (validationResult.GUID != globals.LState_Error.GUID);
                ErrorMessage += validationResult.Additional1;
            }



        }


        public MedieViewerController() { }
    }
}
