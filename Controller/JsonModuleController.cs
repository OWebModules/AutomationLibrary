﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{

    public enum JsonFunction
    {
        ConvertJsonToSchema,
        ValidateJson
    }
    public class JsonModuleController : BaseController, IModuleController
    {

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(JsonModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sb = new StringBuilder();
                sb.AppendLine($"Module:{nameof(JsonModule)} {nameof(Function)}:{nameof(JsonFunction.ConvertJsonToSchema)} {nameof(ConvertJsonToSchemaParam.IdConfig)}:<Id of Config>");
                sb.AppendLine($"Module:{nameof(JsonModule)} {nameof(Function)}:{nameof(JsonFunction.ValidateJson)} {nameof(ValidateJsonParam.IdConfig)}:<Id of Config>");
                return sb.ToString();
            }
        }

        public JsonFunction Function { get; private set; }
        public ConvertJsonToSchemaParam ConvertJsonToSchemaParameters { get; private set; }
        public ValidateJsonParam ValidateJsonParameters { get; private set; }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_JsonModule;

        public List<Type> Functions => new List<Type> { typeof(ConvertJsonToSchemaParam), typeof(ValidateJsonParam) };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;


            var syncConnector = new JsonModule.JsonController(globals);


            switch (Function)
            {
                case JsonFunction.ConvertJsonToSchema:
                    try
                    {
                        var request = new JsonModule.Models.ConvertJsonToSchemaRequest(ConvertJsonToSchemaParameters.IdConfig)
                        {
                            MessageOutput = ConvertJsonToSchemaParameters.MessageOutput
                        };
                        var syncTask = await syncConnector.ConvertJsonToSchema(request);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ConvertJsonToSchemaParameters.MessageOutput?.OutputError(result.Additional1);
                        }

                        foreach (var configResult in syncTask.Result)
                        {
                            if (configResult.ResultState.GUID == globals.LState_Error.GUID)
                            {
                                ConvertJsonToSchemaParameters.MessageOutput?.OutputError($"{configResult.ConfigItem.Name}: { configResult.ResultState.Additional1}");
                            }
                            else
                            {
                                ConvertJsonToSchemaParameters.MessageOutput?.OutputInfo($"{configResult.ConfigItem.Name}: Written Schema to {configResult.ConfigItem.SchemaPath}");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        ConvertJsonToSchemaParameters.MessageOutput?.OutputError(result.Additional1);
                        return result;

                    }
                    break;
                case JsonFunction.ValidateJson:
                    try
                    {
                        var validateRequest = new JsonModule.Models.JsonValidationRequest(ValidateJsonParameters.IdConfig)
                        {
                            MessageOutput = ValidateJsonParameters.MessageOutput
                        };
                        var validateResult = await syncConnector.ValidateJson(validateRequest);

                        result = validateResult.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ValidateJsonParameters.MessageOutput?.OutputError(result.Additional1);
                        }

                        foreach (var configResult in validateResult.Result)
                        {
                            ValidateJsonParameters.MessageOutput?.OutputInfo($"{configResult.ConfigItem.Name} ({configResult.ConfigItem.JsonPath} / {configResult.ConfigItem.SchemaPath}) validation results ");
                            if (configResult.ValidationMessages.Any())
                            {
                                foreach (var message in configResult.ValidationMessages)
                                {
                                    ValidateJsonParameters.MessageOutput?.OutputWarning(message);
                                }
                            }
                            else
                            {
                                ValidateJsonParameters.MessageOutput?.OutputInfo($"{configResult.ConfigItem.Name}: Validation Ok");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        ValidateJsonParameters.MessageOutput?.OutputError(result.Additional1);
                        return result;

                    }
                    break;
            }


            return result;

        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var syncConnector = new JsonModule.JsonController(globals);

            switch (Function)
            {
                case JsonFunction.ConvertJsonToSchema:
                    try
                    {
                        var request = new JsonModule.Models.ConvertJsonToSchemaRequest(ConvertJsonToSchemaParameters.IdConfig)
                        {
                            MessageOutput = ConvertJsonToSchemaParameters.MessageOutput
                        };
                        var syncTask = syncConnector.ConvertJsonToSchema(request);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ConvertJsonToSchemaParameters.MessageOutput?.OutputError(result.Additional1);
                        }

                        foreach (var configResult in syncTask.Result.Result)
                        {

                            if (configResult.ResultState.GUID == globals.LState_Error.GUID)
                            {
                                ConvertJsonToSchemaParameters.MessageOutput?.OutputError($"{configResult.ConfigItem.Name}: { configResult.ResultState.Additional1}");
                            }
                            else
                            {
                                ConvertJsonToSchemaParameters.MessageOutput?.OutputInfo($"{configResult.ConfigItem.Name}: Written Schema to {configResult.ConfigItem.SchemaPath}");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        ConvertJsonToSchemaParameters.MessageOutput?.OutputError(result.Additional1);
                        return result;

                    }
                    break;
                case JsonFunction.ValidateJson:
                    try
                    {
                        var validateRequest = new JsonModule.Models.JsonValidationRequest(ValidateJsonParameters.IdConfig)
                        {
                            MessageOutput = ConvertJsonToSchemaParameters.MessageOutput
                        };
                        var validateResult = syncConnector.ValidateJson(validateRequest);
                        validateResult.Wait();

                        result = validateResult.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            ValidateJsonParameters.MessageOutput?.OutputError(result.Additional1);
                        }

                        foreach (var configResult in validateResult.Result.Result)
                        {
                            ValidateJsonParameters.MessageOutput?.OutputInfo($"{configResult.ConfigItem.Name} ({configResult.ConfigItem.JsonPath} / {configResult.ConfigItem.SchemaPath}) validation results ");
                            if (configResult.ValidationMessages.Any())
                            {
                                foreach (var message in configResult.ValidationMessages)
                                {
                                    ValidateJsonParameters.MessageOutput?.OutputWarning(message);
                                }
                            }
                            else
                            {
                                ValidateJsonParameters.MessageOutput?.OutputInfo($"{configResult.ConfigItem.Name}: Validation Ok");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = ex.Message;
                        ValidateJsonParameters.MessageOutput?.OutputError(result.Additional1);
                        return result;

                    }
                    break;
            }
            return result;
        }

        public JsonModuleController(ConvertJsonToSchemaParam syncParam, Globals globals) : base(globals)
        {
            ConvertJsonToSchemaParameters = syncParam;
            Function = JsonFunction.ConvertJsonToSchema;
            IsValid = true;
            var validationResult = ConvertJsonToSchemaParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public JsonModuleController(ValidateJsonParam syncParam, Globals globals) : base(globals)
        {
            ValidateJsonParameters = syncParam;
            Function = JsonFunction.ValidateJson;
            IsValid = true;
            var validationResult = ValidateJsonParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public JsonModuleController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            IsValid = true;
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You have to provide an Function\n";
            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == JsonFunction.ConvertJsonToSchema.ToString().ToLower())
                {
                    Function = JsonFunction.ConvertJsonToSchema;
                }
                else if (commandLine.AdditionalParameters[key].ToLower() == JsonFunction.ValidateJson.ToString().ToLower())
                {
                    Function = JsonFunction.ValidateJson;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += "You have to provide a valid Function\n";
                }
            }

            switch (Function)
            {
                case JsonFunction.ConvertJsonToSchema:
                    ConvertJsonToSchemaParameters = new ConvertJsonToSchemaParam();
                    ConvertJsonToSchemaParameters.MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath);
                    var validateResultAddVariables = ConvertJsonToSchemaParameters.ValidateParam(commandLine, globals);
                    IsValid = (validateResultAddVariables.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateResultAddVariables.Additional1;
                    break;
                case JsonFunction.ValidateJson:

                    ValidateJsonParameters = new ValidateJsonParam();
                    ValidateJsonParameters.MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath);
                    var validateResultValidate = ValidateJsonParameters.ValidateParam(commandLine, globals);
                    IsValid = (validateResultValidate.GUID == globals.LState_Success.GUID);
                    ErrorMessage += validateResultValidate.Additional1;
                    break;
            }

        }

        public JsonModuleController() { }
    }
}
