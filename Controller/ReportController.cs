﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Attributes;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{

    public class ReportController : BaseController, IModuleController
    {
        public enum ModuleFunction
        {
            SyncReport,
            CreatePDFReport,
            MoveObjectsByReport,
            SyncReportColumns
        }

        public ModuleFunction Function { get; private set; }

        public ReportSyncParam ReportSyncParameters { get; private set; } = new ReportSyncParam();
        public CreatePDFReportParam CreatePDFReportParameters { get; private set; } = new CreatePDFReportParam();

        public MoveObjectsByReportParam MoveObjectsByReportParameters { get; private set; } = new MoveObjectsByReportParam();

        public SyncReportColumnsParam SyncReportColumnsParameters { get; private set; } = new SyncReportColumnsParam();

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(ReportModule).ToLower();
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"Module:{nameof(ReportModule)} {nameof(Function)}:[{nameof(ModuleFunction.SyncReport)}] {nameof(ReportSyncParam.IdReport)}:<Id of Report>");
                sbSyntax.AppendLine($"Module:{nameof(ReportModule)} {nameof(Function)}:[{nameof(ModuleFunction.CreatePDFReport)}] {nameof(CreatePDFReportParam.IdConfig)}:<Id of Configuration>");
                sbSyntax.AppendLine($"Module:{nameof(ReportModule)} {nameof(Function)}:[{nameof(ModuleFunction.MoveObjectsByReport)}] {nameof(MoveObjectsByReportParam.IdConfig)}:<Id of Configuration>");
                sbSyntax.AppendLine($"Module:{nameof(ReportModule)} {nameof(Function)}:[{nameof(ModuleFunction.SyncReportColumns)}] {nameof(SyncReportColumnsParam.IdReport)}:<Id of Report>");
                return sbSyntax.ToString();
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_Reports_Module;

        public List<Type> Functions => new List<Type> 
        { 
            typeof(ReportSyncParam), 
            typeof(CreatePDFReportParam), 
            typeof(MoveObjectsByReportParam),
            typeof(SyncReportColumnsParam)
        };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;


            var syncConnector = new ReportModule.ReportController(globals, false);

            try
            {
                switch (Function)
                {
                    case ModuleFunction.SyncReport:

                        var getReportResultOItem = await syncConnector.GetOItem(ReportSyncParameters.IdReport, globals.Type_Object);

                        result = getReportResultOItem.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var getReportResult = await syncConnector.GetReport(getReportResultOItem.Result);

                        result = getReportResult.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var exportResult = await syncConnector.SyncReport(new ReportModule.Models.SyncReportRequest { ReportItem = getReportResult.Result.Report, MessageOutput = ReportSyncParameters.MessageOutput });

                        result = exportResult.ResultState;
                        break;
                    case ModuleFunction.CreatePDFReport:

                        var request = new ReportModule.Models.CreatePDFReportRequest(CreatePDFReportParameters.IdConfig)
                        {
                            MessageOutput = CreatePDFReportParameters.MessageOutput
                        };

                        var createPDFReportResult = await syncConnector.CreatePDFReport(request);

                        result = createPDFReportResult;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        break;
                    case ModuleFunction.MoveObjectsByReport:

                        var moveRequest = new ReportModule.Models.MoveObjectsByReportRequest(MoveObjectsByReportParameters.IdConfig, MoveObjectsByReportParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = MoveObjectsByReportParameters.MessageOutput
                        };

                        var moveObjectsResult = await syncConnector.MoveObjectsByReport(moveRequest);

                        result = moveObjectsResult.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        break;
                    case ModuleFunction.SyncReportColumns:

                        var syncReportColumnsResult = await syncConnector.SyncColumns(SyncReportColumnsParameters.IdReport, SyncReportColumnsParameters.MessageOutput);

                        result = syncReportColumnsResult;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }
                        break;
                }

                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            
            var syncConnector = new ReportModule.ReportController(globals, false);

            try
            {
                switch(Function)
                {
                    case ModuleFunction.SyncReport:

                        var getReportResultOItemTask = syncConnector.GetOItem(ReportSyncParameters.IdReport, globals.Type_Object);
                        getReportResultOItemTask.Wait();

                        result = getReportResultOItemTask.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var getReportResultTask = syncConnector.GetReport(getReportResultOItemTask.Result.Result);
                        getReportResultTask.Wait();

                        result = getReportResultTask.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        var exportResult = syncConnector.SyncReport(new ReportModule.Models.SyncReportRequest { ReportItem = getReportResultTask.Result.Result.Report, MessageOutput = ReportSyncParameters.MessageOutput });
                        exportResult.Wait();

                        result = exportResult.Result.ResultState;
                        break;
                    case ModuleFunction.CreatePDFReport:

                        var request = new ReportModule.Models.CreatePDFReportRequest(CreatePDFReportParameters.IdConfig)
                        {
                            MessageOutput = CreatePDFReportParameters.MessageOutput
                        };

                        var createPDFReportResult = syncConnector.CreatePDFReport(request);
                        createPDFReportResult.Wait();

                        result = createPDFReportResult.Result;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        break;
                    case ModuleFunction.MoveObjectsByReport:

                        var moveRequest = new ReportModule.Models.MoveObjectsByReportRequest(MoveObjectsByReportParameters.IdConfig, MoveObjectsByReportParameters.CancellationTokenSource.Token)
                        {
                            MessageOutput = MoveObjectsByReportParameters.MessageOutput
                        };

                        var moveObjectsResult = syncConnector.MoveObjectsByReport(moveRequest);
                        moveObjectsResult.Wait();

                        result = moveObjectsResult.Result.ResultState;
                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        break;
                }
                
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public ReportController(ReportSyncParam syncParam, Globals globals) : base(globals)
        {
            Function = ModuleFunction.SyncReport;
            ReportSyncParameters = syncParam;
            IsValid = true;
            var validationResult = ReportSyncParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public ReportController(CreatePDFReportParam syncParam, Globals globals) : base(globals)
        {
            Function = ModuleFunction.CreatePDFReport;
            CreatePDFReportParameters = syncParam;
            IsValid = true;
            var validationResult = CreatePDFReportParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public ReportController(MoveObjectsByReportParam syncParam, Globals globals) : base(globals)
        {
            Function = ModuleFunction.MoveObjectsByReport;
            MoveObjectsByReportParameters = syncParam;
            IsValid = true;
            var validationResult = MoveObjectsByReportParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public ReportController(SyncReportColumnsParam syncParam, Globals globals) : base(globals)
        {
            Function = ModuleFunction.SyncReportColumns;
            SyncReportColumnsParameters = syncParam;
            IsValid = true;
            var validationResult = SyncReportColumnsParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public ReportController(CommandLine commandLine, Globals globals) : base(globals,commandLine)
        {
            
            IsValid = true;

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You must provide a Function";

            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == nameof(ModuleFunction.SyncReport).ToLower())
                {
                    Function = ModuleFunction.SyncReport;
                }
                else if (CommandLine.AdditionalParameters[key].ToLower() == nameof(ModuleFunction.CreatePDFReport).ToLower())
                {
                    Function = ModuleFunction.CreatePDFReport;
                }
                else if (CommandLine.AdditionalParameters[key].ToLower() == nameof(ModuleFunction.MoveObjectsByReport).ToLower())
                {
                    Function = ModuleFunction.MoveObjectsByReport;
                }
                else
                {
                    IsValid = false;
                    ErrorMessage += "You must provide a valid Function";
                }


            }

            if (IsValid)
            {
                ReportSyncParameters.MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath);
                switch(Function)
                {
                    case ModuleFunction.SyncReport:
                        key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ReportSyncParam.IdReport).ToLower());
                        if (key == null)
                        {
                            IsValid = false;
                            ErrorMessage += "You must provide a IdReport";

                        }
                        else
                        {
                            ReportSyncParameters.IdReport = commandLine.AdditionalParameters[key];
                            if (string.IsNullOrEmpty(ReportSyncParameters.IdReport) || !globals.is_GUID(ReportSyncParameters.IdReport))
                            {
                                IsValid = false;
                                ErrorMessage += "The provided IdProcess is not valid!";
                            }

                        }

                        break;
                    case ModuleFunction.CreatePDFReport:
                        key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(CreatePDFReportParam.IdConfig).ToLower());
                        if (key == null)
                        {
                            IsValid = false;
                            ErrorMessage += "You must provide a IdConfig";

                        }
                        else
                        {
                            CreatePDFReportParameters.IdConfig = commandLine.AdditionalParameters[key];
                            if (string.IsNullOrEmpty(CreatePDFReportParameters.IdConfig) || !globals.is_GUID(CreatePDFReportParameters.IdConfig))
                            {
                                IsValid = false;
                                ErrorMessage += "The provided IdConfig is not valid!";
                            }

                        }

                        break;

                    case ModuleFunction.MoveObjectsByReport:
                        key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(MoveObjectsByReportParam.IdConfig).ToLower());
                        if (key == null)
                        {
                            IsValid = false;
                            ErrorMessage += "You must provide a IdConfig";

                        }
                        else
                        {
                            MoveObjectsByReportParameters.IdConfig = commandLine.AdditionalParameters[key];
                            if (string.IsNullOrEmpty(MoveObjectsByReportParameters.IdConfig) || !globals.is_GUID(MoveObjectsByReportParameters.IdConfig))
                            {
                                IsValid = false;
                                ErrorMessage += "The provided IdConfig is not valid!";
                            }

                        }

                        break;
                    case ModuleFunction.SyncReportColumns:
                        key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(SyncReportColumnsParam.IdReport).ToLower());
                        if (key == null)
                        {
                            IsValid = false;
                            ErrorMessage += "You must provide an IdReport";
                        }
                        else
                        {
                            SyncReportColumnsParameters.IdReport = commandLine.AdditionalParameters[key];
                            if (string.IsNullOrEmpty(SyncReportColumnsParameters.IdReport) || !globals.is_GUID(SyncReportColumnsParameters.IdReport))
                            {
                                IsValid = false;
                                ErrorMessage += "The provided IdReport is not valid!";
                            }

                        }

                        break;
                    default:
                        IsValid = false;
                        ErrorMessage += "You have to provide a valid function!";
                        break;
                }
            }

            
        }

        public ReportController() { }

    }
}
