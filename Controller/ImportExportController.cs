﻿using ImportExport_Module;
using ImportExport_Module.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Logging;
using AutomationLibrary.Interfaces;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Controller
{
    public enum ImportExportFunction
    {
        CSVImport = 0,
        ImportAssemblyOntology = 1,
        ExportWholeGraph = 2,
        MigrateOntologiesToCosmosDB = 3,
        ExportEsIndex = 4
    }
    public class ImportExportController : BaseController, IModuleController
    {

        public ImportExportFunction Function { get; private set; }

        public CSVImportParam CSVImportParameters { get; private set; }
        public ImportAssemblyOntologyParam ImportAssemblyOntologyParameters { get; private set; }

        public ExportWholeGraphParam ExportWholeGraphParameters { get; private set; }
        public MigrateOntologiesToCosmosDBParam MigrateOntologiesToCosmosDBParameters { get; private set; }

        public ExportEsIndexParam ExportEsIndexParameter { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(ImportExport_Module).ToLower();   
        }

        public override string Syntax
        {
            get
            {
                var sbSyntax = new StringBuilder();
                sbSyntax.AppendLine($"{nameof(ImportExport_Module)} Function:{ImportExportFunction.CSVImport} CSVPath:<Path>");
                sbSyntax.AppendLine($"{nameof(ImportExport_Module)} Function:{ImportExportFunction.ImportAssemblyOntology} {nameof(ImportAssemblyOntologyParam.OntologyAssemblyPath)}:<Path to Ontology-Assembly>");
                sbSyntax.AppendLine($"{nameof(ImportExport_Module)} Function:{ImportExportFunction.ExportWholeGraph} {nameof(ExportWholeGraphParam.ExportPath)}:<Path to Export>");
                sbSyntax.AppendLine($"{nameof(ImportExport_Module)} Function:{ImportExportFunction.MigrateOntologiesToCosmosDB} {nameof(MigrateOntologiesToCosmosDBParam.IdConfig)}:<Id of configuration>");
                sbSyntax.AppendLine($"{nameof(ImportExport_Module)} Function:{ImportExportFunction.ExportEsIndex} {nameof(ExportEsIndexParam.Server)}:<Server of Index> {nameof(ExportEsIndexParam.Port)}:<Port> {nameof(ExportEsIndexParam.Index)}:<Index to export> {nameof(ExportEsIndexParam.Type)}:<Type of Index> {nameof(ExportEsIndexParam.ExportPath)}:<Path to export> {nameof(ExportEsIndexParam.ExportFilePrefix)}:<Prefix of exported files>");
                return sbSyntax.ToString();
            }
        }

        public clsOntologyItem Module => Modules.Config.LocalData.Object_ImportExport_Module;

        public List<Type> Functions => new List<Type> 
        { 
            typeof(CSVImportParam), 
            typeof(ImportAssemblyOntologyParam), 
            typeof(ExportWholeGraphParam), 
            typeof(MigrateOntologiesToCosmosDBParam),
            typeof(ExportEsIndexParam)
        };

        public override async Task<clsOntologyItem> DoWorkAsync()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;


            var syncConnector = new CSVImporter(globals);

            try
            {
                switch (Function)
                {
                    case ImportExportFunction.CSVImport:
                        var request = new RequestImportCSV(CSVImportParameters.CSVPath);
                        var syncTask = await syncConnector.ImportCSV(request);

                        result = syncTask.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            var sbResult = new StringBuilder();

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.ImportedAttributeTypes.Count(att => !att.Success)} AttributeTypes");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.ImportedAttributeTypes.Count(att => att.Success)} AttributeTypes");

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.ImportedRelationTypes.Count(att => !att.Success)} RelationTypes");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.ImportedRelationTypes.Count(att => att.Success)} RelationTypes");

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.ImportedClasses.Count(att => !att.Success)} Classes");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.ImportedClasses.Count(att => att.Success)} Classes");

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.ImportedObjects.Count(att => !att.Success)} Objects");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.ImportedObjects.Count(att => att.Success)} Objects");

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.ImportedClassAtts.Count(att => !att.Success)} ClassAttributes");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.ImportedClassAtts.Count(att => att.Success)} ClassAttributes");

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.ImportedClassRels.Count(att => !att.Success)} ClassRelations");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.ImportedClassRels.Count(att => att.Success)} ClassRelations");

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.ImportedObjAtts.Count(att => !att.Success)} ObjectAttributes");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.ImportedObjAtts.Count(att => att.Success)} ObjectAttributes");

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.ImportedObjRels.Count(att => !att.Success)} ObjectRelations");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.ImportedObjRels.Count(att => att.Success)} ObjectRelations");

                            result.Additional1 = sbResult.ToString();


                        }
                        break;
                    case ImportExportFunction.ImportAssemblyOntology:
                        var assemblyImporter = new AssemblyImporter(globals);

                        ImportAssemblyOntologyParameters.MessageOutput?.OutputInfo($"Import Assembly: {ImportAssemblyOntologyParameters.OntologyAssemblyPath} ...");

                        var checkImportResult = await assemblyImporter.ImportAssembly(ImportAssemblyOntologyParameters.OntologyAssemblyPath);

                        result = checkImportResult.ResultState;

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }
                        ImportAssemblyOntologyParameters.MessageOutput?.OutputInfo($"Imported Assembly.");

                        break;
                    case ImportExportFunction.ExportWholeGraph:
                        var jsonExporter = new JsonExporter(globals);

                        ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Export whole graph: {ExportWholeGraphParameters.ExportPath}");

                        var exportRequest = new ExportWholeGraphRequest(ExportWholeGraphParameters.ExportPath)
                        {
                            MessageOutput = ExportWholeGraphParameters.MessageOutput
                        };
                        var resultExport = await jsonExporter.ExportWholeGraph(exportRequest);

                        if (resultExport.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            ExportWholeGraphParameters.MessageOutput?.OutputError($"Export-Error: {resultExport.ResultState.Additional1}");
                        }
                        else
                        {
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported AttributeTypes: {resultExport.CountAttributeTypes}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported RelationTypes: {resultExport.CountRelationTypes}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported Classes: {resultExport.CountClasses}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported Objects: {resultExport.CountObjects}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported ClassAttributes: {resultExport.CountClassAttributes}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported ClassRelations: {resultExport.CountClassRelations}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported ObjectAttributes: {resultExport.CountObjectAttributes}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported ObjectRelations: {resultExport.CountObjectRelations}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported Overall: {resultExport.CountAll}");
                        }
                        break;
                    case ImportExportFunction.MigrateOntologiesToCosmosDB:
                        var cosmosDBConnector = new CosmosDBConnector(globals);

                        var migrationRequest = new ImportExport_Module.Models.MigrateWholeGraphToCosmosDbRequest(MigrateOntologiesToCosmosDBParameters.IdConfig)
                        {
                            MessageOutput = MigrateOntologiesToCosmosDBParameters.MessageOutput
                        };

                        var migrationResult = await cosmosDBConnector.MigrateWholeGraphToCosmosDb(migrationRequest);

                        result = migrationResult.ResultState;

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        break;
                    case ImportExportFunction.ExportEsIndex:
                        var jsonExportController = new JsonExporter(globals);

                        var exportEsIndexRequest = new ImportExport_Module.Models.ExportEsIndexRequest(ExportEsIndexParameter.Server,
                            ExportEsIndexParameter.Port,
                            ExportEsIndexParameter.Index,
                            ExportEsIndexParameter.Type,
                            ExportEsIndexParameter.ExportPath,
                            ExportEsIndexParameter.CancellationTokenSource.Token)
                        {
                            MessageOutput = MigrateOntologiesToCosmosDBParameters.MessageOutput,
                            ExportFilePrefix = ExportEsIndexParameter.ExportFilePrefix
                        };

                        result = await jsonExportController.ExportESIndex(exportEsIndexRequest);

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        break;
                    default:
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "No valid Function provided!";
                        break;
                }

                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;


            var syncConnector = new CSVImporter(globals);

            try
            {
                switch (Function)
                {
                    case ImportExportFunction.CSVImport:
                        var request = new RequestImportCSV(CSVImportParameters.CSVPath);
                        var syncTask = syncConnector.ImportCSV(request);
                        syncTask.Wait();

                        result = syncTask.Result.ResultState;
                        if (result.GUID == globals.LState_Success.GUID)
                        {
                            var sbResult = new StringBuilder();

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.Result.ImportedAttributeTypes.Count(att => !att.Success)} AttributeTypes");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.Result.ImportedAttributeTypes.Count(att => att.Success)} AttributeTypes");

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.Result.ImportedRelationTypes.Count(att => !att.Success)} RelationTypes");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.Result.ImportedRelationTypes.Count(att => att.Success)} RelationTypes");

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.Result.ImportedClasses.Count(att => !att.Success)} Classes");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.Result.ImportedClasses.Count(att => att.Success)} Classes");

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.Result.ImportedObjects.Count(att => !att.Success)} Objects");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.Result.ImportedObjects.Count(att => att.Success)} Objects");

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.Result.ImportedClassAtts.Count(att => !att.Success)} ClassAttributes");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.Result.ImportedClassAtts.Count(att => att.Success)} ClassAttributes");

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.Result.ImportedClassRels.Count(att => !att.Success)} ClassRelations");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.Result.ImportedClassRels.Count(att => att.Success)} ClassRelations");

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.Result.ImportedObjAtts.Count(att => !att.Success)} ObjectAttributes");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.Result.ImportedObjAtts.Count(att => att.Success)} ObjectAttributes");

                            sbResult.AppendLine($"Failure sync of {syncTask.Result.Result.ImportedObjRels.Count(att => !att.Success)} ObjectRelations");
                            sbResult.AppendLine($"Successful synced {syncTask.Result.Result.ImportedObjRels.Count(att => att.Success)} ObjectRelations");

                            result.Additional1 = sbResult.ToString();


                        }
                        break;
                    case ImportExportFunction.ImportAssemblyOntology:
                        var assemblyImporter = new AssemblyImporter(globals);

                        ImportAssemblyOntologyParameters.MessageOutput?.OutputInfo($"Import Assembly: {ImportAssemblyOntologyParameters.OntologyAssemblyPath} ...");

                        var checkImportResult = assemblyImporter.ImportAssembly(ImportAssemblyOntologyParameters.OntologyAssemblyPath);
                        checkImportResult.Wait();

                        result = checkImportResult.Result.ResultState;

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }
                        ImportAssemblyOntologyParameters.MessageOutput?.OutputInfo($"Imported Assembly.");

                        break;
                    case ImportExportFunction.ExportWholeGraph:
                        var jsonExporter = new JsonExporter(globals);

                        ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Export whole graph: {ExportWholeGraphParameters.ExportPath}");

                        var exportRequest = new ExportWholeGraphRequest(ExportWholeGraphParameters.ExportPath)
                        {
                            MessageOutput = ExportWholeGraphParameters.MessageOutput
                        };
                        var resultExport = jsonExporter.ExportWholeGraph(exportRequest);
                        resultExport.Wait();

                        if (resultExport.Result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            ExportWholeGraphParameters.MessageOutput?.OutputError($"Export-Error: {resultExport.Result.ResultState.Additional1}");
                        }
                        else
                        {
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported AttributeTypes: {resultExport.Result.CountAttributeTypes}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported RelationTypes: {resultExport.Result.CountRelationTypes}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported Classes: {resultExport.Result.CountClasses}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported Objects: {resultExport.Result.CountObjects}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported ClassAttributes: {resultExport.Result.CountClassAttributes}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported ClassRelations: {resultExport.Result.CountClassRelations}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported ObjectAttributes: {resultExport.Result.CountObjectAttributes}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported ObjectRelations: {resultExport.Result.CountObjectRelations}");
                            ExportWholeGraphParameters.MessageOutput?.OutputInfo($"Exported Overall: {resultExport.Result.CountAll}");
                        }
                        break;
                    case ImportExportFunction.MigrateOntologiesToCosmosDB:
                        var cosmosDBConnector = new CosmosDBConnector(globals);

                        var migrationRequest = new ImportExport_Module.Models.MigrateWholeGraphToCosmosDbRequest(MigrateOntologiesToCosmosDBParameters.IdConfig)
                        {
                            MessageOutput = MigrateOntologiesToCosmosDBParameters.MessageOutput
                        };

                        var migrationResult = cosmosDBConnector.MigrateWholeGraphToCosmosDb(migrationRequest);
                        migrationResult.Wait();

                        result = migrationResult.Result.ResultState;

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        break;
                    case ImportExportFunction.ExportEsIndex:
                        var jsonExportController = new JsonExporter(globals);

                        var exportEsIndexRequest = new ImportExport_Module.Models.ExportEsIndexRequest(ExportEsIndexParameter.Server,
                            ExportEsIndexParameter.Port,
                            ExportEsIndexParameter.Index,
                            ExportEsIndexParameter.Type,
                            ExportEsIndexParameter.ExportPath,
                            ExportEsIndexParameter.CancellationTokenSource.Token)
                        {
                            MessageOutput = ExportEsIndexParameter.MessageOutput,
                            ExportFilePrefix = ExportEsIndexParameter.ExportFilePrefix
                        };

                        var exportTask = jsonExportController.ExportESIndex(exportEsIndexRequest);
                        exportTask.Wait();
                        result = exportTask.Result;

                        if (result.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        break;
                    default:
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "No valid Function provided!";
                        break;
                }

                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public ImportExportController(CSVImportParam syncParam, Globals globals) : base(globals)
        {
            CSVImportParameters = syncParam;
            IsValid = true;
            Function = ImportExportFunction.CSVImport;
            var validationResult = CSVImportParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public ImportExportController(ImportAssemblyOntologyParam syncParam, Globals globals) : base(globals)
        {
            ImportAssemblyOntologyParameters = syncParam;
            IsValid = true;
            Function = ImportExportFunction.ImportAssemblyOntology;
            var validationResult = ImportAssemblyOntologyParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public ImportExportController(ExportWholeGraphParam syncParam, Globals globals) : base(globals)
        {
            ExportWholeGraphParameters = syncParam;
            IsValid = true;
            Function = ImportExportFunction.ExportWholeGraph;
            var validationResult = ExportWholeGraphParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public ImportExportController(ExportEsIndexParam syncParam, Globals globals) : base(globals)
        {
            ExportEsIndexParameter = syncParam;
            IsValid = true;
            Function = ImportExportFunction.ExportEsIndex;
            var validationResult = ExportEsIndexParameter.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }
        }

        public ImportExportController(MigrateOntologiesToCosmosDBParam syncParam, Globals globals) : base(globals)
        {
            MigrateOntologiesToCosmosDBParameters = syncParam;
            IsValid = true;
            Function = ImportExportFunction.MigrateOntologiesToCosmosDB;
            var validationResult = MigrateOntologiesToCosmosDBParameters.ValidateParam(globals);

            if (validationResult.GUID == globals.LState_Error.GUID)
            {
                IsValid = false;
                ErrorMessage = validationResult.Additional1;
            }

        }

        public ImportExportController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {
            IsValid = true;
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Function).ToLower());
            if (key == null)
            {
                IsValid = false;
                ErrorMessage += $"You have to provide a Function: {ImportExportFunction.CSVImport}|{ImportExportFunction.ImportAssemblyOntology}!\n";

            }
            else
            {
                if (commandLine.AdditionalParameters[key].ToLower() == nameof(ImportExportFunction.CSVImport).ToLower())
                {
                    Function = ImportExportFunction.CSVImport;
                    CSVImportParameters = new CSVImportParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    var validateCSVImport = CSVImportParameters.ValidateParam(commandLine, globals);
                    IsValid = validateCSVImport.GUID == globals.LState_Success.GUID;
                    ErrorMessage = validateCSVImport.Additional1;

                }
                else if (commandLine.AdditionalParameters[key].ToLower() == nameof(ImportExportFunction.ImportAssemblyOntology).ToLower())
                {
                    Function = ImportExportFunction.ImportAssemblyOntology;
                    ImportAssemblyOntologyParameters = new ImportAssemblyOntologyParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    var importAssemblyOntology = ImportAssemblyOntologyParameters.ValidateParam(commandLine, globals);
                    IsValid = importAssemblyOntology.GUID == globals.LState_Success.GUID;
                    ErrorMessage = importAssemblyOntology.Additional1;

                }
                else if (commandLine.AdditionalParameters[key].ToLower() == nameof(ImportExportFunction.ExportWholeGraph).ToLower())
                {
                    Function = ImportExportFunction.ExportWholeGraph;
                    ExportWholeGraphParameters = new ExportWholeGraphParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    var exportWholeGraphParametersResult = ExportWholeGraphParameters.ValidateParam(commandLine, globals);
                    IsValid = exportWholeGraphParametersResult.GUID == globals.LState_Success.GUID;
                    ErrorMessage = exportWholeGraphParametersResult.Additional1;

                }
                else if (commandLine.AdditionalParameters[key].ToLower() == nameof(ImportExportFunction.MigrateOntologiesToCosmosDB).ToLower())
                {
                    Function = ImportExportFunction.MigrateOntologiesToCosmosDB;
                    MigrateOntologiesToCosmosDBParameters = new MigrateOntologiesToCosmosDBParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    var migrateParamResult = ExportWholeGraphParameters.ValidateParam(commandLine, globals);
                    IsValid = migrateParamResult.GUID == globals.LState_Success.GUID;
                    ErrorMessage = migrateParamResult.Additional1;

                }
                else if (commandLine.AdditionalParameters[key].ToLower() == nameof(ImportExportFunction.ExportEsIndex).ToLower())
                {
                    Function = ImportExportFunction.ExportEsIndex;
                    ExportEsIndexParameter = new ExportEsIndexParam
                    {
                        MessageOutput = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath)
                    };
                    var exportEsIndexValidationResult = ExportEsIndexParameter.ValidateParam(commandLine, globals);
                    IsValid = exportEsIndexValidationResult.GUID == globals.LState_Success.GUID;
                    ErrorMessage = exportEsIndexValidationResult.Additional1;
                }
            }


        }

        public ImportExportController() { }
    }
}
