﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Connectors;
using TypedTaggingModule.Models;

namespace AutomationLibrary.Controller
{
    public class TypedTaggingController : BaseController
    {

        public string IdClassTaggingSource { get; private set; }
        public string IdRelationType { get; private set; }

        public override bool IsResponsible(string module)
        {
            return module.ToLower() == nameof(TypedTaggingConnector).ToLower();
        }

        public override string Syntax
        {
            get
            {
                return $"Module:{nameof(TypedTaggingConnector)} IdClassTaggingSource:<Id of Tagging Source> IdRelationType:<Id of RelationType to Reference>";
            }
        }

        public override clsOntologyItem DoWork()
        {
            var result = base.DoWork();
            if (result.GUID == globals.LState_Error.GUID) return result;

            var request = new RequestRelatedTypedTags(IdClassTaggingSource, IdRelationType);

            var syncConnector = new TypedTaggingConnector(globals);

            try
            {
                //var syncTask = syncConnector.RelateTypedTags(request);
                //syncTask.Wait();

                //result = syncTask.Result.ResultState;
                //if (result.GUID == globals.LState_Success.GUID)
                //{
                //    result.Additional1 = $"Related Typed Tags: {syncTask.Result.Result.CountRelated}";


                //}
                return result;
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;

            }
        }

        public TypedTaggingController(CommandLine commandLine, Globals globals) : base(globals, commandLine)
        {

            IsValid = true;

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdClassTaggingSource).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You must provide a id for the class of tagging-source";

            }
            else
            {
                IdClassTaggingSource = commandLine.AdditionalParameters[key];

            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdRelationType).ToLower());

            if (key == null)
            {
                IsValid = false;
                ErrorMessage += "You must provide a id of relationtype to references";

            }
            else
            {
                IdRelationType = commandLine.AdditionalParameters[key];

            }
        }

        public TypedTaggingController() { }
    }
}
