﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Interfaces
{
    public interface IController
    {
        CommandLine CommandLine { get; }

        Globals Globals { get; }

        bool IsValid { get; }

        string ErrorMessage { get; }

        string Syntax { get; }

        clsOntologyItem DoWork();
    }
}
