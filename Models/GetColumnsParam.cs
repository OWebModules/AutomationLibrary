﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    
    public class GetColumnsParam : ModuleParamBase
    {
        public string IdDbItem { get; set; }
       
        [PasswordPropertyText(true)]
        public string MasterPassword { get; set; }

        [Browsable(false)]
        public CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdDbItem))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdDbItem))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid (no GUID)!";
                return result;
            }


            if (string.IsNullOrEmpty(MasterPassword))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide the password for the master-user";
                return result;
            }
            
            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdDbItem).ToLower());

            if (key != null)
            {
                IdDbItem = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(MasterPassword).ToLower());

            if (key != null)
            {
                MasterPassword = commandLine.AdditionalParameters[key];
                
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Get_Columns;
        }

        public GetColumnsParam() : base(typeof(Controller.DatabaseManagementController))
        {

        }
    }
}
