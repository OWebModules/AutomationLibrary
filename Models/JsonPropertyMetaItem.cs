﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class JsonPropertyMetaItem
    {
        public string PropertyName { get; set; }
        public bool IsEncrypted { get; set; }
        public string DecryptedValue { get; set; }
    }
}
