﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;

namespace AutomationLibrary.Models
{
    public class ReIndexTextParserFieldsParam : ModuleParamBase
    {
        [ParamProp(IdClass = "f18f0dc8e57b41f0afbf8a0edd4c0fa8", NameClass = "Textparser")]
        public string IdTextParser { get; set; }

        private string nonSplittedIds;
        public List<FieldId> IdsFields { get; set; } = new List<FieldId>();

        [ParamProp(IdClass = "c9d2c248e9ea4bf085ade51134818133", NameClass = "Pattern", ResultProperty = ResultProp.Name)]
        public string Query { get; set; }

        [Browsable(false)]
        public CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdTextParser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdCodeItem is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdTextParser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdCodeItem is invalid (no GUID)!";
                return result;
            }

            if (!IdsFields.Any())
            {
                if (string.IsNullOrEmpty(nonSplittedIds))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The id-list is not provided! Provide comma-seperated id-list.";
                    return result;
                }

                IdsFields = nonSplittedIds.Split(',').Select(id => id.Trim()).Select(split => new FieldId { Id = split }).ToList();
            }
            
            if (IdsFields.Any(id => string.IsNullOrEmpty(id.Id)))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The id-list is not valid! Provide comma-seperated id-list.";
                return result;
            }

            if (IdsFields.Any(id => !globals.is_GUID(id.Id)))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The id-list is not valid! Provide comma-seperated id-list.";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdTextParser).ToLower());

            if (key != null)
            {
                IdTextParser = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdsFields).ToLower());

            if (key != null)
            {
                nonSplittedIds = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Query).ToLower());

            if (key != null)
            {
                Query = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Re_Index_Textparser;
        }

        public ReIndexTextParserFieldsParam() : base(typeof(Controller.TextParserController))
        {

        }
    }

    public class FieldId
    {
        public string Id { get; set; }
    }
}
