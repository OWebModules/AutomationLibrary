﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public interface IModuleParam
    {
        string ConfigurationId { get; set; }
        string ConfigurationName { get; set; }

        IMessageOutput MessageOutput { get; set; }
        clsOntologyItem ValidateParam(Globals globals);

        clsOntologyItem GetFunction();

    }
}
