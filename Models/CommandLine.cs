﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    [CommandLine(Usage = "Sync External System with Ontology Modules")]
    public class CommandLine : CommandLineBase
    {
        
        [CommandLineArg(RegexRecognize = "(?i)(Module:.*)",
                        Usage = "[Module:[Name]]",
                        Error = "Provide the name of a Sync-Module",
                        KeyValSeperator = ":",
                        Obligatory = true,
                        OrderId = 1)]

        public string Module { get; private set; }

        [CommandLineArg(RegexRecognize = "(?i)(IdConfig:.*)",
                        Usage = "[IdConfig:[Id]]",
                        Error = "Provide a Id for the Config of the Sync-Module",
                        KeyValSeperator = ":",
                        Obligatory = false,
                        OrderId = 2)]
        public string IdConfig { get; private set; }

        [CommandLineArg(RestArguments = true)]
        public Dictionary<string, string> AdditionalParameters { get; set; } = new Dictionary<string, string>();

        public clsOntologyItem GetAdditionalParamter(string key)
        {
            var result = Globals.LState_Success.Clone();
            if (AdditionalParameters.ContainsKey(key))
            {
                result.Additional1 = AdditionalParameters[key];
            }
            else
            {
                result = Globals.LState_Nothing.Clone();
            }

            return result;
        }

        
        public CommandLine(string[] args, Globals globals) : base(args, globals)
        {
            

        }

        
    }
}
