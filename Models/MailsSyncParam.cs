﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace AutomationLibrary.Models
{

    public class MailsSyncParam: ModuleParamBase
    {
        [ParamProp(IdClass = "28db5992c6634d7ab0da05692d601d49", NameClass = "MailModule")]
        public string IdConfig { get; set; }

        [PasswordPropertyText(true)]
        public string MasterPassword { get; set; }
        public bool DeleteMails { get; set; }

        public DateTime? StartScan { get; set; }
        public DateTime? EndScan { get; set; }

        [Browsable(false)]
        public CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid (no GUID)!";
                return result;
            }

            if (string.IsNullOrEmpty(MasterPassword))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You must provide a Master-Password";
                return result;
            }

            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Sync_Mails;
        }

        public MailsSyncParam() : base(typeof(Controller.MailModuleController))
        {

        }
    }
}
