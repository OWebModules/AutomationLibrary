﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class ExportHtmlProcessParam : ModuleParamBase
    {
        [ParamProp(IdClass = "2c1f5b9721e544ca95d243008011c14d", NameClass = "Process")]
        public string IdProcess { get; set; }
        public string FullPath { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdProcess))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdCodeItem is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdProcess))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdCodeItem is invalid (no GUID)!";
                return result;
            }

            if (string.IsNullOrEmpty(FullPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = @"Provide a FullPath, e.g. \Root\Leaf or \";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdProcess).ToLower());

            if (key != null)
            {
                IdProcess = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(FullPath).ToLower());

            if (key != null)
            {
                FullPath = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Export_Html_Process;
        }

        public ExportHtmlProcessParam() : base(typeof(Controller.ProcessController))
        {
        }
    }
}
