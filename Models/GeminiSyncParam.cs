﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using AutomationLibrary.Attributes;
using AutomationLibrary.Controller;
using AutomationLibrary.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{

    public class GeminiSyncParam : ModuleParamBase
    {
        public GeminiSyncTFSCommentsParam GeminiSyncTFSCommentsParam { get; set; }
        public GeminiSyncParamProjects GeminiSyncParamProjects { get; set; }

        public GeminiFunction Function { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var idConfig = "";
            var idMasterUser = "";
            var masterPassword = "";

            if (GeminiSyncParamProjects == null && GeminiSyncTFSCommentsParam == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No Param assigned!";
                return result;
            }

            switch (Function)
            {
                case GeminiFunction.SyncIssues:
                    idConfig = GeminiSyncParamProjects.IdConfig;
                    idMasterUser = GeminiSyncParamProjects.IdMasterUser;
                    masterPassword = GeminiSyncParamProjects.MasterPassword;
                    break;
                case GeminiFunction.SyncTFSComments:
                    idConfig = GeminiSyncTFSCommentsParam.IdConfig;
                    idMasterUser = GeminiSyncTFSCommentsParam.IdMasterUser;
                    masterPassword = GeminiSyncTFSCommentsParam.MasterPassword;
                    break;
                default:
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The Function is not valid!";
                    return result;
            }


            if (string.IsNullOrEmpty(idConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid!";
                return result;
            }

            if (!globals.is_GUID(idConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid (no GUID)!";
                return result;
            }

            if (string.IsNullOrEmpty(idMasterUser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"You have to provide an valid Id for the master-user";
                return result;
            }

            if (!globals.is_GUID(idMasterUser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"You have to provide an valid Id for the master-user";
                return result;
            }

            if (string.IsNullOrEmpty(masterPassword))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"You have to provide a master-password";
                return result;
            }

            if (Function == GeminiFunction.SyncTFSComments)
            {
                if (string.IsNullOrEmpty(GeminiSyncTFSCommentsParam.IdTFSConfig))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have to provide a Id for the config of TFS-Sync";
                    return result;
                }

                if (!globals.is_GUID(GeminiSyncTFSCommentsParam.IdTFSConfig))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"IdTFSConfig is invalid (no GUID)";
                    return result;
                }

                if (string.IsNullOrEmpty(GeminiSyncTFSCommentsParam.IdUser))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have to provide a Id for a user to tag issues with changesets";
                    return result;
                }

                if (!globals.is_GUID(GeminiSyncTFSCommentsParam.IdUser))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have to provide a valid Id for a user to tag issues with changesets";
                    return result;
                }

                if (string.IsNullOrEmpty(GeminiSyncTFSCommentsParam.IdGroup))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have to provide a Id for a group to tag issues with changesets";
                    return result;
                }

                if (!globals.is_GUID(GeminiSyncTFSCommentsParam.IdGroup))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have to provide a valid Id for a group to tag issues with changesets";
                    return result;
                }

            }

            return result;
        }

        public GeminiSyncParam() : base(typeof(Controller.GeminiSyncController))
        {

        }

    }
}

public class GeminiSyncParamProjects : ModuleParamBase
{
    [ParamProp(IdClass = "ef601f569e394ce894f40012b530d965", NameClass = "GeminiConnectorModule")]
    public string IdConfig { get; set; }

    [ParamProp(IdClass = "c441518dbfe04d55b538df5c5555dd83", NameClass = "user")]
    public string IdMasterUser { get; set; }

    [PasswordPropertyText(true)]
    public string MasterPassword { get; set; }


    public GeminiSyncParamProjects() : base(typeof(AutomationLibrary.Controller.GeminiSyncController))
    {
    }

    public override clsOntologyItem GetFunction()
    {
        return AutomationLibrary.Functions.Config.LocalData.Object_OItem_Sync_Gemini_Projects;
    }
}

public class GeminiSyncTFSCommentsParam : GeminiSyncParamProjects
{


    public string IdTFSConfig { get; set; }

    [ParamProp(IdClass = "1b1f843c19b74ae1a73f6191585ec5c6", NameClass = "Group")]
    public string IdGroup { get; set; }

    [ParamProp(IdClass = "c441518dbfe04d55b538df5c5555dd83", NameClass = "user")]
    public string IdUser { get; set; }


    public GeminiSyncTFSCommentsParam()
    {
    }

    public override clsOntologyItem GetFunction()
    {
        return AutomationLibrary.Functions.Config.LocalData.Object_OItem_Sync_Gemini_TFS_Comments;
    }
}

