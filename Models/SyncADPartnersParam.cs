﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class SyncADPartnersParam: ModuleParamBase
    {
        private string nonSplittedIds { get; set; }
        public List<Partner> PartnerIds { get; set; } = new List<Partner>();

        private string allPartnersPre { get; set; }
        public bool AllPartners { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (!PartnerIds.Any() || (AllPartners && PartnerIds.Any()))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide partner-ids <id,id,id> or AllPartners:true!";
                return result;
            }
            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(PartnerIds).ToLower());

            if (key != null)
            {
                nonSplittedIds = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(AllPartners).ToLower());

            if (key != null)
            {
                allPartnersPre = commandLine.AdditionalParameters[key];
            }

            if ((string.IsNullOrEmpty(nonSplittedIds) && string.IsNullOrEmpty(allPartnersPre) ||
                (!string.IsNullOrEmpty(nonSplittedIds) && !string.IsNullOrEmpty(allPartnersPre))))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide partner-ids <id,id,id> or AllPartners:true!";
                return result;
            }

            if (!PartnerIds.Any() && !string.IsNullOrEmpty(nonSplittedIds))
            {
                var splittedIds = nonSplittedIds.Split(',').Select(id => id.Trim());

                if (!splittedIds.All(id => !string.IsNullOrEmpty(id)))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have provided invalid partner-ids!";
                    return result;
                }

                if (!splittedIds.All(id => !globals.is_GUID(id)))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have provided invalid partner-ids (guid)!";
                    return result;
                }

                PartnerIds = splittedIds.Select(id => new Partner { Id = id }).ToList();

                if (!PartnerIds.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have to provide partner-ids: <id,id,id>!";
                    return result;
                }
            }

            var allPartners = false;
            if (!string.IsNullOrEmpty(allPartnersPre))
            {
                if (bool.TryParse(allPartnersPre, out allPartners))
                {
                    AllPartners = allPartners;
                }
                else
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have provided a valid value for AllPartners:[true|false]!";
                    return result;
                }
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Sync_AD_Partners;
        }

        public SyncADPartnersParam() : base(typeof(Controller.ActiveDirectorySyncController))
        {

        }
    }

    public class Partner
    {
        public string Id { get; set; }
    }
}
