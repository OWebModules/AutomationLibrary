﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class ModuleMeta
    {
        public clsOntologyItem ModuleConfig { get; set; }
        public clsOntologyItem Module { get; set; }
        public clsObjectAtt JsonAttribute { get; set; }

        public clsObjectAtt ParameterNamespace { get; set; }

        public clsOntologyItem Function { get; set; }

        public List<clsOntologyItem> UserAuthentications { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectRel> UserAuthenticationsToUsers { get; set; } = new List<clsObjectRel>();
     
    }
}
