﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class ExportAnonymousParam : ModuleParamBase
    {
        [ParamProp(IdClass = "a747996e9b4749b8b5bb3c067c6db270", NameClass = "ExportAnonymousHTMLModule")]
        public string IdConfig { get; set; }

        public bool ExportHtmlPack { get; set; } = true;

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid (no GUID)!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Export_Anonymous;
        }

        public ExportAnonymousParam() : base(typeof(Controller.HtmlEditorConnector))
        {
        }
    }
}
