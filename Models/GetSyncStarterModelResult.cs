﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class GetSyncStarterModelResult
    {
        public List<clsOntologyItem> ModuleList { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> ModulesToModuleFunction { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ModuleFunctionToFunction { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ModuleFunctionToCommandLineRuns { get; set; } = new List<clsObjectRel>();
    }
}
