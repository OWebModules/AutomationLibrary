﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace AutomationLibrary.Models
{
    public class SyncGitCommentsParam : ModuleParamBase
    {
        [ParamProp(IdClass = "676bddccbc0a4dd682ad2bae5f7cd553", NameClass = "JiraConnectorModule")]
        public string IdConfig { get; set; }

        [ParamProp(IdClass = "c441518dbfe04d55b538df5c5555dd83", NameClass = "user")]
        public string IdMasterUser { get; set; }

        [PasswordPropertyText(true)]
        public string MasterPassword { get; set; }


        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid (no GUID)!";
                return result;
            }

            if (string.IsNullOrEmpty(IdMasterUser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdMasterUser is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdMasterUser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdMasterUser is invalid (no GUID)!";
                return result;
            }

            if (string.IsNullOrEmpty(MasterPassword))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Masterpassword is invalid!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_SyncGitComments;
        }

        public SyncGitCommentsParam() : base(typeof(Controller.GitController))
        {

        }
    }
}
