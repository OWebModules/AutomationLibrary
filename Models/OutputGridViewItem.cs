﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    [KendoGridConfig(width = "100%", height = "100%", groupbable = true, autoBind = true, selectable = SelectableType.cell, scrollable = true)]
    [KendoPageable(refresh = true, pageSizes = new int[] { 5, 10, 15, 20, 50, 100, 1000 }, buttonCount = 5, pageSize = 20)]
    [KendoSortable(mode = SortType.multiple, showIndexes = true)]
    [KendoFilter(extra = true)]
    [KendoStringFilterable(contains = "Contains", startsWith = "Starts with", eq = "Is equal to", neq = "Is not equal to", isnotnull = "Not null", isnull = "Null", isempty = "Empty")]
    public class OutputGridViewItem
    {
        [KendoColumn(hidden = false, title = "Stamp", Order = 0)]
        public DateTime Stamp { get; set; }
        [KendoColumn(hidden = false, title = "Module", Order = 1, filterable = true)]
        public string Module { get; set; }
        [KendoColumn(hidden = false, title = "Function", Order = 2, filterable = true)]
        public string Function { get; set; }
        [KendoColumn(hidden = false, title = "Message", Order = 3, filterable = true)]
        public string Message { get; set; }
        [KendoColumn(hidden = false, title = "Type", Order = 4, filterable = true)]
        public string Type { get; set; }
    }
}
