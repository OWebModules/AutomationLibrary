﻿using MediaViewerModule.Primitives;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class RelateFilesWithMediaItemsAndRefParam : ModuleParamBase
    {

        public string ConfigFile { get; private set; }

        public clsOntologyItem RefItem { get; private set; }

        public List<clsOntologyItem> FileItems { get; private set; }

        public MultimediaItemType MediaType { get; private set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(ConfigFile))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to an Input-File!";
                return result;
            }

            try
            {
                var jsonString = File.ReadAllText(ConfigFile);
                var jsonInput = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonString);

                if (!jsonInput.ContainsKey(nameof(RefItem)) || !jsonInput.ContainsKey(nameof(FileItems)))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The file must contain a {nameof(RefItem)} (clsOntologyItem) and a {nameof(FileItems)} (List<clsOntologyItem>)!";
                    return result;
                }

                RefItem = new clsOntologyItem { GUID = jsonInput[nameof(RefItem)].ToString() };
                FileItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<clsOntologyItem>>(jsonInput[nameof(FileItems)].ToString());

                var mediaType = jsonInput[nameof(MediaType)].ToString();

                if (mediaType.ToLower() == nameof(MultimediaItemType.Audio).ToLower())
                {
                    MediaType = MultimediaItemType.Audio;
                }
                else if (mediaType.ToLower() == nameof(MultimediaItemType.Image).ToLower())
                {
                    MediaType = MultimediaItemType.Image;
                }
                else if (mediaType.ToLower() == nameof(MultimediaItemType.PDF).ToLower())
                {
                    MediaType = MultimediaItemType.PDF;
                }
                else if (mediaType.ToLower() == nameof(MultimediaItemType.Video).ToLower())
                {
                    MediaType = MultimediaItemType.Video;
                }
                else if (mediaType.ToLower() == nameof(MultimediaItemType.VideoAndAudio).ToLower())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided MediaType is not Valid! Valid Types are: <{nameof(MultimediaItemType.Audio)},{nameof(MultimediaItemType.Image)},{nameof(MultimediaItemType.PDF)},{nameof(MultimediaItemType.Video)}>";
                    return result;
                }

            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ConfigFile).ToLower());

            if (key != null)
            {
                ConfigFile = commandLine.AdditionalParameters[key];

            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Relate_Files_With_Mediaitems;
        }

        public RelateFilesWithMediaItemsAndRefParam() : base(typeof(Controller.MedieViewerController))
        {

        }
    }
}
