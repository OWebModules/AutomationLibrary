﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public interface IModuleController
    {
        clsOntologyItem Module { get;  }
        List<Type> Functions { get; }
    }
}
