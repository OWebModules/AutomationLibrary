﻿using Newtonsoft.Json;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public abstract class ModuleParamBase : IModuleParam
    {
        [Browsable(false)]
        public string ConfigurationId { get; set; }

        public string ConfigurationName { get; set; }

        [Browsable(false)]
        [JsonIgnore]
        public IMessageOutput MessageOutput { get; set; }

        private Type controllerType;

        public virtual clsOntologyItem ValidateParam(Globals globals)
        {
            throw new NotImplementedException();
        }

        public virtual clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            throw new NotImplementedException();
        }

        public virtual clsOntologyItem GetFunction()
        {
            throw new NotImplementedException();
        }

        public virtual Type GetControllerType()
        {
            return controllerType;
        }

        public ModuleParamBase(Type controllerType)
        {
            this.controllerType = controllerType;
        }

    }
}
