﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class GenerateOntologyAssemblyParam : ModuleParamBase
    {
        [ParamProp(IdClass = "a39cf4fdfa8d4019ba91052361eb43c8", NameClass = "Ontology-Update")]
        public string IdOntologyUpdate { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdOntologyUpdate))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"You have to provide an valid Id of OntologyUpdate";
                return result;
            }

            if (!globals.is_GUID(IdOntologyUpdate))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"You have to provide an valid Id of OntologyUpdate";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdOntologyUpdate).ToLower());

            if (key != null)
            {
                IdOntologyUpdate = commandLine.AdditionalParameters[key];
            }
            
            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Generate_Ontology_Assembly;
        }

        public GenerateOntologyAssemblyParam() : base(typeof(Controller.GenerateOntologyAssemblyController))
        {

        }
    }
}
