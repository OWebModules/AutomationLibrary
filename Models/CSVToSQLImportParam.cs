﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class CSVToSQLImportParam : ModuleParamBase
    {
        [ParamProp(IdClass = "1e5cf595a21a4734aa6022cdfbc71159", NameClass = "CSVImport")]
        public string IdConfig { get; set; }

        public string DownloadPath { get; set; }

        [Browsable(false)]
        public CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Config-id is not valid!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Config-id is not valid (GUID)!";
                return result;
            }

            if (string.IsNullOrEmpty(DownloadPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Download path is empty!";
                return result;
            }

            try
            {
                if (!Directory.Exists(DownloadPath))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Download path does not exist!";
                    return result;
                }
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"Download path is not valid: {ex.Message}!";
                return result;
                
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdConfig).ToLower());

            if (key != null)
            {
                IdConfig = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(DownloadPath).ToLower());

            if (key != null)
            {
                DownloadPath = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Import_CSV_to_SQL;
        }

        public CSVToSQLImportParam() : base(typeof(Controller.CSVToSQLController))
        {
        }
    }
}
