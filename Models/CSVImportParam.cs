﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class CSVImportParam : ModuleParamBase
    {
        public string CSVPath { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(CSVPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a CSV-Path!";
                return result;
            }

            if (!System.IO.Directory.Exists(CSVPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"The path {CSVPath} does not exist!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(CSVPath).ToLower());

            if (key != null)
            {
                CSVPath = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Import_CSV;
        }

        public CSVImportParam() : base(typeof(Controller.ImportExportController))
        {
        }
    }
}
