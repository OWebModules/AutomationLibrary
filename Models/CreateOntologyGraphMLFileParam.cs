﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GraphMLConnector.Models;

namespace AutomationLibrary.Models
{
    public class CreateOntologyGraphMLFileParam : ModuleParamBase
    {
        public string IdOntology { get; set; }
        public string PathGraphMLFile { get; set; }

        public GraphType GraphType { get; set; }
        public string UrlTemplate { get; set; }

        [Browsable(false)]
        public CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdOntology))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdOntology is not valid!";
                return result;
            }

            if (!globals.is_GUID(IdOntology))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdOntology is not valid (GUID)!";
                return result;
            }

            if (string.IsNullOrEmpty(PathGraphMLFile))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Path to GraphML File is invalid!";
                return result;
            }

            try
            {
                var directoryName = System.IO.Path.GetDirectoryName(PathGraphMLFile);

                if (!System.IO.Directory.Exists(directoryName))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The Directory {directoryName} of File is not existing!";
                    return result;
                }
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"The Path to GraphML File is invalid:{ex.Message}!";
                return result;

            }

            if (GraphType == GraphType.RDF)
            {
                if (string.IsNullOrEmpty(UrlTemplate))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have to provide an URL-Template!";
                    return result;
                }

                try
                {
                    var url = string.Format(UrlTemplate, "xxx");
                }
                catch (Exception ex)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The URI-Template must be like this: http://www.xxx.de/{0} or ex:{0}!";
                    return result;

                }
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdOntology).ToLower());

            if (key != null)
            {
                IdOntology = commandLine.AdditionalParameters[key];

            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(UrlTemplate).ToLower());

            if (key != null)
            {
                UrlTemplate = commandLine.AdditionalParameters[key];

            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Create_Ontology_GraphML_File;
        }

        public CreateOntologyGraphMLFileParam() : base(typeof(Controller.GraphMLController))
        {
        }
    }
}
