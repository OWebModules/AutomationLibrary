﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class ModuleController
    {
        public IModuleController ModuleControllerItem { get; set; }
        public List<IModuleParam> ModuleParams { get; set; } = new List<IModuleParam>();
    }
}
