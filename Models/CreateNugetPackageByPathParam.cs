﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class CreateNugetPackageByPathParam : ModuleParamBase
    {
        [ParamProp(IdClass = "61555a0ab9704a9c91a9920fc6df4fbe", NameClass = "NugetPackageLib")]
        public string IdConfig { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid (no GUID)!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Create_Nuget_Package_By_Path;
        }

        public CreateNugetPackageByPathParam() : base(typeof(Controller.NugetPackageController))
        {
        }
    }
}
