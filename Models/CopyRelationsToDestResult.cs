﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyItemsModule.Models
{
    public class CopyRelationsToDestResult
    {
        public int CountAttributes { get; set; }
        public int CountRelations { get; set; }
    }
}
