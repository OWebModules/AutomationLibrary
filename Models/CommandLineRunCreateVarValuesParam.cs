﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class CommandLineRunCreateVarValuesParam : ModuleParamBase
    {
        [ParamProp(IdClass = "dbadb971a838485ba4eedce6c477ac3c", NameClass = "Comand Line (Run)")]
        public string IdCommandLineRun { get; set; }
        [ParamProp(IdClass = "8a894710e08c42c5b829ef4809830d33", NameClass = "Path", ResultProperty = ResultProp.Name)]
        public string ExcelFilePath { get; set; }
        [ParamProp(IdClass = "6c300ff7ed414a168f3cdf7de6392b07", NameClass = "Sheet (Excel)", ResultProperty = ResultProp.Name)]
        public string SheetName { get; set; }
        public long ColIndexVariables { get; set; }
        public long ColIndexValues { get; set; }
        public bool RemoveHeader { get; set; }
        public bool NoImport { get; set; }
        [ParamProp(IdClass = "8a894710e08c42c5b829ef4809830d33", NameClass = "Path", ResultProperty = ResultProp.Name)]
        public string ExportPath { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdCommandLineRun))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdCodeItem is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdCommandLineRun))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdCodeItem is invalid (no GUID)!";
                return result;
            }

            if (string.IsNullOrEmpty(ExcelFilePath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Filepath of Excel-File is not valid!";
                return result;
            }

            try
            {
                if (!File.Exists(ExcelFilePath))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Excel-File cannot be found!";
                    return result;
                }
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"Error while searching File: {ex.Message}!";
                return result;
            }

            if (!string.IsNullOrEmpty(ExportPath))
            {
                try
                {
                    if (File.Exists(ExportPath))
                    {
                        File.Delete(ExportPath);
                    }
                }
                catch (Exception ex)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Error while checking output-file: {ex.Message}!";
                    return result;
                }
            }
            

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdCommandLineRun).ToLower());

            if (key == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide an Id of Command Line or Code Snipplet";
                return result;
            }

            IdCommandLineRun = commandLine.AdditionalParameters[key];

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ExcelFilePath).ToLower());

            if (key == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide an Path to Excel-File which contains the mapping of variable and value";
                return result;
            }

            ExcelFilePath = commandLine.AdditionalParameters[key];

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(SheetName).ToLower());

            if (key == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a sheet-name";
                return result;
            }

            SheetName = commandLine.AdditionalParameters[key];

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ColIndexVariables).ToLower());

            if (key == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a col-index of the variables-column";
                return result;
            }

            int colIndexVar = 0;
            if (!int.TryParse(commandLine.AdditionalParameters[key], out colIndexVar))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a valid col-index of the variables-column";
                return result;
            }
            ColIndexVariables = colIndexVar;

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ColIndexValues).ToLower());

            if (key == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a col-index of the variables-column";
                return result;
            }

            int colIndexVal = 0;
            if (!int.TryParse(commandLine.AdditionalParameters[key], out colIndexVal))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a valid col-index of the values-column";
                return result;
            }
            ColIndexValues = colIndexVal;

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(RemoveHeader).ToLower());

            if (key != null)
            {
                bool removeHeader;
                if (!bool.TryParse(commandLine.AdditionalParameters[key], out removeHeader))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have to provide a valid flag for RemoveHeader";
                    return result;
                }
                RemoveHeader = removeHeader;
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(NoImport).ToLower());

            if (key != null)
            {
                bool noImport;
                if (!bool.TryParse(commandLine.AdditionalParameters[key], out noImport))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have to provide a valid flag for RemoveHeader";
                    return result;
                }
                NoImport = noImport;
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ExportPath).ToLower());

            if (key != null)
            {
                ExportPath = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Create_Variable_Values;
        }

        public CommandLineRunCreateVarValuesParam() : base(typeof(Controller.CommandLineRunController))
        {
        }
    }
}
