﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class SyncBanktransactionsParam : ModuleParamBase
    {
        [ParamProp(IdClass = "f18f0dc8e57b41f0afbf8a0edd4c0fa8", NameClass = "Textparser")]
        public string IdTextParser { get; set; }

        [ParamProp(IdClass = "c9d2c248e9ea4bf085ade51134818133", NameClass = "Pattern")]
        public string Query { get; set; } = "*";

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdTextParser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Id of textparser is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdTextParser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Id of textparser is invalid (no GUID)!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdTextParser).ToLower());

            if (key != null)
            {
                IdTextParser = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Query).ToLower());

            if (key != null)
            {
                Query = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Sync_Banktransactions;
        }

        public SyncBanktransactionsParam() : base(typeof(Controller.BillModuleController))
        {

        }
    }
}
