﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class GenerateOntologyAssemblyClassParam : ModuleParamBase
    {
        public string IdClassRoot { get; set; }

        public string IdOntology { get; set; }
        public string Version { get; set; }
        public string NameSpace { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdOntology))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"You have to provide an Id of an Ontology";
                return result;
            }

            if (!globals.is_GUID(IdOntology))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"You have to provide an valid Id of an Ontology";
                return result;
            }

            if (string.IsNullOrEmpty(Version))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"You have to provide Version. Format: \"Major.Minor.Build.Revision\"";
                return result;

            }
            var regexVersion = new Regex(@"\d+\.\d+\.\d+\.\d+");

            if (!regexVersion.Match(Version).Success)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"You have to provide Version. Format: \"Major.Minor.Build.Revision\"";
                return result;
            }

            if (string.IsNullOrEmpty(NameSpace))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"You have to provide a Namespace";
                return result;

            }

            if (string.IsNullOrEmpty(FileName))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"You have to provide a Filename";
                return result;
            }

            var invalidFileNameChars = System.IO.Path.GetInvalidFileNameChars();

            if (FileName.Any(fileChar => invalidFileNameChars.Contains(fileChar)))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"You have to provide a valid Filename";
                return result;
            }

            if (string.IsNullOrEmpty(Path))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"You have to provide a Path for Export";
                return result;

            }

            try
            {
                if (!System.IO.Directory.Exists(Path))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The export-path does not exist!";
                    return result;

                }
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"Error while checking the export-Path: {ex.Message}";
                return result;
            }

            if (string.IsNullOrEmpty(IdClassRoot))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a Class-Id for Export";
                return result;
            }

            if (!globals.is_GUID(IdClassRoot))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Class-Id is not valid!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdOntology).ToLower());

            if (key != null)
            {
                IdOntology = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Version).ToLower());

            if (key != null)
            {
                Version = commandLine.AdditionalParameters[key];
            }


            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(NameSpace).ToLower());

            if (key != null)
            {
                NameSpace = commandLine.AdditionalParameters[key];
            }


            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(FileName).ToLower());

            if (key != null)
            {
                FileName = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Path).ToLower());

            if (key != null)
            {
                Path = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdClassRoot).ToLower());

            if (key != null)
            {
                IdClassRoot = commandLine.AdditionalParameters[key];
            }

            result = base.ValidateParam(commandLine, globals);

            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Generate_Ontology_Class_Assembly;
        }

        public GenerateOntologyAssemblyClassParam() : base(typeof(Controller.GenerateOntologyAssemblyController))
        {

        }
    }
}
