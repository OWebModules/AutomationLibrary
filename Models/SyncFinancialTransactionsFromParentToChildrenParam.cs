﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class SyncFinancialTransactionsFromParentToChildrenParam : ModuleParamBase
    {
        public string IdFinanicalTransaction { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (!string.IsNullOrEmpty(IdFinanicalTransaction))
            {
                if (!globals.is_GUID(IdFinanicalTransaction))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Id of Financial Transaction is invalid (no GUID)!";
                    return result;
                }
            }
            
            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdFinanicalTransaction).ToLower());

            if (key != null)
            {
                IdFinanicalTransaction = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Sync_Financial_Transactions_Parent_to_Child;
        }

        public SyncFinancialTransactionsFromParentToChildrenParam() : base(typeof(Controller.BillModuleController))
        {

        }
    }
}
