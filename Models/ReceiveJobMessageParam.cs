﻿using AutomationLibrary.Attributes;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class ReceiveJobMessageParam : ModuleParamBase
    {
        [ParamProp(IdClass = "c1028ce06eca4b13b58c1b0813b24786", NameClass = "Receive Job Message")]
        public string IdConfig { get; set; }

        [Browsable(false)]
        public CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();


        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid (no GUID)!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            
            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Receive_Job_Message;
            
        }

        public ReceiveJobMessageParam() : base(typeof(Controller.AzureServiceBusController))
        {

        }
    }
}
