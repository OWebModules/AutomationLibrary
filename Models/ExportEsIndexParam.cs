﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GraphMLConnector.Models;

namespace AutomationLibrary.Models
{
    public class ExportEsIndexParam : ModuleParamBase
    {
        
        public string Server { get; set; }

        public int Port { get; private set; }
        public string Index { get; private set; }
        public string Type { get; private set; }
        public string ExportPath { get; private set; }
        public string ExportFilePrefix { get; set; }

        [Browsable(false)]
        public CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(Server))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Server is not valid!";
                return result;
            }

            if (Port <= 0)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Port is not valid!";
                return result;
            }

            if (string.IsNullOrEmpty(Index))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Index is not valid!";
                return result;
            }

            if (string.IsNullOrEmpty(Type))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Type is not valid!";
                return result;
            }

            if (string.IsNullOrEmpty(ExportPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Path to GraphML File is invalid!";
                return result;
            }

            try
            {
                if (!System.IO.Directory.Exists(ExportPath))
                {
                    System.IO.Directory.CreateDirectory(ExportPath);
                }
            }
            catch (Exception ex)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"The Exportpath is invalid:{ex.Message}!";
                return result;

            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Server).ToLower());

            if (key != null)
            {
                Server = commandLine.AdditionalParameters[key];

            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Port).ToLower());

            if (key != null)
            {
                int port;
                if (!int.TryParse(commandLine.AdditionalParameters[key], out port))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The Parameter {Port} is no valid integer!";
                    return result;
                }
                Port = port;
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Index).ToLower());

            if (key != null)
            {
                Index = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Type).ToLower());

            if (key != null)
            {
                Type = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ExportPath).ToLower());

            if (key != null)
            {
                ExportPath = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ExportFilePrefix).ToLower());

            if (key != null)
            {
                ExportFilePrefix = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Export_ElasticSearch_Index;
        }

        public ExportEsIndexParam() : base(typeof(Controller.ImportExportController))
        {
        }
    }
}
