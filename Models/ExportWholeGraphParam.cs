﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class ExportWholeGraphParam : ModuleParamBase
    {
        public string ExportPath { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(ExportPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a Export-Path!";
                return result;
            }

            if (!System.IO.Directory.Exists(ExportPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"The path {ExportPath} does not exist!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ExportPath).ToLower());

            if (key != null)
            {
                ExportPath = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Export_whole_graph;
        }

        public ExportWholeGraphParam() : base(typeof(Controller.ImportExportController))
        {
        }
    }
}
