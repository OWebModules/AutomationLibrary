﻿using Newtonsoft.Json;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    [KendoGridConfig(width = "100%", height = "100%", groupbable = true, autoBind = true, selectable = SelectableType.row, scrollable = true)]
    [KendoPageable(refresh = true, pageSizes = new int[] { 5, 10, 15, 20, 50, 100, 1000 }, buttonCount = 5, pageSize = 20)]
    [KendoSortable(mode = SortType.multiple, showIndexes = true)]
    [KendoFilter(extra = true)]
    [KendoStringFilterable(contains = "Contains", startsWith = "Starts with", eq = "Is equal to", neq = "Is not equal to", isnotnull = "Not null", isnull = "Null", isempty = "Empty")]
    public class KendoModuleItem
    {
        [KendoColumn(hidden = true)]
        public string Id { get; set; }
        [KendoColumn(hidden = false, title = "Module", Order = 0, filterable = true)]
        public string Name { get; set; }
        [KendoColumn(hidden = true)]
        public string IdFunction { get; set; }
        [KendoColumn(hidden = false, title = "Function", Order = 1, filterable = true)]
        public string Function { get; set; }
        [KendoColumn(hidden = true)]

        public KendoModuleItem()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
