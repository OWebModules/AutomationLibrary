﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class ExportOntologyAssembliesParam : ModuleParamBase
    {
        public string RootPath { get; set; }
        public List<NamespaceToExport> NamespacesToExport { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (!string.IsNullOrEmpty(RootPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a RootPath or you have to set UseDbPath to true!";
                return result;
            }

            if (!string.IsNullOrEmpty(RootPath))
            {
                try
                {
                    if (!Directory.Exists(RootPath))
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = $"The RootPath does not exist!";
                    }
                }
                catch (Exception ex)
                {

                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The RootPath is not valid: {ex.Message}";
                }
            }
            
            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var keyRootPath = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(RootPath).ToLower());

            if (keyRootPath != null)
            {
                RootPath = commandLine.AdditionalParameters[keyRootPath];
            }

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(NamespacesToExport).ToLower());

            if (key != null)
            {
                try
                {
                    var namespacesUnsplitted = commandLine.AdditionalParameters[nameof(NamespacesToExport)];

                    NamespacesToExport = namespacesUnsplitted.Split(',').Select(names => names.Trim()).Select(name => new NamespaceToExport { Name = name }).ToList();
                }
                catch (Exception ex)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The NamespacesToExport-Parameter is not valid: {ex.Message}";
                    return result;
                }
            }

            result = ValidateParam(globals);
            return result;
        }
        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Export_Ontology_Assemblies;
        }

        public ExportOntologyAssembliesParam() : base(typeof(Controller.FileSystemModuleController))
        {
        }
    }

    public class NamespaceToExport
    {
        public string Name { get; set; }
    }
}
