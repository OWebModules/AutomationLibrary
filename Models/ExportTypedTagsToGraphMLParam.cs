﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class ExportTypedTagsToGraphMLParam : ModuleParamBase
    {
        public string IdConfig { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is not valid (empty)!";
                return result;
            }

            if (!globals.is_GUID( IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is not valid (no guid)!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdConfig).ToLower());

            if (key != null)
            {
                IdConfig = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Export_Typed_Tags;
        }

        public ExportTypedTagsToGraphMLParam() : base(typeof(Controller.GraphMLController))
        {
        }
    }
}
