﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;

namespace AutomationLibrary.Models
{
    public class ReIndexTextParserPatternSourceFieldsParam : ModuleParamBase
    {
        [ParamProp(IdClass = "f18f0dc8e57b41f0afbf8a0edd4c0fa8", NameClass = "Textparser")]
        public string IdTextParser { get; set; }

        [Browsable(false)]
        public CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdTextParser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdTextParser is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdTextParser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdTextParser is invalid (no GUID)!";
                return result;
            }


            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdTextParser).ToLower());

            if (key != null)
            {
                IdTextParser = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Re_Index_Pattern_Source_Fields;
        }

        public ReIndexTextParserPatternSourceFieldsParam() : base(typeof(Controller.TextParserController))
        {

        }
    }
}
