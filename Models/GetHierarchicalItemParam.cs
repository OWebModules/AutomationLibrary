﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class GetHierarchicalItemParam : ModuleParamBase
    {

        public string IdClass { get; set; }
        public string IdRelationType { get; set; }
        public string FullName { get; set; }
        public string FullNameSplitter { get; set; }
        public List<HierarchicalExcludeItem> NameSpaceIdsToExclude { get; set; } = new List<HierarchicalExcludeItem>();

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdClass))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdClass is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdClass))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdClass is invalid (no GUID)!";
                return result;
            }

            if (string.IsNullOrEmpty(IdRelationType))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdClass is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdRelationType))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdClass is invalid (no GUID)!";
                return result;
            }

            if (string.IsNullOrEmpty(FullNameSplitter))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a fullname-splitter!";
                return result;
            }

            if (string.IsNullOrEmpty(FullName))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a fullname of the hierarchical Item, e.g. RootLeaf.SubLeaf.EndLeaf!";
                return result;
            }

            foreach (var nameSpace in NameSpaceIdsToExclude)
            {
                if (string.IsNullOrEmpty(nameSpace.Id) || !globals.is_GUID(nameSpace.Id))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The List of Exclude-ids is not valid!";
                    return result;
                }
            }

            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Get_hierarchical_item;
        }

        public GetHierarchicalItemParam() : base(typeof(Controller.ObjectTreeController))
        {

        }
    }

    public class HierarchicalExcludeItem
    {
        public string Id { get; set; }
        public HierarchicalExcludeItem()
        {

        }
    }
}
