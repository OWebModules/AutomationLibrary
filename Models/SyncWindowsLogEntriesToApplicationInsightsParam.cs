﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace AutomationLibrary.Models
{
    
    public class SyncWindowsLogEntriesToApplicationInsightsParam : ModuleParamBase
    {
        [ParamProp(IdClass = "5acab7412f114736b4f03d16f95c014d", NameClass = "LogEntries to Application Insights")]
        public string IdConfig { get; set; }

        public string IdUser { get; set; }

        [PasswordPropertyText(true)]
        public string Password { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid (no GUID)!";
                return result;
            }

            if (string.IsNullOrEmpty(IdUser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdUser is invalid (empty)!";
                return result;
            }

            if (!globals.is_GUID(IdUser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid (no GUID)!";
                return result;
            }


            if (string.IsNullOrEmpty(Password))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Password is empty!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdConfig).ToLower());

            if (key == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide an Id of Config!";
                return result;

            }

            IdConfig = commandLine.AdditionalParameters[key];

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_OItem_Sync_Windows_Logentries_to_Application_Insights;
        }

        public SyncWindowsLogEntriesToApplicationInsightsParam() : base(typeof(Controller.LogController))
        {

        }
    }
}
