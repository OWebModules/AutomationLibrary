﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class ImportAssemblyOntologyParam : ModuleParamBase
    {
        public string OntologyAssemblyPath { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(OntologyAssemblyPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a Path to the ontology-assembly!";
                return result;
            }

            if (!System.IO.File.Exists(OntologyAssemblyPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"The path {OntologyAssemblyPath} does not exist!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(OntologyAssemblyPath).ToLower());

            if (key != null)
            {
                OntologyAssemblyPath = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Import_Assembly;
        }

        public ImportAssemblyOntologyParam() : base(typeof(Controller.ImportExportController))
        {

        }
    }
}
