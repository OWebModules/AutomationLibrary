﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace AutomationLibrary.Models
{
    public class CheckExistanceDBItemsParam : ModuleParamBase
    {
        [ParamProp(IdClass = "0c5695558a9d47ca8267bd3b0d5f7dc9", NameClass = "Check existance of Database-Items")]
        public string IdConfig { get; set; }

        [PasswordPropertyText(true)]
        public string MasterPassword { get; set; }

        [Browsable(false)]
        public CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No valid IdConfig!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid GUID!";
                return result;
            }

            if (string.IsNullOrEmpty(MasterPassword))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Master-Passwort must be provided!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdConfig).ToLower());

            if (key != null)
            {
                IdConfig = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(MasterPassword).ToLower());
            if (key != null)
            {
                MasterPassword = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Check_Existance_of_DB_Item;
        }

        public CheckExistanceDBItemsParam() : base(typeof(Controller.DatabaseManagementController))
        {

        }
    }
}
