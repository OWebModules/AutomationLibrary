﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class CheckMsConnectionStringParam : ModuleParamBase
    {
        [ParamProp(IdClass = "23eb3bec8bf148e8aa5017498346ff86", NameClass = "Connectionstring (Database)")]
        public string IdConnectionString { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdConnectionString))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No valid id for Connection string!";
                return result;
            }

            if (!globals.is_GUID(IdConnectionString))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConnectionString is no valid GUID!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdConnectionString).ToLower());

            if (key != null)
            {
                IdConnectionString = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Check_MS_Connectionstring;
        }

        public CheckMsConnectionStringParam() : base(typeof(Controller.DatabaseManagementController))
        {
        }
    }
}
