﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Threading;

namespace AutomationLibrary.Models
{
    
    public class ReplaceFileContentParam : ModuleParamBase
    {
        [ParamProp(IdClass = "076b9a1761fb4c94a5d1a39bc777dc71", NameClass = "Replace Content in Files")]
        public string IdConfig { get; set; }

        [Browsable(false)]
        public CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid (no GUID)!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdConfig).ToLower());

            if (key == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide an Id of a Replace Content in Files configuration!";
                return result;

            }

            IdConfig = commandLine.AdditionalParameters[key];

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Replace_File_Content;
        }

        public ReplaceFileContentParam() : base(typeof(Controller.FileSystemModuleController))
        {

        }
    }
}
