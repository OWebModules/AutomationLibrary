﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class SyncGitProjectsParam : ModuleParamBase
    {
        [ParamProp(IdClass = "6adc7ae21356415d8990a5b68beaf632", NameClass = "GitConnectorModule")]
        public string IdConfig { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid (no GUID)!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Sync_Git_Projects;
        }

        public SyncGitProjectsParam() : base(typeof(Controller.GitController))
        {

        }
    }
}
