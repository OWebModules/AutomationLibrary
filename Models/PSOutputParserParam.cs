﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class PSOutputParserParam : ModuleParamBase
    {
        public string IdConfig { get; set; }
        public string IdUser { get; set; }

        [PasswordPropertyText(true)]
        public string Password { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide an Id for the Config!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a valid Id for the Config!";
                return result;
            }

            if (string.IsNullOrEmpty(IdUser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide an Id for the User!";
                return result;
            }

            if (!globals.is_GUID(IdUser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a valid Id for the User!";
                return result;
            }

            if (string.IsNullOrEmpty(Password))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a password for the User!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var keyIdUser = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdUser).ToLower());

            if (keyIdUser != null)
            {
                IdUser = commandLine.AdditionalParameters[keyIdUser];
            }

            var keyPassword = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Password).ToLower());

            if (keyPassword != null)
            {
                Password = commandLine.AdditionalParameters[keyPassword];
            }

            var keyIdConfig = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdConfig).ToLower());

            if (keyIdConfig != null)
            {
                IdConfig = commandLine.AdditionalParameters[keyIdConfig];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Output_Powershell_Script;
        }

        public PSOutputParserParam() : base(typeof(Controller.PSOutputParser))
        {

        }
    }
}
