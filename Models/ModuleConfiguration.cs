﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;

namespace AutomationLibrary.Models
{
    public class ModuleConfiguration
    {
        public string IdModule { get; set; }
        public string NameModule { get; set; }
        public string IdFunction { get; set; }
        public string NameFunction { get; set; }
        public string IdModuleConfiguration { get; set; }

        public string NameModuleConfiguration { get; set; }

        public List<UserAuthentication> UserAuthentications { get; set; }
    }

    public class UserAuthentication
    {
        public string IdUserAuthentication { get; set; }
        public string NameUserAuthentication { get; set; }
        public string IdUser { get; set; }
        public string NameUser { get; set; }
    }
}
