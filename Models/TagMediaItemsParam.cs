﻿using MediaViewerModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class TagMediaItemsParam : ModuleParamBase
    {
        [ParamProp(IdClass = "177fba268d254bd3a73488a67cba31f6", NameClass = "Media-Types", ResultProperty = ResultProp.Name)]
        public string MediaType { get; set; }

        [Browsable(false)]
        public MediaItemType MediaItemType { get; set; }

        [ParamProp(IdClass = "8a894710e08c42c5b829ef4809830d33", NameClass = "Path", ResultProperty = ResultProp.Name)]
        public string MediaStorePath { get; set; }

        public List<clsOntologyItem> ItemGuidFilter { get; set; } = new List<clsOntologyItem>();

        public List<clsOntologyItem> RelatedGuidFilter { get; set; } = new List<clsOntologyItem>();

        public string NameRegex { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(MediaStorePath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The MediaStor-path is not valid";
                return result;
            }

            if (!System.IO.Directory.Exists(MediaStorePath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The MediaStore-Path is not existing";
                return result;
            }

            if (string.IsNullOrEmpty(MediaType))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The MediaItemType is not valid";
                return result;
            }

            switch (MediaType.ToLower())
            {
                case "audio":
                    MediaItemType = MediaItemType.Audio;
                    break;
                case "video":
                    MediaItemType = MediaItemType.Video;
                    break;

                case "image":
                    MediaItemType = MediaItemType.Image;
                    break;

                default:
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The MediaItemType is not valid";
                    return result;
            }

            if (!string.IsNullOrEmpty(NameRegex))
            {
                try
                {
                    var regex = new Regex(NameRegex);

                }
                catch (Exception ex)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The Regex for names is not valid: {ex.Message} ";
                    return result;

                }
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(MediaStorePath).ToLower());

            if (key != null)
            {
                MediaStorePath = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(MediaType).ToLower());

            if (key != null)
            {
                MediaType = commandLine.AdditionalParameters[key];

            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ItemGuidFilter).ToLower());

            if (key != null)
            {
                var idsSplitted = commandLine.AdditionalParameters[key].Split(',');
                foreach (var id in idsSplitted)
                {
                    var trimmedId = id.Trim();
                    if (string.IsNullOrEmpty(trimmedId))
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "The ItemFilter-Ids are not valid (empty id)";
                        return result;
                    }

                    if (!globals.is_GUID(trimmedId))
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "The ItemFilter-Ids are not valid (no valid GUID)";
                        return result;
                    }
                    ItemGuidFilter.Add(new clsOntologyItem
                    {
                        GUID = trimmedId
                    });
                }
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(RelatedGuidFilter).ToLower());

            if (key != null)
            {
                var idsSplitted = commandLine.AdditionalParameters[key].Split(',');
                foreach (var id in idsSplitted)
                {
                    var trimmedId = id.Trim();
                    if (string.IsNullOrEmpty(trimmedId))
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "The RelatedFilter-Ids are not valid (empty id)";
                        return result;
                    }

                    if (!globals.is_GUID(trimmedId))
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = "The RelatedFilter-Ids are not valid (no valid GUID)";
                        return result;
                    }
                    RelatedGuidFilter.Add(new clsOntologyItem
                    {
                        GUID = trimmedId
                    });
                }
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(NameRegex).ToLower());

            if (key != null)
            {
                NameRegex = commandLine.AdditionalParameters[key];
            }
            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Tag_Media_Items;
        }

        public TagMediaItemsParam() : base(typeof(Controller.MedieViewerController))
        {

        }
    }
}
