﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class CompareAdGroupMembersParam : ModuleParamBase
    {
        public string SrcGroupFilter { get; set; }
        public string SrcPrincipalPath { get; set; }
        public string SrcContextPrincipalName { get; set; }


        public string DstGroupFilter { get; set; }
        public string DstPrincipalPath { get; set; }
        public string DstContextPrincipalName { get; set; }


        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(SrcGroupFilter))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Source: You have to provide a Groupfilter for the Source-Groups!";
                return result;
            }

            if (string.IsNullOrEmpty(SrcContextPrincipalName))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Source: You have to provide a Principalname for the context!";
                return result;
            }

            if (string.IsNullOrEmpty(SrcPrincipalPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Source: You have to provide a Ldap-Path for the Group-OU!";
                return result;
            }


            if (string.IsNullOrEmpty(DstGroupFilter))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Dest: You have to provide a Groupfilter for the Source-Groups!";
                return result;
            }

            if (string.IsNullOrEmpty(DstContextPrincipalName))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Dest: You have to provide a Principalname for the context!";
                return result;
            }

            if (string.IsNullOrEmpty(DstPrincipalPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Dest: You have to provide a Ldap-Path for the Group-OU!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(SrcContextPrincipalName).ToLower());

            if (key != null)
            {
                SrcContextPrincipalName = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(SrcGroupFilter).ToLower());

            if (key != null)
            {
                SrcGroupFilter = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(SrcPrincipalPath).ToLower());

            if (key != null)
            {
                SrcPrincipalPath = commandLine.AdditionalParameters[key];
            }


            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(DstContextPrincipalName).ToLower());

            if (key != null)
            {
                DstContextPrincipalName = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(DstGroupFilter).ToLower());

            if (key != null)
            {
                DstGroupFilter = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(DstPrincipalPath).ToLower());

            if (key != null)
            {
                DstPrincipalPath = commandLine.AdditionalParameters[key];
            }
            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Compare_Members_of_Ad_Groups;
            
        }

        public CompareAdGroupMembersParam() : base(typeof(Controller.ActiveDirectorySyncController))
        {
        }
    }
}
