﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class MoveObjectListsParam : ModuleParamBase
    {
        [ParamProp(IdClass = "ae079d8a90ea4e8cbeec137f0ac6396e", NameClass = "Move Objectlists")]
        public string IdConfig { get; set; }
        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Config-id is not valid!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Config-id is not valid!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdConfig).ToLower());

            if (key != null)
            {
                IdConfig = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Move_Objects;
        }

        public MoveObjectListsParam() : base(null)
        {

        }
    }
}
