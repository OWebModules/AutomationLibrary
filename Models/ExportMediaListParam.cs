﻿using MediaViewerModule.Primitives;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class ExportMediaListParam : ModuleParamBase
    {
        public string IdMediaList { get; set; }

        [ParamProp(IdClass = "8a894710e08c42c5b829ef4809830d33", NameClass = "Path", ResultProperty = ResultProp.Name)]
        public string MediaStorePath { get; set; }

        [ParamProp(IdClass = "177fba268d254bd3a73488a67cba31f6", NameClass = "Media-Types", ResultProperty = ResultProp.Name)]
        public string MediaType { get; set; }

        [Browsable(false)]
        public MultimediaItemType MediaItemType { get; set; }

        [ParamProp(IdClass = "8a894710e08c42c5b829ef4809830d33", NameClass = "Path", ResultProperty = ResultProp.Name)]
        public string ExportPath { get; set; }

        public string Sort { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdMediaList))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide an Id of an MediaList";
                return result;
            }

            if (!globals.is_GUID(IdMediaList))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The Id of MediaList is no valid GUID";
                return result;
            }

            if (string.IsNullOrEmpty(ExportPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The exportpath is not valid";
                return result;
            }

            if (!System.IO.Directory.Exists(ExportPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The exportpath is not existing";
                return result;
            }

            if (string.IsNullOrEmpty(MediaStorePath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The MediaStor-path is not valid";
                return result;
            }

            if (!System.IO.Directory.Exists(MediaStorePath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The MediaStore-Path is not existing";
                return result;
            }

            if (string.IsNullOrEmpty(MediaType))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The MediaItemType is not valid";
                return result;
            }

            switch (MediaType.ToLower())
            {
                case "audio":
                    MediaItemType = MultimediaItemType.Audio;
                    break;
                case "video":
                    MediaItemType = MultimediaItemType.Video;
                    break;

                case "image":
                    MediaItemType = MultimediaItemType.Image;
                    break;

                case "pdf":
                    MediaItemType = MultimediaItemType.PDF;
                    break;
                default:
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The MediaItemType is not valid";
                    return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdMediaList).ToLower());

            if (key != null)
            {
                IdMediaList = commandLine.AdditionalParameters[key];
            }
            
            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ExportPath).ToLower());

            if (key != null)
            {
                ExportPath = commandLine.AdditionalParameters[key];
            }
            
            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(MediaStorePath).ToLower());

            if (key != null)
            {
                MediaStorePath = commandLine.AdditionalParameters[key];
            }
            
            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(MediaType).ToLower());

            if (key != null)
            {
                MediaType = commandLine.AdditionalParameters[key];
                
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Sort).ToLower());

            if (key != null)
            {
                Sort = commandLine.AdditionalParameters[key];
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Export_Medialist;
        }

        public ExportMediaListParam() : base(typeof(Controller.MedieViewerController))
        {
        }
    }
}
