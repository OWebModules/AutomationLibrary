﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    
    public class CommandLineRunAddVariableParam : ModuleParamBase
    {
        [ParamProp(IdClass = "e116e2bd424b4a76a22908513f919a85", NameClass = "Code-Snipplets")]
        public string IdCodeItem { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdCodeItem))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdCodeItem is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdCodeItem))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdCodeItem is invalid (no GUID)!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdCodeItem).ToLower());

            if (key == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide an Id of Command Line or Code Snipplet!";
                return result;

            }
            
            IdCodeItem = commandLine.AdditionalParameters[key];

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Add_Variables;
        }

        public CommandLineRunAddVariableParam() : base(typeof(Controller.CommandLineRunController))
        {
        }
    }
}
