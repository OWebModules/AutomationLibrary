﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    
    public class SQLImporterParam : ModuleParamBase
    {
        public string IdConfig { get; set; }
        public string IdUser { get; set; }

        [PasswordPropertyText(true)]
        public string Password { get; set; }

        [Browsable(false)]
        public CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid (no GUID)!";
                return result;
            }

            if (string.IsNullOrEmpty(IdUser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a Id of the master-user";
                return result;
            }

            if (!globals.is_GUID(IdUser))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide a valid Id of the master-user";
                return result;
            }

            if (string.IsNullOrEmpty(Password))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide the password for the master-user";
                return result;
            }
            
            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdConfig).ToLower());

            if (key != null)
            {
                IdConfig = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdUser).ToLower());

            if (key != null)
            {
                IdUser = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Password).ToLower());

            if (key != null)
            {
                Password = commandLine.AdditionalParameters[key];
                
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Import_SQL;
        }

        public SQLImporterParam() : base(typeof(Controller.SQLImporterController))
        {

        }
    }
}
