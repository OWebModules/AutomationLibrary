﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class UpdateOntologiesParam : ModuleParamBase
    {
        [ParamProp(IdClass = "8a894710e08c42c5b829ef4809830d33", NameClass = "Path", ResultProperty = ResultProp.Name)]
        public string RootPath { get; set; }
        public bool UseDbPath { get; set; }

        [ParamProp(IdClass = "c9d2c248e9ea4bf085ade51134818133", NameClass = "Pattern", ResultProperty = ResultProp.Name)]
        public string RegexExclude { get; set; }
        [ParamProp(IdClass = "c9d2c248e9ea4bf085ade51134818133", NameClass = "Pattern", ResultProperty = ResultProp.Name)]
        public string RegexInclude { get; set; }

        public bool Test { get; set; }

        [Browsable(false)]
        public Regex ExcludeRegex { get; set; }

        [Browsable(false)]
        public Regex IncludeRegex { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (UseDbPath)
            {
                if (!string.IsNullOrEmpty(RootPath))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have to provide a RootPath or you have to set UseDbPath to true!";
                    return result;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(RootPath))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have to provide a RootPath or you have to set UseDbPath to true!";
                    return result;
                }
            }

            if (!string.IsNullOrEmpty(RootPath))
            {
                try
                {
                    if (!Directory.Exists(RootPath))
                    {
                        result = globals.LState_Error.Clone();
                        result.Additional1 = $"The RootPath does not exist!";
                    }
                }
                catch (Exception ex)
                {

                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The RootPath is not valid: {ex.Message}";
                }
            }

            if (!string.IsNullOrEmpty(RegexExclude))
            {
                try
                {
                    ExcludeRegex = new Regex(RegexExclude);
                }
                catch (Exception ex)
                {

                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided Regex is not valid: {ex.Message}";
                    return result;
                }
            }
            
            if (!string.IsNullOrEmpty(RegexInclude))
            {
                try
                {
                    IncludeRegex = new Regex(RegexInclude);
                }
                catch (Exception ex)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The provided Regex is not valid: {ex.Message}";
                    return result;
                }
            }


            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var keyRootPath = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(RootPath).ToLower());

            if (keyRootPath != null)
            {
                RootPath = commandLine.AdditionalParameters[keyRootPath];
            }

            var keyUseDbPath = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(UseDbPath).ToLower());

            if (keyUseDbPath != null)
            {
                var useDbPath = false;

                if (!bool.TryParse(commandLine.AdditionalParameters[keyUseDbPath], out useDbPath))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have to provide a RootPath or you have to set UseDbPath to true!";
                    return result;
                }
                else
                {
                    UseDbPath = useDbPath;
                }

            }

            var keyExclude = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ExcludeRegex).ToLower());

            if (keyExclude != null)
            {
                RegexExclude = commandLine.AdditionalParameters[keyExclude];
            }

            var keyInclude = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IncludeRegex).ToLower());

            if (keyInclude != null)
            {
                RegexInclude = commandLine.AdditionalParameters[keyInclude];
            }

            var keyTest = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Test).ToLower());

            if (keyTest != null)
            {
                var boolVal = false;
                if (!bool.TryParse(commandLine.AdditionalParameters[keyTest], out boolVal))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The parameter Test must have a valid boolean value!";
                    return result;
                }
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Update_Ontologies;
        }

        public UpdateOntologiesParam() : base(typeof(Controller.FileSystemModuleController))
        {

        }
    }
}
