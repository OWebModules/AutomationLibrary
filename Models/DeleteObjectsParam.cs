﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class DeleteObjectsParam : ModuleParamBase
    {
        public string IdConfig { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdConfig))
            {
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result.Additional1 = "IdConfig is no valid GUID!";
                return result;
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Delete_Objects;
        }

        public DeleteObjectsParam() : base(typeof(Controller.ObjectEditController))
        {
        }
    }
}
