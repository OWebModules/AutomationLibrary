﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class MeasureTextParserParam : ModuleParamBase
    {
        [ParamProp(IdClass = "60df3e8e41144cb082183911e07f26a0", NameClass = "Textparser-Measurement")]
        public string IdConfig { get; set; }

        [Browsable(false)]
        public CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();
            if (string.IsNullOrEmpty(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid!";
                return result;
            }

            if (!globals.is_GUID(IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invalid (no GUID)!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();
            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdConfig).ToLower());

            if (key == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide an Id of a Config to execute (measure)";
                return result;
            }

            IdConfig = commandLine.AdditionalParameters[key];

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Measure_Textparser;
        }

        public MeasureTextParserParam() : base(typeof(Controller.TextParserController))
        {

        }

    }
}
