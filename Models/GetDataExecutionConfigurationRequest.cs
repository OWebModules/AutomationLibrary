﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class GetDataExecutionConfigurationRequest
    {
        public string IdModule { get; private set; }
        public string IdFunction { get; private set; }

        public List<ModuleController> ModuleItems { get; set; } = new List<ModuleController>();

        public IMessageOutput MessageOutput { get; set; }

        public GetDataExecutionConfigurationRequest(string idModule, string idFunction)
        {
            IdModule = idModule;
            IdFunction = idFunction;
        }
    }
}
