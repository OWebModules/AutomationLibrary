﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class GetUseCaseListByTypedTagParam : ModuleParamBase
    {
        public string IdRef { get; set; }

        [Browsable(false)]
        public CancellationTokenSource CancellationTokenSource { get; set; } = new CancellationTokenSource();

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdRef))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdRef is not valid!";
                return result;
            }

            if (!globals.is_GUID(IdRef))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdRef is not valid (GUID)!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdRef).ToLower());

            if (key != null)
            {
                IdRef = commandLine.AdditionalParameters[key];

            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Get_UseCases_by_Typed_Tag;
        }

        public GetUseCaseListByTypedTagParam() : base(typeof(Controller.UseCaseController))
        {

        }
    }
}
