﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using AutomationLibrary.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class ExportMsgFilesParam : ModuleParamBase
    {
        [ParamProp(IdClass = "8a894710e08c42c5b829ef4809830d33", NameClass = "Path", ResultProperty = ResultProp.Name)]
        public string DestPath { get; set; }
        public bool ExportSubFolders { get; set; }

        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(DestPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "You have to provide an Path to Export Msg-files to!";
                return result;
            }

            if (!System.IO.Directory.Exists(DestPath))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The exportpath is not existing!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(DestPath).ToLower());

            if (key != null)
            {
                DestPath = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(ExportSubFolders).ToLower());

            if (key != null)
            {
                bool checkBool;
                if (!bool.TryParse(commandLine.AdditionalParameters[key], out checkBool))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have to provide a valid bool-value for parameter {nameof(ExportSubFolders)}";
                    return result;
                }

                ExportSubFolders = checkBool;
            }

            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Export_MSG_Files;
        }

        public ExportMsgFilesParam() : base(typeof(Controller.OutlookController))
        {
        }
    }
}
