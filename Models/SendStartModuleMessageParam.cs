﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Models
{
    public class SendStartModuleMessageParam : ModuleParamBase
    {
        public string IdMessage { get; set; }
        
        [PasswordPropertyText(true)]
        public string Password { get; set; }
        public override clsOntologyItem ValidateParam(Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(IdMessage))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No valid id for message!";
                return result;
            }

            if (!globals.is_GUID(IdMessage))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No valid id for message!";
                return result;
            }

            if (string.IsNullOrEmpty(Password))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "No valid password!";
                return result;
            }

            return result;
        }

        public override clsOntologyItem ValidateParam(CommandLine commandLine, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(IdMessage).ToLower());

            if (key != null)
            {
                IdMessage = commandLine.AdditionalParameters[key];
            }

            key = commandLine.AdditionalParameters.Keys.FirstOrDefault(keyItem => keyItem.ToLower() == nameof(Password).ToLower());

            if (key != null)
            {
                Password = commandLine.AdditionalParameters[key];
            }


            result = ValidateParam(globals);
            return result;
        }

        public override clsOntologyItem GetFunction()
        {
            return Functions.Config.LocalData.Object_Send_Sync_Starter_Message;
        }

        public SendStartModuleMessageParam() : base(typeof(Controller.RabbitMQController))
        {

        }
    }
}
