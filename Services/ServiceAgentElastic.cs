﻿using AutomationLibrary.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Services
{
    public class ServiceAgentElastic : ElasticBaseAgent
    {
        public async Task<ResultItem<List<ModuleConfiguration>>> GetModuleConfigurations(string idModule, string idFunction)
        {
            var taskResult = await Task.Run<ResultItem<List<ModuleConfiguration>>>(() =>
           {
               var result = new ResultItem<List<ModuleConfiguration>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<ModuleConfiguration>()
               };

               var searchModuleConfig = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Other = idModule,
                       ID_RelationType = SyncStarterModule.Config.LocalData.ClassRel_ModuleConfig__SyncStarter__belongs_to_Module.ID_RelationType,
                       ID_Parent_Object = SyncStarterModule.Config.LocalData.ClassRel_ModuleConfig__SyncStarter__belongs_to_Module.ID_Class_Left
                   }
               };

               var dbReaderModuleConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderModuleConfig.GetDataObjectRel(searchModuleConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Module-Config by module-id";
                   return result;
               }

               var searchModuleConfigByFunctionId = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Other = idFunction,
                       ID_RelationType = SyncStarterModule.Config.LocalData.ClassRel_ModuleConfig__SyncStarter__belongs_to_Module_Function.ID_RelationType,
                       ID_Parent_Object = SyncStarterModule.Config.LocalData.ClassRel_ModuleConfig__SyncStarter__belongs_to_Module_Function.ID_Class_Left
                   }
               };

               var dbReaderModuleConfigByFunctionId = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderModuleConfigByFunctionId.GetDataObjectRel(searchModuleConfigByFunctionId);

               var searchUserAuthentications = dbReaderModuleConfig.ObjectRels.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Object,
                   ID_RelationType = SyncStarterModule.Config.LocalData.ClassRel_ModuleConfig__SyncStarter__secured_by_User_Authentication.ID_RelationType,
                   ID_Parent_Other = SyncStarterModule.Config.LocalData.ClassRel_ModuleConfig__SyncStarter__secured_by_User_Authentication.ID_Class_Right
               }).ToList();

               var dbReaderUserAuthentications = new OntologyModDBConnector(globals);

               if (searchUserAuthentications.Any())
               {
                   result.ResultState = dbReaderUserAuthentications.GetDataObjectRel(searchUserAuthentications);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Userauthentications!";
                       return result;
                   }
               }

               var searchUsersOfUserAuthentications = dbReaderUserAuthentications.ObjectRels.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = SyncStarterModule.Config.LocalData.ClassRel_User_Authentication_secured_by_user.ID_RelationType,
                   ID_Parent_Other = SyncStarterModule.Config.LocalData.ClassRel_User_Authentication_secured_by_user.ID_Class_Right
               }).ToList();

               var dbReaderUserAuthenticationsToUsers = new OntologyModDBConnector(globals);

               if (searchUsersOfUserAuthentications.Any())
               {
                   result.ResultState = dbReaderUserAuthenticationsToUsers.GetDataObjectRel(searchUsersOfUserAuthentications);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Users of Userauthentications!";
                       return result;
                   }

               }

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Module-Config by function-id";
                   return result;
               }

               result.Result.AddRange(from moduleConfigByIdModule in dbReaderModuleConfig.ObjectRels
                                       join moduleConfigByIdFunction in dbReaderModuleConfigByFunctionId.ObjectRels on moduleConfigByIdModule.ID_Object equals moduleConfigByIdFunction.ID_Object
                                      select new ModuleConfiguration
                                      {
                                          IdModule = moduleConfigByIdModule.ID_Other,
                                          NameModule = moduleConfigByIdModule.Name_Other,
                                          IdFunction = moduleConfigByIdFunction.ID_Other,
                                          NameFunction = moduleConfigByIdFunction.Name_Other,
                                          IdModuleConfiguration = moduleConfigByIdModule.ID_Object,
                                          NameModuleConfiguration = moduleConfigByIdModule.Name_Object
                                      });

               foreach (var moduleConfig in result.Result)
               {
                   var userAuthentications = dbReaderUserAuthentications.ObjectRels
                       .Where(rel => rel.ID_Object == moduleConfig.IdModuleConfiguration).ToList();

                   moduleConfig.UserAuthentications = (from userAuthentication in userAuthentications
                       join userRel in dbReaderUserAuthenticationsToUsers.ObjectRels on userAuthentication.ID_Other
                           equals userRel.ID_Object
                       select new UserAuthentication
                       {
                           IdUserAuthentication = userAuthentication.ID_Other,
                           NameUserAuthentication = userAuthentication.Name_Other,
                           IdUser = userRel.ID_Other,
                           NameUser = userRel.Name_Other
                       }).ToList();
               }

               return result;
           });
           return taskResult;
          
        }
        public ServiceAgentElastic(Globals globals) : base(globals)
        {
            
        }
    }
}
