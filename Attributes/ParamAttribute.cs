﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Attributes
{
    public class ParamAttribute : Attribute
    {
        public string FunctionId { get; set; }
    }
}
