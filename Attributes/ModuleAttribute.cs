﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary.Attributes
{
    public class ModuleAttribute:Attribute
    {
        public string Module { get; set; }
        public Type[] ParameterTypes { get; set; }
    }
    
}
