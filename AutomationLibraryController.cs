﻿using AutomationLibrary.Controller;
using AutomationLibrary.Models;
using AutomationLibrary.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AutomationLibrary
{
    public class AutomationLibraryController
    {
        public Globals Globals { get; private set; }

        public async Task<ResultItem<KendoDropDownRemoteConfig>> GetDropdownConfigExecutionConfig(string readAction, string metaAction)
        {
            var taskResult = await Task.Run<ResultItem<KendoDropDownRemoteConfig>>(() =>
           {
               var result = new ResultItem<KendoDropDownRemoteConfig>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new KendoDropDownRemoteConfig(nameof(ModuleConfiguration.IdModuleConfiguration), nameof(ModuleConfiguration.NameModuleConfiguration))
                   {
                       optionLabel = "Configuration",
                       dataSource = new KendoDataSourceSimple
                       {
                           transport = new KendoTransport
                           {
                               read = new KendoTransportCRUD
                               {
                                   dataType = "json",
                                   url = readAction
                               }
                           }
                       },
                       MetaUrl = metaAction
                   }
               };
               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<ModuleConfiguration>>> GetDataExecutionConfiguration(Models.GetDataExecutionConfigurationRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<ModuleConfiguration>>>(async() =>
           {
               var result = new ResultItem<List<ModuleConfiguration>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<ModuleConfiguration>
                   {
                       new ModuleConfiguration
                       {
                           IdModuleConfiguration = "NewConfiguration",
                           NameModuleConfiguration = "New Configuration"
                       }
                   }
               };

               if (!request.ModuleItems.Any())
               {
                   var getModulesResult = await GetModuleControllers();
                   result.ResultState = getModulesResult.ResultState;
                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       return result;
                   }

                   request.ModuleItems = getModulesResult.Result;
               }

               if (string.IsNullOrEmpty(request.IdFunction) || string.IsNullOrEmpty(request.IdModule))
               {
                   return result;
               }

               var serviceAgent = new Services.ServiceAgentElastic(Globals);

               var configsResult = await serviceAgent.GetModuleConfigurations(request.IdModule, request.IdFunction);

               result.ResultState = configsResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }
               configsResult.Result = configsResult.Result.OrderBy(config => config.NameModuleConfiguration).ToList();
               result.Result.AddRange(configsResult.Result);

               return result;
           });

            return taskResult;
        }

        public clsObjectRel GetNewExecutionConfig()
        {
            return new clsObjectRel
            {
                ID_Other = "Empty",
                Name_Other = "New Config"
            };
        }

        public async Task<ResultItem<List<ModuleController>>> GetModuleControllers()
        {
            var taskResult = await Task.Run<ResultItem<List<ModuleController>>>(() =>
            {
                var result = new ResultItem<List<ModuleController>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<ModuleController>()
                };

                var types = Assembly.GetExecutingAssembly().GetTypes();

                foreach (var moduleType in types.Where(type => type.BaseType == typeof(BaseController)))
                {
                    if (moduleType.GetInterfaces().FirstOrDefault(interf => interf.Name == nameof(IModuleController)) == null) continue;
                    var moduleMeta = Activator.CreateInstance(moduleType) as IModuleController;
                    if (moduleMeta == null) continue;

                    var moduleController = new ModuleController
                    {
                        ModuleControllerItem = moduleMeta
                    };
                    foreach (var parameterType in moduleMeta.Functions)
                    {
                        var parameter = Activator.CreateInstance(parameterType) as IModuleParam;
                        moduleController.ModuleParams.Add(parameter);

                    }
                    result.Result.Add(moduleController);
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<KendoModuleItem>>> GetKendoModuleItems()
        {
            var taskResult = await Task.Run<ResultItem<List<KendoModuleItem>>>(() =>
           {
               var result = new ResultItem<List<KendoModuleItem>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<KendoModuleItem>()
               };

               var types = Assembly.GetExecutingAssembly().GetTypes();

               foreach (var moduleType in types.Where(type => type.BaseType == typeof(BaseController)))
               {
                   if (moduleType.GetInterfaces().FirstOrDefault(interf => interf.Name == nameof(IModuleController)) == null) continue;
                   var moduleMeta = Activator.CreateInstance(moduleType) as IModuleController;
                   if (moduleMeta == null) continue;

                   foreach (var parameterType in moduleMeta.Functions)
                   {
                       var parameter = Activator.CreateInstance(parameterType) as IModuleParam;
                       var function = parameter.GetFunction();

                       var moduleItem = new KendoModuleItem
                       {
                           Name = moduleMeta.Module.Name,
                           Id = moduleMeta.Module.GUID,
                           IdFunction = function.GUID,
                           Function = function.Name
                       };
                       result.Result.Add(moduleItem);
                   }
               }

               return result;
           });

            return taskResult;
        }

        public AutomationLibraryController(Globals globals)
        {
            Globals = globals;
        }
    }
}
